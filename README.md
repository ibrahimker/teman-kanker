# Cancure
Cancure is a mobile application for cancer patient, providing one-stop solution for them from chat into symptom checker. Cancure was build on top of react-native. Check the design at [Zepplin](https://app.zeplin.io/project/594798f07422b65b865550ba/dashboard)

## Some work motivation for us
![Mengapa Dian Sastro Cantik Sekali](http://seleb.fajar.co.id/wp-content/uploads/2017/04/maxresdefault.jpg "Mengapa Dian Sastro Cantik Sekali")

## Dependency
- [react-native](https://facebook.github.io/react-native/)
- [react-native-app-intro](https://github.com/FuYaoDe/react-native-app-intro)
- [native-base](https://github.com/GeekyAnts/NativeBase)
- [react-native-drawer](https://github.com/root-two/react-native-drawer)
- [react-navigation](https://reactnavigation.org/)

## Installation
1. For windows, it's recommended to use [chocho](https://chocolatey.org/) as a package manager.
2. Install JDK8, Node.JS, Python2, Yarn 
    ```
    choco install nodejs.install
    choco install python2
    choco install jdk8
    choco install yarn
    ```
3. Install [Android Studio](https://developer.android.com/studio/index.html), make sure to install AVD, SDK version 22, latest gradle, and latest SDK. Also install google-usb-driver if you plan to deploy directly to your handphone.
4. Install React Native CLI ```npm install -g react-native-cli```
5. Makesure you already setup git SSH key in your local. If you have set it up, clone to your local `git clone git@github.ibm.com:ibrahimni/cancure.git`
6. Open the folder using command prompt, for first time installation, run `npm install`. This will install all dependency stated in package.json. Some plugin need to be linked to the manifest, just run `react-native link` for that case
7. After success, startup the emulator then run `react-native run-android`

## Common Problem
- Failed because of gradle. Just go to android folder then clean the gradle
    ```
    cd android
    gradlew clean
    ```
- More will be come :)

## Project Structure
Some important folder in this Project is listed below
-android : Hold all file for compiling into native android
-app : Our main code
--components: Hold component template, such as sidebar
--config: constanta config defined here, such as image location and page routing
--images: as the names
--pages: code about view and logic is in here
-ios : Hold all file for compiling into native ios
package.json: Information about dependency. If want to upgrade react-native (or anything else), just edit it in here
Readme.md: Readme file, please update it if you have latest update

## MIT License
MIT License

Copyright (c) 2017 IBM Global Business Services Indonesia

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.