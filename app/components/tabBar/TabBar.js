import React, { Component } from 'react';
import { View, StyleSheet, TouchableWithoutFeedback, Text, AsyncStorage, NetInfo, StatusBar, Platform } from 'react-native';
import OneSignal from 'react-native-onesignal';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as chatCounterActions from '../../actions/chatCounterActions.js'
import * as pesanActions from '../../actions/pesanActions.js'

const APIENDPOINT = 'http://temankanker.com/api/';

const styles = StyleSheet.create({
  badge: {
    backgroundColor: '#79C948', 
    height: 20, 
    width: 20, 
    position: 'absolute', 
    bottom: 28, 
    right: 5, 
    borderRadius: 30, 
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  tabbar: {
    height: Platform.OS === 'ios' ? 50 : 60,
    flexDirection: 'row',
    backgroundColor: 'white',
    opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 0,
  },
  tabText: {
    fontFamily: 'Montserrat',
    fontSize: 10, 
    marginTop: 5
  },
  tab: {
    alignSelf: 'stretch',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabChild: {
    position: 'absolute', 
    bottom: 0, 
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center', 
    height: 60, 
    width: Platform.OS === 'ios' ? 60 : 70, 
    backgroundColor: '#855fa8', 
    borderTopLeftRadius: Platform.OS === 'ios' ? 30 : 35, 
    borderTopRightRadius: Platform.OS === 'ios' ? 30 : 35
  }
});

class TabBar extends Component {
    constructor(props) {
        super(props)

        this.state = {
          connection: null
        }

        AsyncStorage.getItem('userId', (err, result) => {
          this.setState({userId: result})
          this.props.pesanActions.loadPesan(result)
        });
        AsyncStorage.getItem('onesignalId', (err, result) => {
          if(result!== null){
            if(this.state.userId!=''){
              fetch(APIENDPOINT+'/UserKankers/registerDevice', {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  "userId": this.state.userId,
                  "onesignalId":result
                })
              })
              .then((response) => {
                fetch(APIENDPOINT+'/UserKankers/loginDevice', {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    "userId": this.state.userId,
                    "onesignalId":result
                  })
                })
                .then((response) => {
                })
              })
            }
          }
        });
    }

    componentDidMount() {
      NetInfo.isConnected.fetch().then(isConnected => {
        console.log('First, is ' + (isConnected ? 'online' : 'offline'));
        this.setState({connection: isConnected});
      });

      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleFirstConnectivityChange
      );
    }

    handleFirstConnectivityChange = (isConnected) => {
      console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

      if(isConnected) {
        this.setState({connection: true});
      }
      else {
        this.setState({connection: false});
      }
    }

    removeConnectivityListener() {
      console.log('eventremoved');
      NetInfo.isConnected.removeEventListener(
        'connectionChange',
        this.handleFirstConnectivityChange
      );
    }

    componentWillMount() {
      OneSignal.addEventListener('received', this.onReceived);
      OneSignal.addEventListener('opened', this.onOpened);
      OneSignal.addEventListener('registered', this.onRegistered);    
      OneSignal.addEventListener('ids', this.onIds);
      OneSignal.inFocusDisplaying(2);
    }

    componentWillUnmount() {
      OneSignal.removeEventListener('received', this.onReceived);
      OneSignal.removeEventListener('opened', this.onOpened);
      OneSignal.removeEventListener('registered', this.onRegistered);
      OneSignal.removeEventListener('ids', this.onIds);
    }

    onReceived(notification) {
      console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
    }

    onRegistered(notifData) {
      console.log("Device had been registered for push notifications!", notifData);
    }

    onIds(device) {
      console.log('Device info: ', device.userId);
      setTimeout(function() {
        AsyncStorage.setItem('onesignalId', device.userId);
      }, 3000);
    }

    render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#489ffc');
    const {
      navigation,
      renderIcon,
      getLabel,
      activeTintColor,
      inactiveTintColor,
      jumpToIndex
    } = this.props;

    const { state, actions, room, pesanActions } = this.props;

    const {
      routes
    } = navigation.state;

    return (
      <View>
        {(this.state.connection !== null && this.state.connection === false) && (
            <View style={{ backgroundColor: '#d9534f', padding: 10, position: 'absolute', right: 0, left: 0, bottom: 50, opacity: 0.9 }}>
              <Text style={{ backgroundColor: 'transparent', color: 'white', fontFamily: 'Montserrat' }}>Koneksi internet anda terputus</Text>
            </View>
        )}
        <View style={styles.tabbar}>

          {routes && routes.map((route, index) => {
            const focused = index === navigation.state.index;
            const tintColor = focused ? activeTintColor : inactiveTintColor;
            const activeColor = focused ? '#499FFD' : 'transparent'

            return (
              <TouchableWithoutFeedback
                key={route.key}
                style={styles.tab}
                onPress={() => {jumpToIndex(index)}}
              >
                <View style={styles.tab}>
                  <View style={[styles.tabChild, {backgroundColor: activeColor}]}>
                      <Text style={{color: tintColor, marginTop: Platform.OS === 'ios' ? 10 : 5}}>{renderIcon({route})}</Text>
                      <Text style={[styles.tabText, {color: tintColor}]}>{getLabel({ route })}</Text>
                      {(route.key === 'Pesan' && room.unreadAll > 0) && (
                        <View style={styles.badge}>
                          <Text style={{ color: 'white', backgroundColor: 'transparent', fontSize: 12 }}>{ room.unreadAll }</Text>
                        </View> 
                      )}
                  </View>
                </View>
              </TouchableWithoutFeedback>
            );
          })}

        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  state: state.chatCounter,
  room: state.pesanReducer
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(chatCounterActions, dispatch),
  pesanActions: bindActionCreators(pesanActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(TabBar)
