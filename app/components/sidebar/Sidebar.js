import React, { Component } from 'react';
import {Text,View,Content,Container,List,ListItem,Left,Right,Badge,Thumbnail} from 'native-base';
import { StyleSheet, Image, AsyncStorage, TouchableOpacity, Alert} from 'react-native';
import images from '../../config/images.js'
import styles from './style.js'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Sidebar extends Component {

  state = {
    role: ''
  }

  constructor (props){
    super(props);
  }

  componentWillMount() {
    AsyncStorage.getItem('role', (err, result) => {
      this.setState({role: result});
    }); 
    AsyncStorage.getItem('role', (err, result) => {
      this.setState({role: result});
    }); 
  }

  static navigationOptions = {
    header:null,
  }

  render() {
    
    return (
      <Container>
        <Content style={styles.sidebar}>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
          
          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Profile')}>
          <View style={{borderBottomWidth: 0.5, borderBottomColor:'rgba(178, 178, 178, 0.4)'}}>
            <View style={{height: 200, width: 300, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'center',}}>
              <Thumbnail style={{marginTop: 20, height: 100, width: 100, borderRadius: 50}} source={{uri: 'https://scontent-sin6-2.cdninstagram.com/t51.2885-19/s320x320/13658735_1734084193512326_585790629_a.jpg'}} />
              <Text style={{fontFamily:'Kreon-Regular', fontSize:16 , color: '#3B5998', backgroundColor: 'transparent', textAlign: 'center', marginTop:10}}>Hardika</Text>
              <Text style={{fontFamily:'Montserrat-Regular', fontSize:12 , color: '#B2B2B2', backgroundColor: 'transparent', textAlign: 'center'}}>Penyintas Kanker</Text>
            </View>
          </View>
          </TouchableOpacity>

          <List style={{marginTop:5 }}>
            {/*<ListItem onPress={() =>  this.props.navigation.navigate('Home')} style={styles.borderSideBar}>
                                      <Left>
                                        <Text style={styles.text}>Beranda</Text>
                                      </Left>
                                    </ListItem>*/}
            <ListItem onPress={() =>  this.props.navigation.navigate('News')} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='home' />
                <Text style={styles.text}>Beranda</Text>
              </Left>
            </ListItem>

            {this.state.role != 'doctor' && this.state.role != 'survivor' && 

              <View>
                <ListItem onPress={() =>  this.props.navigation.navigate('Learn')} style={styles.borderSideBar}>
                  <Left>
                    <Icon style={styles.inputIcon} active name='info-circle' />
                    <Text style={styles.text}>Informasi Kanker</Text>
                  </Left>
                </ListItem>

                  <ListItem onPress={() =>  this.props.navigation.navigate('Notes')} style={styles.borderSideBar}>
                  <Left>
                    <Icon style={styles.inputIcon} active name='sticky-note' />
                    <Text style={styles.text}>Memo</Text>
                  </Left>
                </ListItem>
              </View>
            }

            {this.state.role == 'patient' && 
              <ListItem onPress={() =>  this.props.navigation.navigate('SymptomChecker')} style={styles.borderSideBar}>
                <Left>
                  <Icon style={styles.inputIcon} active name='thermometer' />
                  <Text style={styles.text}>Deteksi Dini</Text>
                </Left>
              </ListItem>
            }
            {/*<ListItem onPress={() =>  this.props.navigation.navigate('SymptomChecker')} style={styles.borderSideBar}>
                          <Left>
                            <Text style={styles.text}>Deteksi Dini (under development)</Text>
                          </Left>
                        </ListItem>*/}
            
            {/*<ListItem onPress={() =>  this.props.navigation.navigate('NutritionGuide')}>
              <Left>
                <Text style={styles.text}>Nutrition Guide</Text>
              </Left>
            </ListItem>*/}
            
            <ListItem onPress={() =>  this.props.navigation.navigate('CancerSupport')} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='location-arrow' />
                <Text style={styles.text}>Direktori</Text>
              </Left>
            </ListItem>
            <ListItem onPress={() =>  this.props.navigation.navigate('ChatList')} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='comments' />
                <Text style={styles.text}>Pesan</Text>
              </Left>
            </ListItem>
            {/*<ListItem onPress={() =>  this.props.navigation.navigate('Profile')} style={styles.borderSideBar}>
                          <Left>
                            <Icon style={styles.inputIcon} active name='user' />
                            <Text style={styles.text}>Profil</Text>
                          </Left>
                        </ListItem>*/}
            <ListItem onPress={() =>  this.props.navigation.navigate('Settings')} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='cog' />
                <Text style={styles.text2}>Pengaturan</Text>
              </Left>
            </ListItem>
            <ListItem onPress={() =>  this.props.navigation.navigate('Help')} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='question-circle' />
                <Text style={styles.text2}>Bantuan</Text>
              </Left>
            </ListItem>
            <ListItem onPress={() =>  this.props.navigation.navigate('About')} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='info-circle' />
                <Text style={styles.text2}>Tentang</Text>
              </Left>
            </ListItem>
            <ListItem onPress={() => this.logout()} style={styles.borderSideBar}>
              <Left>
                <Icon style={styles.inputIcon} active name='power-off' />
                <Text style={styles.text2}>Keluar</Text>
              </Left>
            </ListItem>
          </List>
          </Image>

        </Content>
      </Container>
    );
  }

  logout(){
    AsyncStorage.setItem('isLoggedIn', 'false');
    AsyncStorage.setItem('username', '');
    AsyncStorage.setItem('role', '');
    this.props.navigation.navigate('Login');
  }
}