const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default {
  sidebar: {
    flex: 1,
    backgroundColor: '#ffffff',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
  },
  drawerCover: {
    alignSelf: 'stretch',
    // resizeMode: 'cover',
    height: deviceHeight / 3.5,
    width: null,
    position: 'relative',
    marginBottom: 10,
  },
  drawerImage: {
    position: 'absolute',
    // left: (Platform.OS === 'android') ? 30 : 40,
    left: (Platform.OS === 'android') ? deviceWidth / 10 : deviceWidth / 9,
    // top: (Platform.OS === 'android') ? 45 : 55,
    top: (Platform.OS === 'android') ? deviceHeight / 13 : deviceHeight / 12,
    width: 210,
    height: 75,
    resizeMode: 'cover',
  },
  listItemContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  iconContainer: {
    width: 37,
    height: 37,
    borderRadius: 18,
    marginRight: 12,
    paddingTop: (Platform.OS === 'android') ? 7 : 5,
  },
  sidebarIcon: {
    fontSize: 21,
    color: '#fff',
    lineHeight: (Platform.OS === 'android') ? 21 : 25,
    backgroundColor: 'transparent',
    alignSelf: 'center',
  },
  text: {
    fontWeight: (Platform.OS === 'ios') ? '400' : '400',
    fontSize: 16,
    fontFamily: 'Kreon-Regular',
    marginLeft: 0,
    color: '#3B5998',
  },
  text2: {
    fontWeight: (Platform.OS === 'ios') ? '400' : '400',
    fontSize: 14,
    fontFamily: 'Kreon-Regular',
    marginLeft: 0,
    color: '#3B5998',
  },
  badgeText: {
    fontSize: (Platform.OS === 'ios') ? 13 : 11,
    fontWeight: '400',
    textAlign: 'center',
    marginTop: (Platform.OS === 'android') ? -3 : undefined,
  },
  icon: {
    color: '#fff',
    fontSize: 26,
    width: 30,
  },
  statBar:{
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
    backgroundColor:'#3ebb64'
  },
  borderSideBar:{
    borderBottomColor:'rgba(178, 178, 178, 0)',
  },
  inputIcon: {
    marginTop: 3,
    marginLeft: 5,
    color: '#3B5998',
    fontSize: 18,
    width:35
  },
  
};