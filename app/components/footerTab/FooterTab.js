import React, { Component } from 'react';
import {Text,Badge,Footer,FooterTab,Button,Body} from 'native-base';
import { StyleSheet, Image, AsyncStorage, TouchableOpacity, Alert} from 'react-native';
import images from '../../config/images.js'
import styles from './style.js'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class FooterTabs extends Component {

  state = {
    role: ''
  }

  constructor (props){
    super(props);
  }

  componentWillMount() {
    AsyncStorage.getItem('role', (err, result) => {
      this.setState({role: result});
    }); 
    AsyncStorage.getItem('role', (err, result) => {
      this.setState({role: result});
    }); 
  }

  static navigationOptions = {
    header:null,
  }

  render() {
    
    return (
      
      <Footer noShadow style={styles.navBarFooterStyle}>
        <Body style={styles.bodySpaceBetween}>
          <Image source={images.buttonFooterBarActive} style={{flex: 1,alignSelf: 'stretch',width: null}}>
            <Button transparent onPress={() =>  this.props.navigation.navigate('Beranda')} style={styles.buttonNavbarTextActive}>
              <Icon style={styles.footerIconActive} name='home'/>
              <Text style={styles.footerTextActive}>Beranda</Text>
            </Button>
          </Image>
          <Button transparent onPress={() =>  this.props.navigation.navigate('Informasi')} style={styles.buttonNavbarText}>
            <Icon style={styles.footerIconInactive} name='file'/>
            <Text style={styles.footerTextInactive}>Informasi</Text>
          </Button>
          <Button transparent onPress={() =>  this.props.navigation.navigate('Login')} style={styles.buttonNavbarText}>
            <Icon style={styles.footerIconInactive} name='commenting'/>
            <Text style={styles.footerTextInactive}>Pesan</Text>
          </Button>
          <Button transparent onPress={() =>  this.props.navigation.navigate('Direktori')} style={styles.buttonNavbarText}>
            <Icon style={styles.footerIconInactive} name='list'/>
            <Text style={styles.footerTextInactive}>Direktori</Text>
          </Button>
          <Button transparent onPress={() => this.drawer._root.open()} style={styles.buttonNavbarText}>
            <Icon style={styles.footerIconInactive} name='ellipsis-h'/>
            <Text style={styles.footerTextInactive}>Lainnya</Text>
          </Button>
          {/*
          <Button vertical onPress={() =>  this.props.navigation.navigate('Beranda')}>
            <Icon name="home" style={{fontSize:20,color:'#fff'}}/>
            <Text style={{color:'#fff',fontFamily:'Montserrat'}}>Beranda</Text>
          </Button>
          <Button vertical onPress={() =>  this.props.navigation.navigate('Informasi')}>
            <Icon name="file" style={{fontSize:20}}/>
            <Text style={{color:'#fff',fontFamily:'Montserrat'}}>Informasi</Text>
          </Button>
          <Button vertical onPress={() =>  this.props.navigation.navigate('ChatList')}>
            <Icon active name="commenting" style={{fontSize:20}}/>
            <Text style={{color:'#fff',fontFamily:'Montserrat'}}>Pesan</Text>
          </Button>
          <Button vertical onPress={() =>  this.props.navigation.navigate('CancerSupport')}>
            <Icon name="list" style={{fontSize:20}}/>
            <Text style={{color:'#fff',fontFamily:'Montserrat'}}>Direktori</Text>
          </Button>
          <Button vertical onPress={() =>  this.props.navigation.navigate('Learn')}>
            <Icon name="ellipsis-h" style={{fontSize:20}}/>
            <Text style={{color:'#fff',fontFamily:'Montserrat'}}>Lainnya</Text>
          </Button>
          */}
        </Body>
      </Footer>
      /*
        <Footer noShadow style={styles.navBarFooterStyle}>
                <Body style={styles.bodySpaceBetween}>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('News')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='home'/>
                    <Text style={styles.footerTextInactive}>Beranda</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('Learn')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='info-circle'/>
                    <Text style={styles.footerTextInactive}>Informasi</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('ChatList')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconActive} name='comments'/>
                    <Text style={styles.footerTextActive}>Pesan</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('CancerSupport')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='location-arrow'/>
                    <Text style={styles.footerTextInactive}>Direktori</Text>
                  </Button>
                  <Button transparent onPress={() => this.drawer._root.open()} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='ellipsis-h'/>
                    <Text style={styles.footerTextInactive}>Lainnya</Text>
                  </Button>
                </Body>
              </Footer>
              */
    );
  }

  logout(){
    AsyncStorage.setItem('isLoggedIn', 'false');
    AsyncStorage.setItem('username', '');
    AsyncStorage.setItem('role', '');
    this.props.navigation.navigate('Login');
  }
}