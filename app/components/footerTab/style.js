const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
  navBarFooterStyle:{
		backgroundColor:'#F9F3F3',
    opacity:0.95,
		borderBottomWidth: 0.5,
		borderBottomColor:'#B2B2B2',
		height: 60
	},
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonNavbarText:{
		flexDirection: 'column',
	},
  buttonNavbarTextActive:{
    flexDirection: 'column'    
  },
	footerIconInactive:{
		color: '#7e7e7e',
    	fontSize: 22,
	},
	footerIconActive:{
		color: '#fff',
    fontSize: 22,
    fontFamily:'Montserrat'
	},
	footerTextInactive:{
		color: '#7e7e7e',
		fontSize: 10,
		fontFamily: 'Montserrat',
	},
	footerTextActive:{
		color: '#fff',
		fontSize: 10,
		fontFamily: 'Montserrat',
	},
	thumbnailSize:{
		height:30,
		width:30,
		borderRadius:15,
	},
	thumbnailMargin:{
		marginTop: 5,
		marginBottom: 5,
	}

}