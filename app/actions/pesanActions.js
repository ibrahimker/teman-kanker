export function loadPesan(userId) {
    return function(dispatch) {
        return fetch('https://temankanker.com/api/Rooms/getAllRoom?userId=' + userId)
        .then(response => response.json())
        .then(data => {dispatch(loadPesanSuccess(data))})
        .catch(error => {throw(error)})
    }
}

export function incrementUnreadCounter(roomId, count, userId) {
    return function(dispatch) {
        return fetch('https://temankanker.com/api/Rooms/' + roomId, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "unread": count + 1
            })
        })
        .then(data => {dispatch(loadPesan(userId))})
        .catch(error => {throw(error)})
    }
}

export function resetUnreadCounter(roomId, userId) {
    console.log('update')
    return function(dispatch) {
        return fetch('https://temankanker.com/api/Rooms/' + roomId, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "unread": 0
            })
        })
        .then(data => {console.log(data)})
        .catch(error => {throw(error)})
    }
}

export function setChatRoomStatus(roomId, inChatRoom) {
    return (dispatch, getState) => {
        const state = getState()
        dispatch(setChatRoom(state.pesanReducer, roomId, inChatRoom))
    }
}

export function changeUnreadAll(currentCount) {
    return (dispatch, getState) => {
        const state = getState()
        dispatch(changeUnreadAllData(state.pesanReducer, currentCount))
    }
}

export function loadPesanSuccess(data) {
    return {
        type: 'LOAD_PESAN_SUCCESS',
        data
    }
}

export function setChatRoom(currentState, roomId, inChatRoom) {
    return {
        type: 'SET_CHATROOM_STATUS',
        currentState,
        roomId,
        inChatRoom
    }
}

export function changeUnreadAllData(currentState, currentCount) {
    return {
        type: 'CHANGE_UNREAD_ALL',
        currentState,
        currentCount
    }
}