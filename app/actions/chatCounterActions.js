export function increment() {
  return {
    type: 'increment'
  };
}

export function reset() {
  return {
    type: 'reset'
  };
}

export function changeText(text) {
    return {
        type: 'changeText',
        text
    }
}

export function setCurrentScreen(screen) {
    return {
        type: 'setCurrentScreen',
        screen
    }
}