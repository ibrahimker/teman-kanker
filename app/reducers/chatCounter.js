import React from 'react';

const initialState = {
    readCount: 0,
    text: '',
    currentScreen: 'Beranda'
}

export default function chatCounter(state = initialState, action = {}) {
    switch(action.type) {
        case 'increment':
            return { ...state, readCount: state.readCount + 1 }
        case 'reset': 
            return { ...state, readCount: 0 }
        case 'changeText':
            return { ...state, text: action.text }
        case 'setCurrentScreen':
            return { ...state, currentScreen: action.screen}
        default:
            return state
    }
}