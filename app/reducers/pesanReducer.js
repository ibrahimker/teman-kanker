import React from 'react';

const initialState = {
    refreshing: false,
    dataLoaded: false,
    unreadAll: 0,
    inChatRoom: false,
    currentChatRoom: null
}

export default function pesanReducer(state = initialState, action = {}) {
    switch(action.type) {
        case 'LOAD_PESAN_SUCCESS':

            let room = action.data
            let roomList = []
            let roomDoctor = []
            let unreadAll = 0

            room.forEach((element) => {
                unreadAll = unreadAll + element.unread
            });

            roomList = room.filter(function(item){
                return item.userOppositeProfile.role != 'Dokter';
            });

            roomDoctor = room.filter(function(item){
                return item.userOppositeProfile.role === 'Dokter';
            });

            return { ...state, room: room, roomList: roomList, roomDoctor: roomDoctor, dataLoaded: true, refreshing: false, unreadAll: unreadAll }
        case 'CHANGE_UNREAD_ALL':
            
            let data = action.currentState
            let newUnrealAllCount = data.unreadAll - action.currentCount

            return { ...data, unreadAll: newUnrealAllCount }
        case 'SET_CHATROOM_STATUS':
            let datas = action.currentState
            return { ...datas, inChatRoom: action.inChatRoom, currentChatRoom: action.roomId }
        default:
            return state
    }
}