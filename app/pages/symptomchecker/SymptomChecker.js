import React, { Component } from 'react';
import {Text,View,Drawer,Button,Icon,Container,Content,Header,Left,Body,Right,Title} from 'native-base';

import {GiftedChat, Actions, Bubble} from 'react-native-gifted-chat';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class SymptompCheckerScreen extends Component {
  static navigationOptions = {
    title: 'Deteksi Dini',
    header:null
  };

  state = {
    messages: [],
  };

  componentWillMount() {
    this.setState({
      messages: [
        {
          _id: 1,
          text: 'Selamat datang di deteksi dini. Silahkan tuliskan keluhan anda',
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'Teman Kanker',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        }
      ],
    });
  }

  onSend(messages = []) {
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));

    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: "Mohon maaf sistem ini sedang dalam pengembangan dan tidak dapat digunakan. Silahkan mencari pertolongan di dokter terdekat",
          createdAt: new Date(),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        }),
      };
    });
  }

  onReceive(text) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(),
          user: {
            _id: 1,
            name: 'React Native',
            avatar: 'https://facebook.github.io/react/img/logo_og.png',
          },
        }),
      };
    });
  }

  render() {
    StatusBar.setBarStyle('dark-content', true);
    openDrawer = () => {
      this.drawer._root.open()
    };
    closeDrawer = () =>{
      this.drawer._root.close()
    };
    pressDrawer = () =>{

    }
    return (
        <Container>
            <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='ios-arrow-back'/>
                  </Button>
              </Left>
              <Body style={styles.navBarCentered}>
                <Title style={styles.navBarTitle}>Deteksi Dini</Title>
              </Body>
              <Right style={styles.navBarWidth}/>
            </Header>
              <GiftedChat
                messages={this.state.messages}
                onSend={(messages) => this.onSend(messages)}
                user={{
                  _id: 1,
                }}
              />
        </Container>
      );
    }
  }
