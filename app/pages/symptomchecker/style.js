const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#B2B2B2'
	},
	navBarTitle:{
		fontFamily: 'Kreon-Regular',
		fontWeight: '400',
		color: '#3B5998',
		fontSize: 24,
	},
	navBarStyle:{
		backgroundColor:'#ffffff',
		height: 70,
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	}

}