import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Tabs,Tab,Badge,H1,Form,Item,Input,Picker,ActionSheet,List,ListItem,Spinner,Toast} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Modal, KeyboardAvoidingView, TouchableHighlight, Image, Platform } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import moment from 'moment';
import DatePicker from 'react-native-datepicker'
import Sidebar from '../../components/sidebar/Sidebar.js'
import images from '../../config/images.js';
import styles from './style.js';

// import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';

var ImagePicker = require('react-native-image-picker');

export default class Registration_2 extends Component {
  static navigationOptions = {
    title: 'Learn',
    header:null
  };

  
  constructor(props) {
    super(props);
    
    this.state = {
      selected2: undefined
    };
  }
  onValueChange2(value) {
    this.setState({
      sex: value
    });
  }

  componentWillMount() {
    this.state =  {
      fullname : '',
      email: '',
      birthday: '',
      password: '',
      password2: '',
      location: '',
      sex: '',
      kota: [],
      filteredKota: [],
      kotaField: '',
      showKota: false,
      showGender: false,
      kotaLoaded: false,
      checkEmail: false,
      emailAvailable: null
    }

    fetch('https://raw.githubusercontent.com/nolimitid/nama-tempat-indonesia/master/json/kota-kabupaten.json')
    .then((response) => {
      console.log(response);
      return response.json();
    })
    .then((data) => {
      this.setState({ kota: data, filteredKota: data, kotaLoaded: true });
    })
    .catch(error => {
        this.setState({ kotaLoaded: false, showKota: false });
        Toast.show({
          text: 'Tejadi kesalahan pada proses pengambilan data kota',
          position: 'top',
          type:'danger',
          duration: 2000
        });
    })
  }

  continueStep() {
    this.props.navigation.navigate('Registration_3', {role: this.props.navigation.state.params.role,fullname:this.state.fullname,sex:this.state.sex,location:this.state.location,email:this.state.email,birthday:this.state.birthday,password:this.state.password,photo: this.state.userProfPict});
  }

  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  }

  renderPickKota() {
    if(this.state.kotaLoaded){
      return (
        // <Picker
        //   style={{ flex: 1 }}
        //   textStyle={[styles.placeholderStyles, {marginLeft: -8}]}
        //   mode="dropdown"
        //   placeholder="Tipe kanker pasien*"
        //   selectedValue={this.state.cancertype}
        //   onValueChange={this.onValueChange2.bind(this)}
        // >
        //   {this.state.dataKanker.map(valuez => {
        //     return (<Item label={valuez.value} value={valuez.value} />)
        //   })}
        // </Picker>
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showKota}
            onRequestClose={() => {alert("Modal has been closed.")}}
            >
            <View style={{marginTop: 50, backgroundColor: '#489DFF', height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
            <View style={{padding: 10}}>
              <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih kota</Text>

              <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
                this.setState({showKota: false})
              }}>
                <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
              </Button>

              <Item rounded style={styles.input}>
                <Icon style={[styles.inputIcon, {color: 'white'}]} active name='search' />
                <Input value={this.state.kotaField} style={styles.placeholderStyles} type="text" placeholder='Cari kota' placeholderTextColor='#B22B2' onChangeText={(location) => {this.filterKota(location); this.setState({location: location})}}/>
              </Item>

              <View style={{marginBottom: 80}}>
                  <List
                    dataArray={this.state.filteredKota}
                    renderRow={(item) =>
                      <ListItem key={item} style={{borderColor: 'transparent'}} onPress={() => this.pickKota(item)}>
                        <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{item}</Text>
                      </ListItem>
                    }>
                  </List>
              </View>

            </View>
            </View>
          </Modal>
      )
    }
    else {
      return (<Spinner color='#B2B2B2'/>);
    }
  }

  renderPickGender() {
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showGender}
          onRequestClose={() => {alert("Modal has been closed.")}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10}}>
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih jenis kelamin</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              this.setState({showGender: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View>
                <List>
                    <ListItem key={'Pria'} style={{borderColor: 'transparent'}} onPress={() => this.setState({sex: 'Pria', showGender: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Pria</Text>
                    </ListItem>
                    <ListItem key={'Wanita'} style={{borderColor: 'transparent'}} onPress={() => this.setState({sex: 'Wanita', showGender: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Wanita</Text>
                    </ListItem>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  filterKota(kota) {

    this.setState({ kotaField: kota });

    this.setState({ showKota: true });

    let filteredKota = this.state.kota.filter(value => {
      value = value.toLowerCase();
      kota = kota.toLowerCase();
      return value.indexOf(kota) > -1;
    });

    this.setState({ filteredKota: filteredKota });

    console.log(filteredKota);
  }

  pickKota(kota) {
    this.setState({kotaField: kota});
    this.setState({ showKota: false });
    this.setState({ location: kota });
    console.log(kota);
  }

  changeProfPict(event){
    console.log("hi");
    ImagePicker.showImagePicker({maxHeight: 300, maxWidth: 300} ,(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          userProfPict: source,
          dataLoaded: false
        });

      }
    });
  }

  checkEmail(e) {
    console.log(e.nativeEvent.text);

    this.setState({checkEmail: true, emailAvailable: null});
    let email = e.nativeEvent.text;

    fetch('https://temankanker.com/api/UserKankers/count?where={"username": "'+ email +'"}')
    .then(response => {
      return response.json();
    })
    .then(data => {
      console.log(data);
      if(data.count > 0) {
        this.setState({emailAvailable: false});
      }
      else {
        this.setState({emailAvailable: true});
      }

      this.setState({checkEmail: false});
    })
    .catch(error => {
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
  }

  render() {
      StatusBar.setBarStyle('light-content', true);
        return (
          <Container>
            <Image source={images.backgroundHome} style={styles.notesBackground}>
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                      <Icon style={styles.navBarColor} name='arrow-left'/>
                    </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Registrasi</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>     
              
                <Content>
                  <Grid style={styles.progressBarArea}>
                    <Row>
                      <Col>
                        <View style={styles.progressBarActive}>
                          <Icon style={styles.progressBarActiveText} name='check'/>
                        </View>
                      </Col>
                      <Col>
                        <View style={styles.progressBarActive}>
                          <Text style={styles.progressBarActiveText}>2</Text>
                        </View>
                      </Col>
                      <Col>
                        <View style={styles.progressBar}>
                          <Text style={styles.progressBarText}>3</Text>
                        </View>
                      </Col>
                    </Row>
                    <Row>
                      <Col style={{flex: 1, alignItems: 'center'}}>
                        <TouchableHighlight rounded onPress={() => this.changeProfPict()} style={{height: 100, width: 100, marginTop:30, borderRadius: 50}} >
                          <Image style={{height: 100, width: 100, borderRadius: 50}} source={this.state.userProfPict ? this.state.userProfPict : images.profilePictDefault} defaultSource={require('../../images/defaultprofpic-empty.png')} indicator={Progress.Circle}/>
                        </TouchableHighlight>
                        <Text style={{ flex: 1, width: null, textAlign: 'center', fontFamily: 'Montserrat', fontSize: 14, marginTop: 10 }}>Unggah foto diri anda</Text>
                      </Col>
                    </Row>
                    <Row style={[styles.buttonPadding, {marginTop: 20}]}>
                      <Content>
                        <Form style={{marginTop:-10}}>
                            <Item rounded style={styles.input}>
                                <Icon style={styles.inputIcon} active name='user' />
                                <Input style={styles.placeholderStyles} type="text" placeholder='Nama Lengkap*' placeholderTextColor='#B2B2B2' onChangeText={(fullname) => this.setState({fullname: fullname})}/>
                            </Item>
                            <Item rounded style={styles.input} onPress={() => this.setState({ showGender: true })}>
                                <Icon style={styles.inputIcon} active name='intersex' />
                                <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.sex === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.sex === '' ? 'Pilih jenis kelamin*' : this.state.sex}</Text>
                            </Item>
                            {this.state.showGender && (
                                    this.renderPickGender()
                            )}
                            <Item rounded style={styles.input} onPress={() => this.setState({ showKota: true })}>
                                <Icon style={styles.inputIcon} active name='map-marker' />
                                <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, color: 'grey', fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.location === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.location === '' ? 'Pilih kota*' : this.state.location}</Text>
                            </Item>
                            {this.state.showKota && (
                                    this.renderPickKota()
                            )}
                            
                            <Item rounded style={styles.input}>
                                <Icon style={styles.inputIcon} active name='envelope' />
                                <Input keyboardType={'email-address'} onEndEditing={(value) => this.checkEmail(value)} style={styles.placeholderStyles} placeholder='Email*' placeholderTextColor='#B2B2B2' onChangeText={(email) => this.setState({email: email})}/>
                                <Spinner animating={true} style={{position: 'absolute', right: 15, opacity: this.state.checkEmail ? 1.0 : 0.0}} color="#489DFF" size={Platform.OS === 'ios' ? 'small' : 25}/>
                                {this.state.emailAvailable !== null && ( this.state.emailAvailable ? (
                                    <Icon style={[styles.inputIcon, {color: 'green'}]} active name='check' />
                                ) : (
                                    <Icon style={[styles.inputIcon, {color: 'red'}]} active name='close' /> 
                                ))}
                            </Item>
                            <Item rounded style={styles.input}>
                                <Icon style={styles.inputIcon} active name='lock' />
                                <Input style={styles.placeholderStyles} secureTextEntry={true} placeholder='Kata Sandi*' placeholderTextColor='#B2B2B2' onChangeText={(password) => this.setState({password: password})}/>
                                {/*<Icon style={{marginRight: 20, marginLeft: -60, color: '#3ebb64'}} active name='eye' />*/}
                            </Item>
                            <Item rounded style={styles.input}>
                                <Icon style={styles.inputIcon} active name='lock' />
                                <Input style={styles.placeholderStyles} secureTextEntry={true} placeholder='Konfirmasi Kata Sandi*' placeholderTextColor='#B2B2B2' onChangeText={(password2) => this.setState({password2: password2})}/>
                                {/*<Icon style={{marginRight: 20, marginLeft: -60, color: '#3ebb64'}} active name='eye' />*/}
                            </Item>
                            <Item rounded style={styles.input}>
                              <Icon style={styles.inputIcon} active name='calendar' />
                              <Input disabled/>
                              <DatePicker
                                style={{width: 200, backgroundColor: 'transparent', position: 'absolute'}}
                                date={this.state.date}
                                mode="date"
                                format="YYYY-MM-DD"
                                minDate="1900-01-01"
                                iconComponent={(<Icon style={styles.inputIcon2} active name='calendar' />)}
                                maxDate={moment().format('YYYY-MM-DD')}
                                placeholder="Tanggal Lahir*"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                  dateInput: {
                                    backgroundColor: 'transparent',
                                    borderWidth: 0,
                                    marginLeft: 20
                                  },
                                  placeholderText: {
                                    fontFamily: 'Montserrat-Regular',
                                    fontSize: 18,
                                    color: '#B2B2B2',
                                    marginLeft: 20
                                  },
                                  dateText: {
                                    fontFamily: 'Montserrat-Regular',
                                    fontSize: 18,
                                    color: 'black',
                                    marginLeft: 15
                                  }
                                  // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({date: date, birthday: date})}}
                              />
                            </Item>
                        </Form>
                        {((this.state.email != '' && this.state.emailAvailable === true) && this.state.fullname != '' && this.state.birthday != '' && this.state.sex != '' && this.state.location != '' && this.state.password != '' && this.state.password2 != '' && (this.state.password == this.state.password2)) ? (
                            <Button block rounded
                            style={styles.buttonRegis}
                            onPress={() => this.continueStep()}>
                              <Text style={styles.montserratText}>Selanjutnya</Text>
                            </Button>
                        ) : (
                          <Button block rounded
                            style={[styles.buttonRegis, {backgroundColor: 'rgba(255, 170, 57, 0.3)'}]}>
                              <Text style={styles.montserratText}>Selanjutnya</Text>
                            </Button>
                        )}
                      </Content>
                    </Row>
                  </Grid>
                </Content> 
              </Image>   

              {(this.state.showKota || this.state.showGender) && (
                  <View style={[styles.loadingOverlay, {backgroundColor: 'black', opacity: 0.4}]}></View>
              )}

          </Container>
        );
    }
}