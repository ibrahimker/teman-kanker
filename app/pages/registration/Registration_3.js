import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Tabs,Tab,Badge,H1,Form,Item,Input,Toast,Spinner,List,ListItem} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, Alert, AsyncStorage, Picker, Modal} from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

import Sidebar from '../../components/sidebar/Sidebar.js';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import images from '../../config/images.js';
import styles from './style.js';

const APIENDPOINT = 'https://temankanker.com/api/';

export default class Registration_3 extends Component {
  static navigationOptions = {
    title: 'Learn',
    header:null
  };


  componentWillMount() {
    this.state = {
      cancertype : '',
      cancer: '',
      group:'',
      illnessStartDate:'',
      illnessEndDate:'',
      hospital:'',
      speciality:'',
      contact:'',
      doctor:'',
      registering: false,
      dataKanker: [],
      dataLoaded: false,
      showTypeKanker: false,
      showYearList: false,
      yearsList: []
    }

    const { params } = this.props.navigation.state;

    let years = [];
    let currYear = new Date().getFullYear();
    let startYear = 1900;

    while( currYear >= startYear ) {
      years.push(currYear--);
    }

    

    fetch(APIENDPOINT+'/MasterData?filter={"where":{"param":"Kanker"}}')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      this.setState({ dataKanker: data, dataLoaded: true, cancertype: '', illnessStartDate:'', yearsList: years });
    });

  }

  onValueChange(value) {
    this.setState({
      illnessStartDate: value
    });
  }

  onValueChange2(value) {
    this.setState({
      cancertype: value
    });
  }

  pickKanker(kanker) {
    this.setState({cancertype: kanker, showTypeKanker: false});
  }

  pickYear(year, type) {
    type === 'start' ? this.setState({illnessStartDate: year, showYearList: false}) : this.setState({illnessEndDate: year, showYearList2: false})
  }

  renderPickGroup() {
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showGroup}
          onRequestClose={() => {this.setState({showGroup: false})}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10}}>
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih Jenis Kanker</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              this.setState({showGroup: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View>
                <List>
                    <ListItem key={'Pria'} style={{borderColor: 'transparent'}} onPress={() => this.setState({group: 'Anak', showGroup: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Anak</Text>
                    </ListItem>
                    <ListItem key={'Wanita'} style={{borderColor: 'transparent'}} onPress={() => this.setState({group: 'Dewasa', showGroup: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Dewasa</Text>
                    </ListItem>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  renderPickGroupLainnya() {
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showGroup}
          onRequestClose={() => {this.setState({showGroup: false})}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10}}>
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih Jenis Relawan</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              this.setState({showGroup: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View>
                <List>
                    <ListItem key={'Pria'} style={{borderColor: 'transparent'}} onPress={() => this.setState({group: 'Relawan', showGroup: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Relawan</Text>
                    </ListItem>
                    <ListItem key={'Wanita'} style={{borderColor: 'transparent'}} onPress={() => this.setState({group: 'Simpatisan', showGroup: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Simpatisan</Text>
                    </ListItem>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  renderCancerType() {
      return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showTypeKanker}
            onRequestClose={() => {this.setState({showTypeKanker: false})}}
            >
            <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, maxHeight: 300, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
            <View style={{padding: 10, paddingBottom: 0}}>
              <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih tipe kanker</Text>

              <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
                this.setState({showTypeKanker: false})
              }}>
                <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
              </Button>
              
              {this.state.dataLoaded ? (
                  
                      <List
                        style={{height: 200}}
                        dataArray={this.state.dataKanker}
                        renderRow={(item) =>
                          <ListItem key={item.id} style={{borderColor: 'transparent'}} onPress={() => this.pickKanker(item.value)}>
                            <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{item.value}</Text>
                          </ListItem>
                        }>
                      </List>
                  
              ) : (<Spinner color='#B2B2B2'/>)}
              

            </View>
            </View>
          </Modal>
      )
  }

  renderPickYear(type) {

      return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={type === 'start' ? this.state.showYearList : this.state.showYearList2}
          onRequestClose={() => {this.setState({showYearList: false})}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, maxHeight: 300, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10, paddingBottom: 0}}>
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>{type === 'start' ? 'Pilih tahun mulai sakit' : 'Pilih tahun sembuh sakit'}</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              type === 'start' ? this.setState({showYearList: false}) : this.setState({showYearList2: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View style={{height: 200}}>
                <List
                  dataArray={this.state.yearsList}
                  renderRow={(item) =>
                    <ListItem key={item} style={{borderColor: 'transparent'}} onPress={() => this.pickYear(item, type)}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{item}</Text>
                    </ListItem>
                  }>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  renderForm() {
    if(this.props.navigation.state.params.role == 'Pasien') {
        return (
            <Form>
                <Item rounded style={styles.input} onPress={() => this.setState({ showTypeKanker: true })} >
                    <Icon style={styles.inputIcon} active name='heart' />
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.cancertype === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.cancertype === '' ? 'Tipe kanker pasien*' : this.state.cancertype}</Text>
                    {/* <Input style={styles.placeholderStyles} type="text" disabled placeholder='Pria/Wanita' placeholderTextColor='#B2B2B2' onChangeText={(sex) => this.setState({sex})}/> */}
                </Item>
                {this.state.showTypeKanker && (
                        this.renderCancerType()
                )}
                <Item rounded style={styles.input} onPress={() => this.setState({ showGroup: true })}>
                    <Icon style={styles.inputIcon} active name='intersex' />
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.group === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.group === '' ? 'Rentang Usia Pasien*' : this.state.group}</Text>
                </Item>
                {this.state.showGroup && (
                        this.renderPickGroup()
                )}
                <Item rounded style={styles.input} onPress={() => this.setState({ showYearList: true })}>
                    <Icon style={styles.inputIcon} active name='calendar' />
                    {/* <Input style={styles.placeholderStyles} type="text" placeholder='Tahun Mulai Sakit' placeholderTextColor='#B2B2B2' onChangeText={(illnessStartDate) => this.setState({illnessStartDate: illnessStartDate})}/> */}
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.illnessStartDate === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.illnessStartDate === '' ? 'Tahun Mulai Sakit' : this.state.illnessStartDate}</Text>
                </Item>
                {this.state.showYearList && (
                        this.renderPickYear('start')
                )}
                <Item rounded style={styles.input}>
                    <Icon style={styles.inputIcon} active name='home' />
                    <Input style={styles.placeholderStyles} placeholder='Rumah Sakit Pasien' placeholderTextColor='#B2B2B2' onChangeText={(hospital) => this.setState({hospital: hospital})}/>
                </Item>
                <Item rounded style={styles.input}>
                    <Icon style={styles.inputIcon} active name='stethoscope' />
                    <Input style={styles.placeholderStyles} type="text" placeholder='Dokter Pasien' placeholderTextColor='#B2B2B2' onChangeText={(doctor) => this.setState({doctor: doctor})}/>
                </Item>
            </Form>
        );
    }
    else if(this.props.navigation.state.params.role == 'Survivor') {
        return (
            <Form>
                <Item rounded style={styles.input} onPress={() => this.setState({ showTypeKanker: true })} >
                    <Icon style={styles.inputIcon} active name='heart' />
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.cancertype === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.cancertype === '' ? 'Tipe kanker pasien*' : this.state.cancertype}</Text>
                    {/* <Input style={styles.placeholderStyles} type="text" disabled placeholder='Pria/Wanita' placeholderTextColor='#B2B2B2' onChangeText={(sex) => this.setState({sex})}/> */}
                </Item>
                {this.state.showTypeKanker && (
                        this.renderCancerType()
                )}
                <Item rounded style={styles.input} onPress={() => this.setState({ showGroup: true })}>
                    <Icon style={styles.inputIcon} active name='intersex' />
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.group === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.group === '' ? 'Rentang Usia Saat Sakit*' : this.state.group}</Text>
                </Item>
                {this.state.showGroup && (
                        this.renderPickGroup()
                )}
                <Item rounded style={styles.input} onPress={() => this.setState({ showYearList: true })}>
                    <Icon style={styles.inputIcon} active name='calendar' />
                    {/* <Input style={styles.placeholderStyles} type="text" placeholder='Tahun Mulai Sakit' placeholderTextColor='#B2B2B2' onChangeText={(illnessStartDate) => this.setState({illnessStartDate: illnessStartDate})}/> */}
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.illnessStartDate === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.illnessStartDate === '' ? 'Tahun Mulai Sakit' : this.state.illnessStartDate}</Text>
                </Item>
                {this.state.showYearList && (
                        this.renderPickYear('start')
                )}
                <Item rounded style={styles.input} onPress={() => this.setState({ showYearList2: true })}>
                    <Icon style={styles.inputIcon} active name='calendar' />
                    {/* <Input style={styles.placeholderStyles} type="text" placeholder='Tahun Mulai Sakit' placeholderTextColor='#B2B2B2' onChangeText={(illnessStartDate) => this.setState({illnessStartDate: illnessStartDate})}/> */}
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.illnessEndDate === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.illnessEndDate === '' ? 'Tahun Sembuh Sakit' : this.state.illnessEndDate}</Text>
                </Item>
                {this.state.showYearList2 && (
                        this.renderPickYear('end')
                )}
                {/* <Item rounded style={styles.input}>
                    <Icon style={styles.inputIcon} active name='calendar' />
                    <Input style={styles.placeholderStyles} type="text" placeholder='Tahun Sembuh Sakit' placeholderTextColor='#B2B2B2' onChangeText={(illnessEndDate) => this.setState({illnessEndDate: illnessEndDate})}/>
                </Item> */}
                <Item rounded style={styles.input}>
                    <Icon style={styles.inputIcon} active name='stethoscope' />
                    <Input style={styles.placeholderStyles} type="text" placeholder='Dokter' placeholderTextColor='#B2B2B2' onChangeText={(doctor) => this.setState({doctor: doctor})}/>
                </Item>
            </Form>
        );
    }
    else if(this.props.navigation.state.params.role == 'Lainnya') {
        return (
            <Form>
                <Item rounded style={styles.input} onPress={() => this.setState({ showGroup: true })}>
                    <Icon style={styles.inputIcon} active name='intersex' />
                    <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.group === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.group === '' ? 'Pilih tipe relawan*' : this.state.group}</Text>
                </Item>
                {this.state.showGroup && (
                    this.renderPickGroupLainnya()
                )}
            </Form>
        );
    }
  }

  register(){
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let role = params.role;
    let fullname = params.fullname;
    let location = params.location;
    let userProfPict = params.photo;
    let sex = true;

    this.setState({registering: true});

    if(params.sex == "Perempuan" || params.sex == "perempuan" || params.sex == "Wanita" || params.sex == "wanita"){
      sex = false;
    }
    let email = params.email;
    let birthday = moment(params.birthday).format();
    let birthday2 = new Date(birthday).toJSON()
    let password = params.password;
    let cancertype = this.state.cancertype;
    let illnessStartDate = 0;
    let illnessEndDate = 0;
    let hospital = this.state.hospital;
    let speciality = this.state.speciality;
    let doctor = this.state.doctor;
    let group = this.state.group;
    if(role == "Pasien"){
      illnessStartDate = this.state.illnessStartDate;
      illnessEndDate = 0; 
    }
    else if(role == "Survivor"){
      illnessStartDate = this.state.illnessStartDate;
      illnessEndDate = this.state.illnessEndDate; 
    }
    else{

    }    

    fetch(APIENDPOINT+'/UserKankers/register', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "username": email,
        "password":password,
        "name":fullname,
        "sex":sex,
        "city":location,
        "birthdate":birthday2,
        "role":role,
        "type":cancertype,
        "duration_start":illnessStartDate,
        "duration_end":illnessEndDate,
        "hospital":hospital,
        "doctor":doctor,
        "group":group,
        "photo":"/api/Storages/profpict/download/defaultprofpic-empty.png"
      })
    })
    .then((response) => { 
      return response.json();
    })
    .then((data) => {
      console.log(data);
      if(data.hasOwnProperty('error')){
        Toast.show({
          text: 'Register Gagal!',
          position: 'bottom',
          type:'alert',
          duration: 2000
        });
      }
      else{

        if(params.photo) {
            let formdata = new FormData();

            formdata.append("image", {uri: params.photo.uri, name: data.id + '.jpg', type: 'multipart/form-data'});

            console.log(formdata);

            fetch(APIENDPOINT+'/Storages/profpict/upload',{
              method: 'POST',
              headers: {
                'Content-Type': 'multipart/form-data',
              },
              body: formdata
            })
            .then((response) => response.json())
            .then((dataz) => {
              console.log(dataz);
              fetch(APIENDPOINT+'/UserKankers/' + data.id,{
                method: 'PUT',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  "photo": '/api/Storages/profpict/download/' + data.id + '.jpg'
                })
              })
              .then((response) => response.json())
              .then((data) => {
                console.log(data);
                fetch(APIENDPOINT+'/UserKankers/getRegistrationToken?userId=' + data.id)
                .then((response) => response.json())
                .then((data) => {
                  setTimeout(() => {
                      Toast.show({
                        text: 'Silahkan cek email anda untuk konfirmasi',
                        position: 'bottom',
                        type:'success',
                        duration: 2000
                      });
                  }, 3000)
                })

                navigate('Tab');
                Toast.show({
                  text: 'Register Berhasil!',
                  position: 'bottom',
                  type:'success',
                  duration: 2000
                });
                AsyncStorage.setItem('isLoggedIn', 'true');
                AsyncStorage.setItem('username', email);
                AsyncStorage.setItem('userId', data.id.toString());
                AsyncStorage.setItem('role', role);
              })
            })
        }
        else {

          fetch(APIENDPOINT+'/UserKankers/getRegistrationToken?userId=' + data.id)
          .then((response) => response.json())
          .then((data) => {
            setTimeout(() => {
                Toast.show({
                  text: 'Silahkan cek email anda untuk konfirmasi',
                  position: 'bottom',
                  type:'success',
                  duration: 2000
                });
            }, 3000)
          })

          navigate('Tab');
          Toast.show({
            text: 'Register Berhasil!',
            position: 'bottom',
            type:'success',
            duration: 2000
          });
          AsyncStorage.setItem('isLoggedIn', 'true');
          AsyncStorage.setItem('username', email);
          AsyncStorage.setItem('userId', data.id.toString());
          AsyncStorage.setItem('role', role);
        }
      }
    });
  }

  renderLoading() {
    if(this.state.registering) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Mohon menunggu...</Text>
          <Spinner color="#489FFD"/>
        </View>
      );
    }
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    console.disableYellowBox = true;
    
    return (
      <Container>
        <Image source={images.backgroundHome} style={styles.notesBackground}>
          <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                      <Icon style={styles.navBarColor} name='arrow-left'/>
                    </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Registrasi</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>
          <Content>
            <Grid style={styles.progressBarArea}>
              <Row>
                <Col>
                  <View style={styles.progressBarActive}>
                    <Icon style={styles.progressBarActiveText} name='check'/>
                  </View>
                </Col>
                <Col>
                  <View style={styles.progressBarActive}>
                    <Icon style={styles.progressBarActiveText} name='check'/>
                  </View>
                </Col>
                <Col>
                  <View style={styles.progressBarActive}>
                    <Text style={styles.progressBarActiveText}>3</Text>
                  </View>
                </Col>
              </Row>
              <Row style={{flexDirection: 'column'}}>
                {this.props.navigation.state.params.role !== 'Lainnya' ? (
                    <Text style={styles.mainQuestion2}>Informasi Kanker</Text>
                ) : (<View style={{marginTop: 100}}></View>)}
                <Text style={styles.mainQuestionDesc}>Kami akan menjaga kerahasiaan semua informasi yang Anda berikan.</Text>
              </Row>
              <Row style={[styles.buttonPadding, {marginTop: 10}]}>
                <Content>
                  {this.renderForm()}
                  {this.props.navigation.state.params.role !== 'Lainnya' ? (
                      (this.state.cancertype !== '') ? (
                          <Button block rounded 
                          style={styles.buttonRegis}
                          onPress={() => this.register()}>
                            <Text style={styles.montserratText}>Daftar</Text>
                          </Button>
                      ) : (
                        <Button block rounded 
                        style={[styles.buttonRegis, {backgroundColor: 'rgba(255, 170, 57, 0.3)'}]}>
                          <Text style={styles.montserratText}>Daftar</Text>
                        </Button>
                      )
                  ) : (
                    <Button block rounded 
                      style={styles.buttonRegis}
                      onPress={() => this.register()}>
                        <Text style={styles.montserratText}>Daftar</Text>
                      </Button>
                  )}
                  
                </Content>
              </Row>
            </Grid>
          </Content> 
        </Image> 

        {this.renderLoading()}

        {(this.state.showYearList || this.state.showTypeKanker || this.state.showYearList2) && (
            <View style={[styles.loadingOverlay, {backgroundColor: 'black', opacity: 0.4}]}></View>
        )}

      </Container>
    );
  }
}