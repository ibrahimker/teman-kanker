import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Tabs,Tab,Badge,H1} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class Registration extends Component {
  static navigationOptions = {
    title: 'Learn',
    header:null
  };
   render() {
      StatusBar.setBarStyle('light-content', true);
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
      	this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
            <Image source={images.backgroundHome} style={styles.notesBackground}>
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                      <Icon style={styles.navBarColor} name='close'/>
                    </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Registrasi</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>     
                <Content>
                  <View>
                      <Grid style={styles.progressBarArea}>
                        <Row>
                          <Col>
                            <View style={styles.progressBarActive}>
                              <Text style={styles.progressBarActiveText}>1</Text>
                            </View>
                          </Col>
                          <Col>
                            <View style={styles.progressBar}>
                              <Text style={styles.progressBarText}>2</Text>
                            </View>
                          </Col>
                          <Col>
                            <View style={styles.progressBar}>
                              <Text style={styles.progressBarText}>3</Text>
                            </View>
                          </Col>
                        </Row>
                        <Row>
                          <Text style={styles.mainQuestion}>Siapakah Anda?</Text>
                        </Row>
                        <Row style={[styles.buttonPadding, {marginTop: 10}]}>
                          <Content>
                            <View style={{width: 320,alignSelf: 'center', marginBottom: -30, borderColor: 'grey',padding:15,borderRadius:4,borderStyle:'dashed',borderWidth:1,textAlign:'center',fontSize:12}}>
                                <Text style={{textAlign:'center',fontSize:14, fontFamily: 'Montserrat-Regular', paddingBottom: 20}}>Pilih pasien jika anda penderita kanker. Atau anda adalah Orang Tua/Pengasuh dari penderita kanker</Text>
                            </View>
                            <Button block rounded 
                            style={[styles.buttonRegis, {marginTop: 0}]}
                            onPress={() => this.props.navigation.navigate('Registration_2', {role: 'Pasien'})}>
                              <Text style={styles.montserratText}>Pasien</Text>
                            </Button>

                            <View style={{width: 320,alignSelf: 'center', marginBottom: -30,borderColor: 'grey',padding:15,borderRadius:4,borderStyle:'dashed',borderWidth:1,textAlign:'center',fontSize:12}}>
                                <Text style={{textAlign:'center',fontSize:14, fontFamily: 'Montserrat-Regular', paddingBottom: 20}}>Pilih survivor jika anda sudah sembuh dari penyakit kanker</Text>
                            </View>
                            <Button block rounded 
                            style={[styles.buttonRegis, {marginTop: 0}]}
                            onPress={() => this.props.navigation.navigate('Registration_2', {role: 'Survivor'})}>
                              <Text style={styles.montserratText}>Survivor</Text>
                            </Button>

                            <View style={{width: 320,alignSelf: 'center', marginBottom: -30,borderColor: 'grey',padding:15,borderRadius:4,borderStyle:'dashed',borderWidth:1,textAlign:'center',fontSize:12}}>
                                <Text style={{textAlign:'center',fontSize:14, fontFamily: 'Montserrat-Regular', paddingBottom: 20}}>Pilih lainnya jika anda diluar dua kategori diatas</Text>
                            </View>
                            <Button block rounded style={[styles.buttonRegis, {marginTop: 0}]}
                            onPress={() => this.props.navigation.navigate('Registration_2', {role: 'Lainnya'})}>
                              <Text style={styles.montserratText}>Lainnya</Text>
                            </Button>
                          </Content>
                        </Row>
                      </Grid>
                  </View>
                </Content> 
              </Image>         
              
          </Container>
        );
    }
}