const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
		maxWidth: 40
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	navBarColor:{
		color: '#fff',
		paddingLeft:5,
		fontSize:24
	},
	montserratText: {
		fontFamily: 'BalooThambi-Regular',
		fontSize: (Platform.OS === 'android') ? 20 : 16,
    	marginTop: (Platform.OS === 'android') ? -8 : 8,
	},
	header:{
		backgroundColor:'transparent',
		borderBottomWidth: 0,
		borderBottomColor:'#B2B2B2',
		height: 70,
	},
	navBarTitle:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#fff',
		fontSize: 24,
	},
	navBarStyle:{
		backgroundColor:'#489FFD',
		height: 70,
	},
	colorTabGrey:{
		color: '#B2B2B2',
		fontFamily: 'Montserrat',
		fontWeight: '400',
		fontSize: 12
	},
	colorTabNavy:{
		color: '#3B5998',
		fontFamily: 'Montserrat',
		fontWeight: '400',
		fontSize: 12
	},
	backgroundColorWhite:{
		backgroundColor: '#f44e4b',
	},
	backgroundImage: {
		flex: 1,
		width:null
	},
	progressBarArea:{
		height: null,
		width: null,
		alignItems: 'center',
		paddingTop:30
	},
	progressBarActive:{
		backgroundColor: '#79C948',
		flex: 1,
		width: 40,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		borderRadius: 50,
	},
	progressBar:{
		backgroundColor:'transparent',
		borderWidth: 2,
		borderColor: '#79C948',
		borderStyle:'dashed',
		flex: 1,
		width: 40,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'center',
		borderRadius: 50
	},
	progressBarActiveText:{
		color:'#FFF',
		fontSize: 14,
		fontFamily: 'Montserrat-Regular'
	},
	progressBarText:{
		color:'#79C948',
		fontSize: 14,
		fontFamily: 'Montserrat-Regular'
	},
	mainQuestion:{
		paddingTop:40,
		fontSize:25,
		color:'#489FFD',
		fontFamily:'BalooThambi-Regular'
	},
	mainQuestionNoPadding:{
		fontSize:36,
		color:'#489FFD',
		fontFamily:'BalooThambi-Regular'
	},
	mainQuestion2:{
		paddingTop:40,
		paddingBottom: 20,
		fontSize:36,
		textAlign: 'center',
		color:'#489FFD',
		fontFamily:'BalooThambi-Regular'
	},
	mainQuestionDesc: {
		textAlign: 'center',
		color: '#000',
		fontFamily: 'Montserrat-Regular',
		marginLeft: 40,
		marginRight: 40
	},
	buttonPadding:{
		marginTop: 50,
		width: 450,
		flex: 1,
		paddingRight: 60,
		paddingLeft: 60
	},
	notesBackground: {
		backgroundColor:'#fff',
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
	},
	input:{
		backgroundColor:'transparent',
		marginTop:10,
		marginBottom:5,
		paddingLeft:20,
		borderWidth:2,
		borderStyle:'dashed'
	},
	inputIcon: {
		color: '#489DFF',
		width: 35,
		fontSize: 20,
	},
	inputIcon2: {
		color: '#489DFF',
		width: 35,
		position: 'absolute',
		left: 20,
		fontSize: 20,
	},
	placeholderStyles: {
		fontFamily: 'Montserrat-Regular',
		fontSize: 18,
		color: 'black'
	},
	loadingOverlay: {
        opacity: 0.9,
        backgroundColor: 'white',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
	buttonRegis: {
		backgroundColor: '#ffaa39',
		height:50,
		marginTop: 30,
		marginBottom:30,
		shadowColor: '#e08723',
		shadowOffset: { width: 0, height: 2 },
		shadowOpacity: 0.3,
		shadowRadius: 2,
	}
}