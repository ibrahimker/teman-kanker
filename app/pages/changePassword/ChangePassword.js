import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Left,Right,Body, Title, Form, Item, Input, Toast} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class ChangePasswordScreen extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        konfirmasiPass: null,
        username: '',
        passwordLama: '',
        passwordBaru: '',
        passwordKonfirmasi: '',
        updating: false
    }

    constructor(props) {
        super(props)
    }

    componentWillMount() {
        AsyncStorage.getItem('username', (err, result) => {
            this.setState({ username: result });
        });
    }

    renderLoading() {
        if(this.state.updating) {
            return (
                <View style={styles.loadingOverlay}>
                    <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Mengubah Password...</Text>
                    <Spinner color="#489FFD"/>
                </View>
            );
        }
    }

    checkPass(password) {
        this.setState({passwordKonfirmasi: password})

        if(this.state.passwordBaru === password) {
            this.setState({ konfirmasiPass: true });
        }
        else {
            this.setState({ konfirmasiPass: false });
        }
    }

    ubahPassword() {

        let username = this.state.username;
        let oldpassword = this.state.passwordLama;
        let newpassword = this.state.passwordBaru;

        this.setState({ updating: true });

        fetch('https://temankanker.com/api/UserKankers/changePassword', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "username" : username,
                "oldpassword" : oldpassword,
                "newpassword" : newpassword
            })
        })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data)
            this.setState({ updating: false })
            this.props.navigation.goBack()
            Toast.show({
                text: 'Password telah diubah',
                position: 'bottom',
                type:'success',
                duration: 2000
            })
        })
        .catch(error => {
            this.setState({ updating: false })
            console.log(error)
            Toast.show({
                text: 'Terjadi kesalahan pada sistem kami',
                position: 'bottom',
                type:'danger',
                duration: 2000
            })
        })
    }

    render() {
        StatusBar.setBarStyle('light-content', true);
        return (
            <Container>
                <Header noShadow style={styles.navBarStyle}>
                    <Left style={styles.navBarWidth}>
                        <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                            <Icon style={styles.navBarColor} name='arrow-left'/>
                        </Button>
                    </Left>
                    <Body style={styles.navBarCentered}>
                        <Title style={styles.navBarTitle}>Ubah Password</Title>
                    </Body>
                        <Right style={styles.navBarWidth}>
                    </Right>
                </Header>
                <Image source={images.backgroundHome} style={styles.backgroundImage}>
                    <Content>
                            <View style={styles.form}>
                                <Text style={{textAlign: 'center', fontFamily: 'Montserrat', fontSize: 14, marginBottom: 20, lineHeight: 20}}>Masukkan kata sandi baru anda untuk mengubahnya</Text>
                                <Form>
                                    <Item rounded style={styles.input}>
                                        <Icon style={styles.inputIcon} active name='lock' />
                                        <Input style={styles.placeholderStyles} secureTextEntry={true} placeholder='Kata Sandi Lama' placeholderTextColor='#93938E' onChangeText={(password) => this.setState({passwordLama: password})}/>
                                    </Item>
                                    <Item rounded style={styles.input}>
                                        <Icon style={styles.inputIcon} active name='lock' />
                                        <Input style={styles.placeholderStyles} secureTextEntry={true} placeholder='Kata Sandi Baru' placeholderTextColor='#93938E' onFocus={() => this.setState({passwordKonfirmasi: '', konfirmasiPass: null})} onChangeText={(password) => this.setState({passwordBaru: password})}/>
                                    </Item>
                                    <Item rounded style={styles.input}>
                                        <Icon style={styles.inputIcon} active name='lock' />
                                        <Input style={styles.placeholderStyles} secureTextEntry={true} placeholder='Ketik Ulang Kata Sandi Baru' placeholderTextColor='#93938E' onChangeText={(password) => this.checkPass(password)}/>
                                        {/* <Spinner animating={true} style={{position: 'absolute', right: 15, opacity: this.state.checkPass ? 1.0 : 0.0}} color="#489DFF" size={'small'}/> */}
                                        {this.state.konfirmasiPass !== null && ( this.state.konfirmasiPass ? (
                                            <Icon style={[styles.inputIcon, {color: 'green'}]} active name='check' />
                                        ) : (
                                            <Icon style={[styles.inputIcon, {color: 'red'}]} active name='close' /> 
                                        ))}
                                    </Item>
                                </Form>
                                {this.state.passwordLama !== '' && this.state.passwordBaru !== '' && this.state.passwordKonfirmasi !== '' && this.state.konfirmasiPass ? (
                                    <Button rounded onPress={() => this.ubahPassword()} style={[styles.buttonLightBlue, {marginTop: 30}]} block>
                                        <Text style={styles.balooThambi}>Ubah</Text>
                                    </Button>
                                ) : (
                                    <Button rounded style={[styles.buttonLightBlue, {marginTop: 30, backgroundColor: 'rgba(73, 159, 253, 0.5)'}]} block>
                                        <Text style={styles.balooThambi}>Ubah</Text>
                                    </Button>
                                )}
                            </View>
                    </Content>
                </Image>
                {this.renderLoading()}
            </Container>
        )
    }
}
