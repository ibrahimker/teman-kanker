const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarStyle:{
		backgroundColor:'#ffffff',
		height: 70,
	},
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#B2B2B2'
	},
	navBarTitle:{
		fontFamily: 'Kreon-Regular',
		fontWeight: '400',
		color: '#3B5998',
		fontSize: 24
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	colorTabGrey:{
		color: '#B2B2B2',
		fontFamily: 'Montserrat-Regular',
		fontWeight: '400',
		fontSize: 12
	},
	colorTabNavy:{
		color: '#3B5998',
		fontFamily: 'Montserrat-Regular',
		fontWeight: '400',
		fontSize: 12
	},
	backgroundColorWhite:{
		backgroundColor: '#ffffff'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
  	},
  	contentStyle1:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginBottom: 20,
  		fontFamily: 'Kreon-Regular',
  		fontSize: 18,
  		lineHeight: 25,
  		color: '#3EBB64'
  	},
  	contentStyle2:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 20,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 28,
  		color: '#868686'
  	},
  	contentStyle2Point:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 0,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 28,
  		color: '#868686'
  	},
  	contentStyle1Quote:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginLeft: 60,
  		marginTop: 20,
  		marginBottom: 10,
  		fontFamily: 'Kreon-Regular',
  		fontSize: 18,
  		lineHeight: 25,
  		color: '#3B5998'
  	},
  	contentStyle2Quote:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: -10,
  		marginLeft: 60,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 28,
  		color: '#868686'
  	},

}