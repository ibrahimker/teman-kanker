import React, { Component } from 'react';
import {Text,View,Drawer,Button,Icon,Container,Content,Header,Left,Body,Right,Title,Tabs,Tab,ScrollableTab} from 'native-base';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class LearnDetailScreen extends Component {
  static navigationOptions = {
    title: 'Learn Detail',
    header:null
  };
   state = {
    messages: [],
    id: '',
    description_under: '',
    doctor_under: '',
    pict_under: '',
    type_under: '',
    title_under: '',
    doctor_under: '',
    date_under: '',
    update_under: '',
    data: '',
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    let title_news = params.title_news;
    fetch('https://temankanker.com/api/understanding_cancer/' + id)
    .then((response) => response.json())
    .then((responseJson) => {
         console.log(responseJson);
         this.setState({
            data: responseJson
         })
      })
    .done();
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    StatusBar.setBarStyle('light-content', true);
    openDrawer = () => {
      this.drawer._root.open()
    };
    closeDrawer = () =>{
      this.drawer._root.close()
    };
    pressDrawer = () =>{

    }
    return (

        <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='ios-arrow-back'/>
                </Button>

              </Left>
              <Body style={styles.navBarCentered}>
                <Title style={styles.navBarTitle}>{this.state.data.type_under}</Title>
              </Body>

              <Right style={styles.navBarWidth}/>
            
            </Header>            
              <Content style={{backgroundColor:'#ffffff'}}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>{this.state.data.title_under}</Text>
                          <Text style={styles.contentStyle2}>{this.state.data.description_under}</Text>
                          {/*<Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                                                    <Text style={styles.contentStyle2Point}>• Anemia (Pucat)</Text>
                                                    <Text style={styles.contentStyle2Point}>• Mudah lelah</Text>
                                                    <Text style={styles.contentStyle2Point}>• Demam berkepanjangan</Text>
                                                    <Text style={styles.contentStyle2Point}>• Mudah terkena infeksi</Text>
                                                    <Text style={styles.contentStyle2Point}>• Pendarahan yang tidak jelas sebabnya</Text>
                                                    <Text style={styles.contentStyle2Point}>• Nyeri tulang</Text>
                                                    <Text style={styles.contentStyle2Point}>• Pembengkakan perut</Text>
                                                    <Text style={styles.contentStyle2Point}>• Pembengkakan kelenjar getah bening</Text>
                                                    <Text style={styles.contentStyle2Point}>• Memar di area muka</Text>
                                                    <Text style={styles.contentStyle2Point}>• Berdarah tanpa sebab</Text>
                                                    <Text style={styles.contentStyle2Point}>• Kehilangan nafsu makan</Text>
                                                    <Text style={styles.contentStyle2Point}>• Penyusutan berat badan</Text>
                                                    <Text style={styles.contentStyle2Point}>• Keseimbangan badan</Text>
                                                    <Text style={styles.contentStyle2Point}>• Ketidaknyamanan di bawah tulang rusuk kiri bawah</Text>
                                                    <Text style={styles.contentStyle2Point}>• Sel darah putih yang sangat tinggi</Text>*/}
                        </Image>
                      </View>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}