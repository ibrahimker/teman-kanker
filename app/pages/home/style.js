const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	backgroundImage: {
    	flex: 1,
    	resizeMode: 'cover',
    	width:null
  	},
  	backgroundHomeHeader: {
  		flex:0.6,
  		resizeMode:'stretch',
  		width:null,
  	},
	text: {
		color: 'red',
	},
	view: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		paddingTop: 98
	},
	logoText: {
		color:'#ffffff',
		paddingTop:20,
		fontSize:40
	},
	form: {
		paddingLeft:40,
		paddingRight:40
	},
	input:{
		backgroundColor:'#ffffff',
		borderRadius:10,
		marginTop:20,
		marginBottom:5,
		paddingLeft:20
	},
	button:{
		borderRadius:10,
		marginLeft:20,
		marginTop:30,
		marginBottom:30
	},
	statBar:{
		paddingTop: (Platform.OS === 'ios') ? 20 : 0,
		backgroundColor:'#ffffff'
	},
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#ffffff'
	},
	navBarTitle:{
		fontFamily: 'Shrikhand-Regular',
		color: '#ffffff',
		fontSize: 24,
	}
}