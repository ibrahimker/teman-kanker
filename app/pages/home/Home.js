import React, { Component } from 'react';
import {Text,View,Drawer,Button,Icon,Container,Content,Header,Left,Body,Right,Title,H1} from 'native-base';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, AsyncStorage, Image, StatusBar } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js';

import images from '../../config/images.js';
import styles from './style.js';

export default class HomeScreen extends Component {
  static navigationOptions = {
    title: 'Home',
    header:null
  };

  state =  {
    'isLoggedIn' : 'false'
  }

  constructor() {
    super();
    this.checkAuthentication();
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    openDrawer = () => {
      this.drawer._root.open()
    };
    closeDrawer = () =>{
    	this.drawer._root.close()
    };
    pressDrawer = () =>{

    }
    return (
      <Container>
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<Sidebar navigation={this.props.navigation}/>}
          onClose={() => this.drawer._root.close()}
          panOpenMask={.25} >
          <Image source={images.backgroundHomeHeader} style={styles.backgroundHomeHeader}>
            <Header noShadow style={{backgroundColor: 'transparent', borderBottomColor:'transparent'}}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() => this.drawer._root.open()}>
                  <Icon name='menu' style={styles.navBarColor}/>
                </Button>
              </Left>
              <Body>
                <Title style={styles.navBarTitle}>Gerai Kanker</Title>
              </Body>
              <Right style={styles.navBarWidth}/>
            </Header>
          </Image>              
          <Content style={{backgroundColor:'#fff'}}>
              <Image source={images.backgroundHome} style={styles.backgroundImage}>
                <H1 style={{textAlign:'center',padding:20}}>Health Recommendation</H1>
                <View style={{borderBottomColor: '#e4e4e4',borderBottomWidth: 0.5}} />
                <H1 style={{textAlign:'center',padding:20}}>Treatment Schedule</H1>
                <View style={{borderBottomColor: '#e4e4e4',borderBottomWidth: 0.5}} />
              </Image>
          </Content> 
      </Drawer>
      </Container>
    );
  }

  checkAuthentication = () => {
    AsyncStorage.getItem('isLoggedIn', (err, result) => {
      this.setState({ 'isLoggedIn': result });
      if(this.state.isLoggedIn == 'false') {
        this.props.navigation.navigate('Login');
      }
    });
  }

}