import React, { Component } from 'react';
import {Text,View,Button,Container,Content,Header,Left,Body,Right,Title,Footer,FooterTab,Tabs} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage, Linking} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class TentangKitaScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    'headerTitle':'Beranda',
    'screen':'0',
  }

  constructor(props) {
    super(props);
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
      return (
      <Container>
          <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='arrow-left'/>
                  </Button>
              </Left>
              <Body style={styles.navBarCentered}>
                <Title style={styles.navBarTitle}>Tentang Kami</Title>
              </Body>
              <Right style={styles.navBarWidth}/>
            </Header>


              <Image source={images.backgroundHome} style={styles.backgroundImage}>
            
            <Content>

                  <View style={styles.center}>
                    <View style={{paddingTop:10}}></View>
                    <Image source={images.logoWithText} style={styles.logo}/>
                  </View>

                  <View style={{padding:30, paddingRight:40}}>
                    <Text style={styles.aboutText}>TemanKanker adalah platform digital yang menghubungkan para pihak peduli kanker, dari pasien dan survivor kanker, dokter hingga masyarakat umum di Indonesia.</Text>
                    <Text style={styles.aboutText2}>Melalui digital platform ini, pengguna dapat mengakses informasi seperti jenis-jenis kanker, gejala, informasi rumah sakit serta terhubung dengan dokter spesialis kanker.</Text>
                    <Text style={styles.aboutText2}>Pasien, survivor, dokter, relawan, dan masyarakat umum juga dapat saling berdiskusi melalui fitur chat sehingga interaksi terasa personal. Melalui aplikasi ini pun, pengguna dapat membaca tips dari dokter, kisah survivor kanker dan berita terkait kanker lainnya</Text>
                  </View>

                  <View style={styles.center}>
                      <Text style={styles.header1}>Dipersembahkan oleh:</Text>
                  </View>
                  <View>
                      <TouchableOpacity style={{flex:1, alignItems: 'center',}} onPress={() =>  Linking.openURL('https://www.yoaifoundation.org/').catch(err => console.error('Error', err))}>
                        <Image source={images.logoyoai} style={styles.logo2}/>
                      </TouchableOpacity>
                      {/*<Text style={styles.header1}>Didukung oleh:</Text>*/}
                      <TouchableOpacity style={{flex:1, alignItems: 'center',}} onPress={() =>  Linking.openURL('https://www.ibm.com/id-en/').catch(err => console.error('Error', err))}>
                      <Image source={images.logoibm} style={styles.logo2}/>
                      </TouchableOpacity>
                      <TouchableOpacity style={{flex:1, alignItems: 'center',}} onPress={() =>  Linking.openURL('https://pitapink-ykpi.or.id/').catch(err => console.error('Error', err))}>
                      <Image source={images.logoykpi} style={styles.logo2}/>
                      </TouchableOpacity>
                      <TouchableOpacity style={{flex:1, alignItems: 'center',}} onPress={() =>  Linking.openURL('https://dharmais.co.id/').catch(err => console.error('Error', err))}>
                      <Image source={images.logodharmais} style={styles.logo2}/>
                      </TouchableOpacity>
                  </View>

            </Content>
              </Image>

      </Container>
    );
  }
}