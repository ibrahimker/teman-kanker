const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	logo:{
		width:230,
		height:250,
		marginTop: 30
	},
	logo4:{
		height:150,
		resizeMode:'contain',
		alignItems: 'left',
	},
	logo2:{
		flex:1,
		height:200,
		margin: 20,
		backgroundColor: '#fff',
		padding:10,
		flex: 1,
		alignItems: 'center'
	},
	logo3:{
		height:200,
		resizeMode:'contain'
	},
	aboutText:{
		paddingTop:15,
		fontSize:16,
		fontFamily:'Montserrat-Regular',
		lineHeight: 24,
	},
	aboutText2:{
		paddingTop:5,
		fontSize:16,
		fontFamily:'Montserrat-Regular',
		lineHeight: 24,
	},
	header1:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#489FFD',
		fontSize: 20,
		marginTop: 20,
		flex:1,
		flexDirection:'column',
    	alignItems: 'center',

	},
	center:{
		justifyContent: 'center',
    	alignItems: 'center',
	},
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#fff',
		fontSize:24,
	},
	navBarTitle:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#fff',
		fontSize: 24
	},
	navBarStyle:{
		backgroundColor:'#489FFD',
		height: 70,
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},

}