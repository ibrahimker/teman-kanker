import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail, Spinner, Toast} from 'native-base';


import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, WebView,Share } from 'react-native';
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js'
import { Col, Row, Grid } from 'react-native-easy-grid';
import images from '../../config/images.js';
import styles from './style.js';

import HTMLView from 'react-native-htmlview';
const styles2 = StyleSheet.create({
  p: {
    fontFamily: 'Montserrat-Regular',
    fontSize:14,
    color:'#000000',
    marginTop:-10,
    lineHeight: 24,
    marginBottom:-16
    
  },
  strong: {
    fontFamily: 'Montserrat-Bold'
  },
  b: {
    fontFamily: 'Montserrat-Bold'
  },
  h1: {
    fontFamily: 'Montserrat-Regular'
  },
  i: {
    fontFamily: 'Montserrat-Regular'
  },
  u: {
    fontFamily: 'Montserrat-Regular'
  },
  h2: {
    fontFamily: 'Montserrat-Regular'
  },
  h3: {
    fontFamily: 'Montserrat-Regular'
  },
  li: {
    fontFamily: 'Montserrat-Regular'
  },
  ul: {
    fontFamily: 'Montserrat-Regular'
  },
  ol: {
    fontFamily: 'Montserrat-Regular',
    color:'#000000',
  }
});

export default class BerandaDetilScreen extends Component {
  static navigationOptions = {
    title: 'Beranda Detil',
    header:null,
    tabBarVisible: false,
  };

  state = {
    messages: [],
    id: '',
    data: {},
    dataLoaded: false
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.articleId;
    fetch('https://temankanker.com/api/Cancers/' + id)
    .then((response) => response.json())
    .then((responseJson) => {
         this.setState({
            data: responseJson,
            dataLoaded: true
         })
      })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    });
  }

  share(title){
    Share.share({
      message: title+'... Baca artikel selengkapnya di aplikasi TemanKanker! https://play.google.com/store/apps/details?id=com.yoai.temankanker',
      url: 'Download Teman Kanker at Play Store',
      title: 'Share to Social Media'
    }, {
      // Android only:
      dialogTitle: title,
      // iOS only:
      excludedActivityTypes: [
        'com.apple.UIKit.activity.PostToTwitter'
      ]
    })
  }

  renderLoading() {
    if(!this.state.dataLoaded) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Mengunduh data...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    StatusBar.setBarStyle('light-content', true);
    return (
        <Container>
            <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='arrow-left'/>
                  </Button>
              </Left>
              <Body style={styles.navBarCentered}>
                <Title style={styles.navBarTitle}>{this.state.data.name}</Title>
              </Body>
              <Right style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.share(this.state.data.name)}>
                  <Icon style={styles.navBarColor} name='share-alt'/>
                </Button>
              </Right>
            </Header>
            {this.state.dataLoaded && (
                <Image source={images.backgroundHome} style={styles.backgroundImage}>
                <Content>
                  <Image source={{uri: 'https://temankanker.com'+this.state.data.photo}} style={{height: 300, width: null, flex: 1}}/>
                    <Text style={styles.textTitle}>{this.state.data.name}</Text>
                    <View style={{padding:30,paddingTop:10}}>
                      <HTMLView
                        value={this.state.data.content}
                        stylesheet={styles2}
                      />
                    </View>
                </Content>
                  </Image>
            )}

            {this.renderLoading()}
        </Container>
      );
    }
  }

