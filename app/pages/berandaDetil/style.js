const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
    maxWidth: 50
  },
  navBarColor:{
    color: '#fff',
    paddingLeft:5,
    fontSize:24
  },
	navBarTitle:{
    fontFamily: 'BalooThambi-Regular',
    fontWeight: '400',
    color: '#fff',
    fontSize: 24,
    paddingLeft: (Platform.OS === 'android') ? 0 : 0
  },
  navBarStyle:{
    backgroundColor:'#489DFF',
    height: 70,
  },
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
	textUserDate: {
		fontFamily: 'Montserrat-Regular',
		color: '#B2B2B2',
		fontSize: 12,
		margin: 30,
		marginBottom: 0,
		backgroundColor: 'transparent'
	},
	textUsername: {
    fontFamily: 'BalooThambi-Regular',
    color: '#489DFF',
    fontSize: 18,
    fontWeight:'500',
    paddingTop:33
  },
	textUserTitle: {
		fontFamily: 'Montserrat-Regular',
		color: '#B2B2B2',
		fontSize: 12,
		backgroundColor: 'transparent'
	},
	textTitle: {
    fontFamily: 'BalooThambi-Regular',
    color: '#489DFF',
    fontSize: 22,
    marginTop: 5,
    lineHeight: 30,
    padding:30
  },
  textArticleTime: {
    fontFamily: 'BalooThambi-Regular',
    color: '#B2B2B2',
    fontSize: 16,
    paddingTop:12,
    paddingLeft:30
  },
	contentStyle2:{
		backgroundColor: 'transparent',
		margin: 30,
		marginTop: 30,
		marginBottom: 20,
		fontFamily: 'Montserrat',
		fontSize: 16,
		lineHeight: 24,
		color: '#000'
	},
	contentStyle3:{
		backgroundColor: 'transparent',
		margin: 30,
		marginTop: 0,
		marginBottom: 20,
		fontFamily: 'Montserrat-Regular',
		fontSize: 14,
		lineHeight: 24,
		color: '#868686'
	},
	contentStyle3End:{
		backgroundColor: 'transparent',
		margin: 30,
		marginTop: 0,
		marginBottom: 50,
		fontFamily: 'Montserrat-Regular',
		fontSize: 14,
		lineHeight: 24,
		color: '#868686',
	},
  thumbnailSize:{
    height:40,
    width:40,
    borderRadius:50,
    padding:30,
    marginTop:30,
    marginLeft:30
  },
  thumbnailMargin:{
    marginTop: 5,
    marginBottom: 5,
	},
	loadingOverlay: {
			opacity: 0.9,
			backgroundColor: 'white',
			flex: 1,
			width: null,
			height: null,
			justifyContent: 'center',
			alignItems: 'center'
	},
}