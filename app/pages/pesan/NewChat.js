import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Footer,Left,Body,Right,Title,Card,CardItem,Thumbnail,Form,Item,Input,ListItem,List,Spinner,Tabs,Tab, Toast} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity,AsyncStorage, FlatList } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as pesanActions from '../../actions/pesanActions.js'

class NewChatScreen extends Component {
  static navigationOptions = {
    title: 'NewChat',
    header:null
  };

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    contactList:[],
    contactListPasien:[],
    contactListSurvivor:[],
    contactListLainnya:[],
    dataLoadedSurvivor:false,
    dataSurvivorEnd: false,
    dataLoadedLainnya:false,
    dataLainnyaEnd: false,
    dataLoadedPasien: false,
    dataPasienEnd: false,
    scrollCounterLainnya: 0,
    scrollCounterPasien: 0,
    scrollCounterSurvivor: 0
  }

  constructor(props) {
    super(props);
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });
      if(this.state.userId == '') {
        this.props.navigation.navigate('Login');
      }
      else{
        this.retrieveUsersPasien();
        this.retrieveUsersSurvivor();
        this.retrieveUsersLainnya();
      }
    });
  }

  createRoom(userId1,userId2,receiverName){

    fetch('https://temankanker.com/api/Rooms/createRoom', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "user1": userId1,
        "user2":userId2
      })
    })
    .then((response) => response.json())
    .then((data) => {

      if(data.hasOwnProperty('error')){
      }
      else{
        this.props.pesanActions.setChatRoomStatus(data.roomId, true)
        this.props.navigation.navigate('ChatRoom', {receiverName:receiverName,receiverId: userId1,senderId:userId2,roomId:data.roomId,fromPesan: false, key: this.props.navigation.state.key})
      }
    })
    .catch(error => {

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  retrieveUsersPasien() {
    fetch('https://temankanker.com/api/UserKankers?filter={"where": {"id": {"neq": '+ this.state.userId +'}, "role" : "Pasien", "email_confirm": true}, "limit" : 10, "skip": '+ this.state.scrollCounterPasien * 10 +'}')
    .then((response) => response.json())
    .then((data) => {
      let dataEnd = data.length > 0 ? false : true
      setTimeout(() => {
        return this.setState({ contactListPasien: [ ...this.state.contactListPasien, ...data], dataLoadedPasien: true, dataPasienEnd: dataEnd, scrollCounterPasien: this.state.scrollCounterPasien + 1 });
      }, 800)
    })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  retrieveUsersSurvivor() {
    fetch('https://temankanker.com/api/UserKankers?filter={"where": {"id": {"neq": '+ this.state.userId +'}, "role" : "Survivor", "email_confirm": true}, "limit" : 10, "skip": '+ this.state.scrollCounterSurvivor * 10 +'}')
    .then((response) => response.json())
    .then((data) => {
      let dataEnd = data.length > 0 ? false : true
      setTimeout(() => {
        return this.setState({ contactListSurvivor: [ ...this.state.contactListSurvivor, ...data], dataLoadedSurvivor: true, dataSurvivorEnd: dataEnd, scrollCounterSurvivor: this.state.scrollCounterSurvivor + 1 });
      }, 800)
    })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  retrieveUsersLainnya() {
    fetch('https://temankanker.com/api/UserKankers?filter={"where": {"id": {"neq": '+ this.state.userId +'}, "role" : "Lainnya", "email_confirm": true}, "limit" : 10, "skip": '+ this.state.scrollCounterLainnya * 10 +'}')
    .then((response) => response.json())
    .then((data) => {
      let dataEnd = data.length > 0 ? false : true
      console.log(data, this.state.scrollCounterLainnya)
      setTimeout(() => {
        return this.setState({ contactListLainnya: [ ...this.state.contactListLainnya, ...data], dataLoadedLainnya: true, dataLainnyaEnd: dataEnd, scrollCounterLainnya: this.state.scrollCounterLainnya + 1 });
      }, 800)
    })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  renderChatListPasien() {
    if(!this.state.dataLoadedPasien) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return(
        <FlatList 
            keyExtractor={(item, index) => item.id}
            data = {this.state.contactListPasien}
            bounces={false}
            onEndReached={() => {this.state.dataPasienEnd ? false : this.retrieveUsersPasien()}}
            onEndReachedThreshold={0.3}
            renderItem = {({item}) => 
               <View>
                  <ListItem key={item.id} onPress={() =>  this.createRoom(item.id,this.state.userId,item.name)} style={{backgroundColor: 'transparent'}}  avatar>
                    <Left>
                      <Thumbnail defaultSource={images.profilePictDefault} source={{ uri: 'https://temankanker.com'+item.photo }} />
                    </Left>
                    <Body style={{paddingBottom: 20, borderWidth: 0, borderColor: 'transparent'}}>
                      <Text style={styles.textUsername}>{item.name}</Text>
                      <Text style={styles.textUserTitle}>{item.city}, {item.group}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:81, width: null}}/>
              </View> 
            }
        />
      )
    }
  }

  renderChatListSurvivor() {
    if(!this.state.dataLoadedSurvivor) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return(
        <FlatList 
            keyExtractor={(item, index) => item.id}
            data = {this.state.contactListSurvivor}
            bounces={false}
            onEndReached={() => {this.state.dataSurvivorEnd ? false : this.retrieveUsersSurvivor()}}
            onEndReachedThreshold={0.3}
            renderItem = {({item}) => 
               <View>
                  <ListItem key={item.id} onPress={() =>  this.createRoom(item.id,this.state.userId,item.name)} style={{backgroundColor: 'transparent'}}  avatar>
                    <Left>
                      <Thumbnail defaultSource={images.profilePictDefault} source={{ uri: 'https://temankanker.com'+item.photo }} />
                    </Left>
                    <Body style={{paddingBottom: 20, borderWidth: 0, borderColor: 'transparent'}}>
                      <Text style={styles.textUsername}>{item.name}</Text>
                      <Text style={styles.textUserTitle}>{item.city}, {item.group}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:81, width: null}}/>
              </View> 
            }
        />
      )
    }
  }

  renderChatListLainnya() {
    if(!this.state.dataLoadedLainnya) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return(
        <FlatList 
            keyExtractor={(item) => item.id}
            data = {this.state.contactListLainnya}
            bounces={false}
            onEndReached={() => {this.state.dataLainnyaEnd ? false : this.retrieveUsersLainnya()}}
            onEndReachedThreshold={0.3}
            renderItem = {({item, index}) => 
               <View>
                  <ListItem key={index} onPress={() =>  this.createRoom(item.id,this.state.userId,item.name)} style={{backgroundColor: 'transparent'}}  avatar>
                    <Left>
                      <Thumbnail defaultSource={images.profilePictDefault} source={{ uri: 'https://temankanker.com'+item.photo }} />
                    </Left>
                    <Body style={{paddingBottom: 20, borderWidth: 0, borderColor: 'transparent'}}>
                      <Text style={styles.textUsername}>{item.name}</Text>
                      <Text style={styles.textUserTitle}>{item.city}, {item.group}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:81, width: null}}/>
              </View> 
            }
        />
      )
    }
  }

 render() {
  StatusBar.setBarStyle('light-content', true);
  return (
    <Container>
      <Header noShadow style={styles.navBarStyle}>
        <Left style={styles.navBarWidth}>
          <Button transparent onPress={() =>  this.props.navigation.goBack()}>
            <Icon style={styles.navBarColor} name='arrow-left'/>
          </Button>
        </Left>
        <Body style={styles.navBarCentered}>
          <Title style={styles.navBarTitle}>Buat Pesan Baru</Title>
        </Body>
        <Right style={styles.navBarWidth}>
          <Button transparent onPress={() =>  this.props.navigation.navigate('SearchChat')}>
             <Icon style={styles.headerIcon2} name='search'/>
          </Button>
        </Right>
      </Header>              
      <Tabs tabBarUnderlineStyle={{borderBottomWidth:0}}>
          <Tab heading="Pasien" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
              <Image source={images.backgroundHome} style={{flex: 1, width: null, height: null}}>
                    {this.renderChatListPasien()}
              </Image>
          </Tab>
          <Tab heading="Survivor" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
              <Image source={images.backgroundHome} style={{flex: 1, width: null, height: null}}>
                    {this.renderChatListSurvivor()}
              </Image>
          </Tab>
          <Tab heading="Lainnya" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
              <Image source={images.backgroundHome} style={{flex: 1, width: null, height: null}}>
                    {this.renderChatListLainnya()}
              </Image>
          </Tab>
      </Tabs>
    </Container>
  
    );
  }
}

const mapStateToProps = state => ({
  room: state.pesanReducer
})

const mapDispatchToProps = (dispatch) => ({
  pesanActions: bindActionCreators(pesanActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(NewChatScreen)