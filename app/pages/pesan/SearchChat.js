import React, { Component } from 'react';
import {Toast,Text,View,Drawer,Button,Container,Content,Header,Footer,Left,Body,Right,Title,Card,CardItem,Thumbnail,Form,Item,Input,ListItem,List,Spinner,Tabs,Tab} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as pesanActions from '../../actions/pesanActions.js'

class SearchChatScreen extends Component {
  static navigationOptions = {
    title: 'SearchChat',
    header:null
  };


  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    contactList:[],
    dataLoaded:false
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      contactList: [],
      dataLoaded: false
    };
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });
      if(this.state.userId == '') {
        this.props.navigation.navigate('Login');
      }
      else{
        // this.retrieveUsersPasien();
        // this.retrieveUsersSurvivor();
        // this.retrieveUsersLainnya();
      }
    });
  }

  componentDidMount() {

  }

  renderLoading() {
    if(this.state.sendingNote) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 12, margin:12, textAlign:'center'}}>Sedang Mencari, Mohon Tunggu..</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  validateInput() {
    let query = this.state.query;
    const { navigate } = this.props.navigation;

    this.setState({ loging: true });
    this.setState({sendingNote: true});

    if(query != "") {
      fetch('https://temankanker.com/api/UserKankers?filter[where][or][0][name][regexp]=/' + encodeURIComponent(query) + '/ig&filter[where][or][1][city][regexp]=/' + encodeURIComponent(query) + '/ig')
      .then((response) => response.json())
      .then((data) => {

        var contactnew = [];

        for (var i = 0;i<data.length;i++){
          if (data[i].role != 'Dokter'){
            contactnew.push(data[i])
          }
        }
        this.setState({ contactList: contactnew, dataLoaded: true, sendingNote: false });
      })
      .catch(error => {
        Toast.show({
          text: 'Terjadi kesalahan saat melakukan pencarian',
          position: 'top',
          type:'danger',
          buttonText: 'OK',
          duration: 3000
        });
      })
      .done();
    }
    else {
      Toast.show({
        supportedOrientations: ['portrait','landscape'],
        text: 'Field must not be empty!',
        position: 'bottom',
        buttonText: 'Okay',
        type:'danger',
        duration: 3000
      })
    }
  }

  createRoom(userId1,userId2,receiverName){
    this.setState({dataLoaded:true})
    fetch('https://temankanker.com/api/Rooms/createRoom', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "user1": userId1,
        "user2":userId2
      })
    })
    .then((response) => { 
      return response.json()
    })
    .then(data => {
      if(data.hasOwnProperty('error')){
        this.setState({dataLoaded:false})
      }
      else{
        this.props.pesanActions.setChatRoomStatus(data.roomId, true)
        this.props.navigation.navigate('ChatRoom', {receiverName:receiverName,receiverId: userId1,senderId:userId2,roomId:data.roomId,fromPesan: false, key: this.props.navigation.state.key})
        this.setState({dataLoaded:false})
      }
    })
  }

  renderChatList() {
      return this.state.contactList.map((contact,index) => (
             <TouchableOpacity key={contact.id} onPress={() =>  this.createRoom(contact.id,this.state.userId, contact.name)}>
                <CardItem style={{backgroundColor: '#fffae6'}}>
                  <Left>
                    <Thumbnail style={{height: 30, width: 30, borderRadius: 15}} source={{uri: 'https://temankanker.com'+contact.photo}} />
                    <Body style={{marginLeft: 15}}>
                      <View style={{flexDirection: 'row', justifyContent: 'space-between'}}> 
                        <Text style={styles.textUsername}>{contact.name}</Text>
                      </View>
                      <Text style={styles.textUserTitle}>{contact.city}, {contact.group}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
            </TouchableOpacity>      
      ));
  }

 render() {
  StatusBar.setBarStyle('light-content', true);
  StatusBar.setBackgroundColor('#000');
  return (
    <Container>
      <Header noShadow style={styles.navBarStyle}>
        <Left style={styles.navBarWidth}>
          <Button transparent onPress={() =>  this.props.navigation.goBack()}>
            <Icon style={styles.navBarColor} name='arrow-left'/>
          </Button>
        </Left>
        <Body style={styles.navBarCentered}>
          <Title style={styles.navBarTitle}>Pencarian</Title>
        </Body>
      </Header>              
      <Content style={{backgroundColor: '#fffae6'}}>
        <Image source={images.backgroundHome} style={styles.backgroundImage}>
        <Form style={{padding: 15}}>
                    <Item rounded style={styles.input}>
                          <Icon style={styles.inputIcon} active name='search' />
                          <Input style={styles.placeholderStyles} type="text" placeholder='Kata Pencarian' placeholderTextColor='#93938E' onChangeText={(query) => this.setState({query})}/>
                    </Item>
                    <Button rounded onPress={() => this.validateInput()} style={styles.buttonLightBlue} block>
                        <Text style={styles.balooThambi}>Cari</Text>
                    </Button>
                  </Form>
                  {this.renderLoading()}
              <List>
              {this.renderChatList()}
              </List>
            
        </Image>
      </Content>
    </Container>
    );
  }
}

const mapStateToProps = state => ({
  room: state.pesanReducer
})

const mapDispatchToProps = (dispatch) => ({
  pesanActions: bindActionCreators(pesanActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(SearchChatScreen)