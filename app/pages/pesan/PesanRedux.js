import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Form,Content,Header,Footer,Left,Body,Right,Title,Card,CardItem,Thumbnail,Spinner,Badge,Toast,Input} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity,AsyncStorage,Alert,FlatList, DeviceEventEmitter, List } from 'react-native';
import moment from 'moment';
import FooterTabNavigation from '../../components/footerTab/FooterTab.js';

import Login from '../login/LoginNoClose.js'

import OneSignal from 'react-native-onesignal';
import Swipeout from 'react-native-swipeout';

import io from 'socket.io-client';

import images from '../../config/images.js';
import styles from './style.js';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as chatCounterActions from '../../actions/chatCounterActions.js'
import * as pesanActions from '../../actions/pesanActions.js'

class SwipeButton extends Component {
  render() {
    return (
      <View style={{ flex: 1, width: null, height: null, alignItems: 'center', justifyContent: 'center' }}>
        <Icon name="close" size={26} color="white"/>
        <Text style={{ color: 'white', fontSize: 13, marginTop: 5, fontFamily: 'Montserrat' }}>Hapus</Text>
      </View>
    )
  }
}

class PesanReduxScreen extends Component {
  static navigationOptions = {
    header:null,
    tabBarIcon: (
      <Icon style={{ fontSize: 20 }} name='commenting'/>
    ),
    tabBarLabel: 'Pesan'
  };

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    roomList:[],
    roomDoctor: [],
    datas:[],
    dataId:[],
    email_confirm:false,
    dataLoaded:false,
    readCount: []
  }

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      'isLoggedIn' : 'false',
      'userId':'',
      'role':'',
      roomList:[],
      roomDoctor: [],
      datas:[],
      dataId:[],
      email_confirm:false,
      dataLoaded:false,
      readCount: []
    }
  }

  componentWillMount() {
    this.loadData()
  }

  loadData() {
    this.setState({dataLoaded:false})
    AsyncStorage.getItem('userId', (err, result) => {
      
      if(!result) {
        this.setState({dataLoaded:true, 'userId': result})
      }
      else {
        this.setState({ 'userId': result });

        fetch('https://temankanker.com/api/UserKankers/' + result)
        .then((response) => response.json())
        .then((data) => {

          if(!data.email_confirm) {
            this.setState({dataLoaded:true})
          }
          else{
            this.retrieveChat()
          }

          this.setState({ datas: data })
        })
        .catch(error => {
          Toast.show({
            text: 'Terjadi kesalahan saat mengambil data user ' + error,
            position: 'top',
            type:'danger',
            buttonText: 'OK',
            duration: 3000
          });
          this.setState({dataLoaded:true, datas: {email_confirm: 'No Data'}})
        })
        .done();
      }
    });
  }

  componentWillUnmount() {
    this.socket.close()
  }

  submitForm(userId) {
    fetch('https://temankanker.com/api/UserKankers/getRegistrationToken?userId=' + userId)
    .then((response) => response.json())
    .then((data) => {
      return this.setState({ dataId: data, dataLoaded: true });
    })
    const { navigate } = this.props.navigation;
    const { goBack } = this.props.navigation;
    DeviceEventEmitter.emit('refresh',  {})
    goBack();
    this.confirmMsg();
  }

  retrieveUser(userId){
    fetch('https://temankanker.com/api/UserKankers/' + userId)
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ datas: data })
      })
      .catch(error => {
        Toast.show({
          text: 'Terjadi kesalahan saat mengambil data user ',
          position: 'top',
          type:'danger',
          buttonText: 'OK',
          duration: 3000
        });
      })
      .done();
  }

  retrieveChat(reset = false, roomId = null) {
      this.socket = io('https://temankanker.com', { transports: ['websocket'] });
      this.socket.emit('join', this.state.userId)
      this.socket.on('createroom', (data) => {
        console.log(data)
        this.props.pesanActions.loadPesan(this.state.userId)
      })

      this.props.room.room.forEach(element => {
        this.socket.emit('join', element.roomId);
      })

      this.socket.on('message', (data) => {

        let count = 0
        let id = null

        console.log(data)

        this.props.room.room.forEach(element => {
          if(element.roomId === data.roomId){
            id = element.id
            count = element.unread
          }
        })

        setTimeout(() => {
            if(!this.props.room.inChatRoom){
              console.log('increment1')
              this.props.pesanActions.incrementUnreadCounter(id, count, this.state.userId)
            }
            else if(this.props.room.inChatRoom && data.roomId !== this.props.room.currentChatRoom) {
              console.log('increment2')
              this.props.pesanActions.incrementUnreadCounter(id, count, this.state.userId)
            }
        }, 100)
      });
  }

  exitRoom(roomId,userId){
    fetch('https://temankanker.com/api/Rooms/exitRoom', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "roomId":roomId,
        "userId":parseInt(userId),
      })
    })
    .then((response) => {
      Toast.show({
        text: 'Chat Dihapus!',
        position: 'bottom',
        type:'success',
        duration: 2000
      });

      let count = this.state.readCount

      count.forEach((element,index) => {
        if(element.roomId === roomId) {
          count.splice(index, 1)
        }
      })

      this.setState({ readCount: count })

      this.refreshChat()
    })
  }

  confirmMsg() {
    Toast.show({
      text: 'Silahkan cek email Anda untuk verifikasi!',
      position: 'bottom',
      type:'success',
      duration: 5000
    });
  }

  renderEmailConfirmation() {
    if(this.state.datas.email_confirm==false || this.state.datas.email_confirm === null || this.state.datas.email_confirm === undefined) {
        return(
          <View style={{flex: 10, alignItems: 'center'}}>
              <Form style={{padding: 15}}>
                  <Text style={[styles.textGrey, {backgroundColor: 'transparent'}]}>Maaf, email Anda belum dikonfirmasi, klik tombol dibawah untuk memverifikasi.</Text>
                <Button rounded onPress={() => this.submitForm(this.state.userId)} style={styles.buttonLightBlue} block>
                  <Text style={styles.balooThambi}>Konfirmasi Email</Text>
                </Button>
              </Form>
          </View>
        );
    }
  }

  renderSeparator() {
    return (
      <Image source={images.dotLine} style={{height: 4, marginLeft: "25%", width: null}}/>
    );
  }

  renderEmptyChat() {
    return (
      <View style={{flex: 1, width: null, height: 400, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={images.pesanEmpty} style={{ height: 300, width: 300 }} resizeMode={"contain"}/> 
      </View> 
    )
  }

  renderDoctorChat() {
    if(this.state.datas.role !=='Dokter') {
      return (
        // <TouchableOpacity onPress={() => {this.socket.close(); this.props.navigation.navigate('ChatRoomDoctor', {userId:this.state.userId, fromPesan: false})}}>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('ChatRoomDoctor', {userId:this.state.userId, fromPesan: false})}>
          <CardItem style={{backgroundColor: 'transparent', paddingBottom: 15, paddingTop: 15}}>
            <Left>
              <Thumbnail style={{height: 60, width: 60, borderRadius: 30}} source={{uri: 'https://temankanker.com'+'/api/Storages/hospital/download/logodharmais.png'}} />
              <Body style={{marginLeft: 15}}>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                  <Text style={styles.textUsernameDokter}>dr. Teman Kanker</Text>
                  <Text style={styles.textTime}>{moment().format('h:mm a')}</Text>
                  {/* {this.state.roomDoctor[0].readCount > 0 && (
                      <Badge style={{ position: 'absolute', right: 5, top: 25, backgroundColor: '#79C948' }} info>
                        <Text>{this.state.roomDoctor[0].readCount}</Text>
                      </Badge>
                  )} */}
                </View>
                {/*<Text style={styles.textLastChat}>Layanan belum tersedia</Text>*/}
              </Body>
            </Left>
          </CardItem>
          <Image source={images.dotLine} style={{height: 4, width: null}}/>
        </TouchableOpacity>
      )
    }
  }

  refreshChat = () => {
    this.setState({ refreshing: true })
    this.props.pesanActions.loadPesan(this.state.userId)
  }

  goToChatRoom(item, index) {

    this.props.pesanActions.resetUnreadCounter(item.id, this.state.userId)
    this.props.pesanActions.setChatRoomStatus(item.roomId, true)

    setTimeout(() => console.log(this.props.room), 100)

    this.props.navigation.navigate('ChatRoom', {receiverName:item.userOppositeProfile.name,receiverId:item.userOppositeProfile.id,senderId:this.state.userId,roomId: item.roomId})
  }

  renderUndreadCounter(roomId) {
    return this.state.readCount.map((element) => {
      if(element.roomId === roomId && element.count > 0) {
        return (
          <View key={element.roomId} style={{ height: 20, minWidth: 20, padding: 5, borderRadius: 10, position: 'absolute', right: 5, top: 35, backgroundColor: '#79C948', flex: 1, justifyContent: 'center', alignItems: 'center' }} info>
            <Text style={{ fontSize: 13, color: 'white', backgroundColor: 'transparent' }}>{element.count}</Text>
          </View>
        )
      }
    })
  }

  renderChatList() {
    return (
      <View style={{flex: 1}}>
        {this.renderDoctorChat()}
        <FlatList
          horizontal={false}
          refreshing={this.props.room.refreshing}
          onRefresh={this.refreshChat}
          ListEmptyComponent={this.renderEmptyChat}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => item.id}
          data={this.props.room.roomList}
          renderItem={({item, index, separators}) => (
            <Swipeout key={item.id} backgroundColor={'transparent'} right={[{component: <SwipeButton />, backgroundColor:'#e96244',onPress:()=>{this.exitRoom(item.roomId,this.state.userId)}}]}>
            <TouchableOpacity onPress={() => {this.goToChatRoom(item, index)}}>
              <CardItem style={{backgroundColor: 'transparent', paddingBottom: 15, paddingTop: 15}}>
                <Left>
                <Thumbnail style={{height: 60, width: 60, borderRadius: 30}} defaultSource={images.profilePictDefault} source={{uri: 'https://temankanker.com'+item.userOppositeProfile.photo}} />
                  <Body style={{marginLeft: 15}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                      <Text style={styles.textUsername}>{item.userOppositeProfile.name}</Text> 
                      <Text style={styles.textTime}>{moment(item.updated_at).format('h:mm a')}</Text>
                      {item.unread > 0 && (
                        <View key={item.roomId} style={{ height: 20, minWidth: 20, padding: 5, borderRadius: 10, position: 'absolute', right: 5, top: 35, backgroundColor: '#79C948', flex: 1, justifyContent: 'center', alignItems: 'center' }} info>
                          <Text style={{ fontSize: 13, color: 'white', backgroundColor: 'transparent' }}>{item.unread}</Text>
                        </View>
                      )}
                    </View>
                    <Text numberOfLines={2} style={styles.textLastChat}>{item.lastMessage}</Text>
                  </Body>
                </Left>
              </CardItem>
            </TouchableOpacity>
            {/* <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/> */}
            </Swipeout>
          )}
        />
      </View>
    )
  }

 render() {
   
   const { actions, room } = this.props;

    if(!this.state.userId && room.dataLoaded) {
      return (
        <Login navigation={this.props.navigation}/>
      )
    }
    else {
      return (
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
            <Container>
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Pesan</Title>
                </Body>
                <Right style={styles.navBarWidth}>
                  {
                    (this.state.userId != null && this.state.datas.email_confirm && this.state.datas.role !=='Dokter' && this.state.datas.email_confirm !== 'No Data') &&
                    <Button transparent onPress={() =>  this.props.navigation.navigate('NewChat')}>
                      <Icon style={styles.headerIcon} name='plus'/>
                    </Button>
                  }
                </Right>
              </Header>
                  {!room.dataLoaded ? <Spinner color="#944ea5"/>
                  : !this.state.datas.email_confirm 
                  ? this.renderEmailConfirmation() 
                  : this.state.datas.email_confirm === 'No Data' 
                  ? 
                  (
                    <TouchableOpacity onPress={() => {this.setState({ dataLoaded: false }); this.loadData()}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                      <Text style={{ fontSize: 24, fontFamily: 'Montserrat', color: 'grey' }}>Tap untuk refresh</Text>
                    </TouchableOpacity>
                  ) 
                  : this.renderChatList()}
            </Container>
          </Image>
        );
    }
  }
}

const mapStateToProps = state => ({
  state: state.chatCounter,
  room: state.pesanReducer
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(chatCounterActions, dispatch),
  pesanActions: bindActionCreators(pesanActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(PesanReduxScreen)