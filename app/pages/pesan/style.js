const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
    maxWidth: 40
  },
  navBarColor:{
    color: '#fff',
    paddingLeft:5,
    fontSize:24
  },
  balooThambi:{
        fontFamily: 'BalooThambi-Regular',
        fontSize:16,
        marginTop: (Platform.OS === 'android') ? -8 : 4,
    },
  navBarTitle:{
    fontFamily: 'BalooThambi-Regular',
    fontWeight: '400',
    color: '#fff',
    fontSize: 24,
    paddingLeft: (Platform.OS === 'android') ? 0 : 0
  },
  headerIcon:{
    color:'#fff',
    fontSize:24
  },
  headerIcon2:{
    color:'#fff',
    width: 32,
    fontSize:24
  },
  navBarStyle:{
    backgroundColor:'#499FFD',
    height: 70,
    borderBottomColor: '#499FFD',
  },
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'BalooThambi-Regular',
  		color: '#489FFD',
  		fontSize: 22
  	},
    textUsernameDokter: {
      fontFamily: 'BalooThambi-Regular',
      color: '#79C948',
      fontSize: 22
    },
  	textTime: {
  		fontFamily: 'Montserrat',
  		color: '#C1BFB8',
      fontSize: 12,
      marginTop: 10
  	},
  	textLastChat: {
  		fontFamily: 'Montserrat',
  		color: '#000',
      fontSize: 14,
      width: "70%"
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		marginTop: 5,
  		lineHeight: 30
  	},
  	// Footer Style
  	navBarFooterStyle:{
		backgroundColor:'#ffffff',
	},
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
  buttonLightBlue:{
        height:50,
        marginBottom:10,
        marginTop:10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor:'#499FFD',
    },
    textGrey: {
        color: '#aaaaaa',
        fontSize: 14,
        marginTop:4,
        fontFamily: 'Montserrat-Regular'
    },
	buttonNavbarText:{
		flexDirection: 'column',
		marginBottom: 5
	},
	footerIconInactive:{
		color: '#B2B2B2',
    	fontSize: 22,
	},
	footerIconActive:{
		color: '#3B5998',
    	fontSize: 22,
	},
	footerTextInactive:{
		color: '#B2B2B2',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	footerTextActive:{
		color: '#3B5998',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	// Search
	dirContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    inputText: {
        backgroundColor:'#ffffff',
        marginTop:10,
        marginBottom:5,
        paddingLeft:20,
    },
    inputCustomFont: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14
    },
    buttonPadding:{
    marginTop: 10,
    width: 350,
    flex: 1,
    right: 10,
    left: 10,
    alignItems: 'center'
  },
    input:{
    backgroundColor:'#fffae6',
    marginTop:10,
    marginBottom:5,
    paddingLeft:20,
    borderColor:'#499FFD',
    borderWidth:1,
    borderStyle:'dashed'
  },
    inputIcon: {
    color: '#489FC5',
    width: 32,
    fontSize:20
  },
  placeholderStyles: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18
  },
    textDivider: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		backgroundColor: 'transparent',
  		fontSize: 15,
  		marginLeft: 25,
  		paddingTop: 10,
  		paddingBottom: 10,
  	},

}