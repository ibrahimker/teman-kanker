const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	logo:{
		width:230,
		height:250,
		marginTop: 30
	},
	logo4:{
		height:150,
		resizeMode:'contain',
		alignItems: 'left',
	},
	logo2:{
		flex:1,
		height:200,
		margin: 20,
		backgroundColor: '#fff',
		padding:10,
		flex: 1,
		alignItems: 'center'
	},
	logo3:{
		height:200,
		resizeMode:'contain'
	},
	aboutText:{
		paddingTop:15,
		fontSize:16,
		fontFamily:'Montserrat-Regular',
		lineHeight: 24,
		textAlign: 'center',
	},
	aboutText2:{
		paddingTop:5,
		fontSize:16,
		fontFamily:'Montserrat-Regular',
		lineHeight: 24,
	},
	header1:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#489FFD',
		fontSize: 36,
		marginTop: 20,
		flex:1,
		flexDirection:'column',
    	alignItems: 'center',

	},
	center:{
		justifyContent: 'center',
    	alignItems: 'center',
    	margin: 40,
	},
	navBarWidth:{
		maxWidth: 40
	},
	navBarWidthLeft:{
		maxWidth: 120,
		marginLeft:5,
	},
	navBarWidthRight:{
		maxWidth: 120,
		marginRight:5,
	},
	navBarColor:{
		color: '#fff',
		fontSize:24,
	},
	navBarTitle:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#fff',
		fontSize: 24
	},
	navBarStyle:{
		backgroundColor:'transparent',
		height: 70,
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	backgroundImage2:{
        resizeMode: 'stretch',
        borderRadius: 5,
        height: 310,        
        width: 265,
        top: 40,
    },

}