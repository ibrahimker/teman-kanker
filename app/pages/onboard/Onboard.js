import React, { Component } from 'react';
import {Text,View,Button,Container,Content,Header,Left,Body,Right,Title,Footer,FooterTab,Tabs} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage, Linking} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class OnboardScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    'headerTitle':'Beranda',
    'screen':'0',
  }

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    //kalau sudah login, langsung ke home
    AsyncStorage.getItem('isLoggedIn', (err, result) => {
      if(result == "true"){
        this.props.navigation.navigate('Tab');        
      }
    });
  }



 render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#489ffc');
      return (
      <Container>
        <Image source={images.backgroundHome} style={styles.backgroundImage}>            
          <Content>
            <Grid>
              <Col style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',}}>
                <Image source={images.onboardingBelajar} style={styles.backgroundImage2} />
              </Col>
            </Grid>
            <View style={styles.center}>
              <Text style={styles.header1}>Belajar</Text>
              <Text style={styles.aboutText}>Pelajari tentang kanker. Penyebab, gejala, cara mengatasi, dan bagaimana cara mencegahnya.</Text>
            </View>
          </Content>
          <Header noShadow style={styles.navBarStyle}>
            <Left style={styles.navBarWidthLeft}>
              <Text style={{fontFamily: 'Montserrat-Regular', fontWeight: '500', color: 'rgba(134, 95, 168, 0.5)', fontSize: 14,}}>Kembali</Text>
            </Left>
            <Body style={styles.navBarCentered}>
              <Text style={{fontFamily: 'Montserrat-Regular', fontWeight: '500', color: '#865fa8', fontSize: 14,}} onPress={() => this.props.navigation.navigate('Tab')}>Lewati</Text>
            </Body>
            <Right style={styles.navBarWidthRight}>
              <Text style={{fontFamily: 'Montserrat-Regular', fontWeight: '500', color: '#865fa8', fontSize: 14,}} onPress={() => this.props.navigation.navigate('Onboard2')}>Selanjutnya</Text>
            </Right>
          </Header>
        </Image>
      </Container>
    );
  }
}