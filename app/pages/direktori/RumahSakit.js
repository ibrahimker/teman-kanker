import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Footer,FooterTab,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs,List,ListItem, Toast} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class TabRumahSakitScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    listRumahSakit: [],
    dataLoaded: false
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      listRumahSakit: [],
      dataLoaded: false
    };
  }

  componentDidMount() {
    fetch('https://temankanker.com/api/Hospitals')
    .then((response) => response.json())
    .then((data) => {
      return this.setState({ listRumahSakit: data, dataLoaded: true });
    })
    .catch(error => {
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    });
  }

  renderRumahSakit() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return this.state.listRumahSakit.map((dataRumahSakit,index) => (
        <TouchableOpacity key={dataRumahSakit.id}>
          <ListItem style={{paddingTop:25, paddingBottom:25}} onPress={() => this.props.navigation.navigate('RumahSakitDetil', {data: dataRumahSakit})}> 
            <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com'+dataRumahSakit.photo }} />
            <Body>
              <Text style={styles.hospitalTitle}>{dataRumahSakit.name}</Text>
              <Text style={styles.hospitalLocation}>{dataRumahSakit.address}</Text>
              {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
            </Body>
          </ListItem>
          <View style={{borderBottomWidth: 1, borderStyle:'dashed',paddingLeft:30,paddingRight:30,borderBottomColor: '#F5AF3E'}}/>
        </TouchableOpacity>
      ));
    }
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');
      return (
        <Image source={images.backgroundHome} style={styles.backgroundImage}>
          <Container>
              <Content>
                  <List>
                    {this.renderRumahSakit()} 
                  </List>                
              </Content>
          </Container>
        </Image>

    );
  }
}