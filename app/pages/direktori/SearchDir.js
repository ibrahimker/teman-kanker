import React, { Component } from 'react';
import {Text,View,Toast,Form,Item,Input,Drawer,Spinner,Button,Container,Content,Header,Footer,FooterTab,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs,List,ListItem} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class SearchDirScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    listDokter: [],
    listRumahSakit: [],
    listFaskes: [],
    dataLoaded: false
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      listDokter: [],
      listRumahSakit: [],
      listFaskes: [],
      dataLoaded: false
    };
  }

  componentDidMount() {

  }

  renderLoading() {
    if(this.state.sendingNote) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 12, margin:12, textAlign:'center'}}>Sedang Mencari, Mohon Tunggu..</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  validateInput() {
    let query = this.state.query;
    const { navigate } = this.props.navigation;

    this.setState({ loging: true });
    this.setState({sendingNote: true});

    if(query != "") {
      fetch('https://temankanker.com/api/Doctors?filter[where][or][0][name][regexp]=/' + encodeURIComponent(query) + '/ig&filter[where][or][1][speciality][regexp]=/' + encodeURIComponent(query) + '/ig')
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ listDokter: data, dataLoaded: true, sendingNote: false });
      })
      .done();

      fetch('https://temankanker.com/api/Hospitals?filter[where][or][0][name][regexp]=/' + encodeURIComponent(query) + '/ig&filter[where][or][1][city][regexp]=/' + encodeURIComponent(query) + '/ig')
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ listRumahSakit: data, dataLoaded: true, sendingNote: false });
      })
      .done();

      fetch('https://temankanker.com/api/Others?filter[where][or][0][name][regexp]=/' + encodeURIComponent(query) + '/ig&filter[where][or][1][city][regexp]=/' + encodeURIComponent(query) + '/ig')
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ listFaskes: data, dataLoaded: true, sendingNote: false });
      })
      .done();
    }
    else {
      Toast.show({
        supportedOrientations: ['portrait','landscape'],
        text: 'Field must not be empty!',
        position: 'bottom',
        buttonText: 'Okay',
        type:'danger',
        duration: 1000
      })
    }
  }

  renderDokter() {
      return this.state.listDokter.map((dataDokter,index) => (
        <TouchableOpacity>
          <ListItem key={dataDokter.id} style={{paddingTop:25, paddingBottom:25, backgroundColor:'transparent', borderColor: 'transparent'}} onPress={() =>  this.props.navigation.navigate('ProfileDoctor', {data: dataDokter})}>
            <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com' + dataDokter.photo }} />
            <Body>
              <Text style={styles.hospitalTitle}>{dataDokter.name}</Text>
              <Text style={styles.hospitalLocation}>{dataDokter.speciality}</Text>
              {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
            </Body>
          </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
        </TouchableOpacity>
      ));
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');
      return (
        <Image source={images.backgroundHome} style={styles.backgroundImage}>
          <Container>
              <Header noShadow hasTabs style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navbarIcon} name='close'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Pencarian</Title>
                </Body>
                <Right style={styles.navBarWidth}>
                  {
                    // <Button transparent onPress={() => this.goToSearch()}>
                    //   <Icon style={styles.headerIcon} name='search'/>
                    // </Button>
                  }
                </Right>
              </Header>
              <Content>
                  <Form style={{padding: 15}}>
                    <Item rounded style={styles.input}>
                          <Icon style={styles.inputIcon} active name='search' />
                          <Input style={styles.placeholderStyles} type="text" placeholder='Kata Pencarian' placeholderTextColor='#93938E' onChangeText={(query) => this.setState({query})}/>
                    </Item>
                    <Button rounded onPress={() => this.validateInput()} style={styles.buttonLightBlue} block>
                        <Text style={styles.balooThambi}>Cari</Text>
                    </Button>
                  </Form>
                  {this.renderLoading()}
                  <List>
                    {this.renderRumahSakit()} 
                    {this.renderDokter()} 
                    {this.renderFaskes()} 
                  </List>                
              </Content>
          </Container>
        </Image>

    );
  }

  renderFaskes() {
      return this.state.listFaskes.map((dataDokters,index) => (
        <TouchableOpacity key={dataDokters.id}>
          <ListItem style={{paddingTop:25, paddingBottom:25, backgroundColor:'transparent', borderColor: 'transparent'}} onPress={() => this.props.navigation.navigate('OtherDetil', {data: dataDokters})}> 
            <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com'+dataDokters.photo }} />
            <Body>
              <Text style={styles.hospitalTitle}>{dataDokters.name}</Text>
              <Text style={styles.hospitalLocation}>{dataDokters.city}</Text>
              {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
            </Body>
          </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
        </TouchableOpacity>
      ));
  }

  renderRumahSakit() {
      return this.state.listRumahSakit.map((dataRumahSakit,index) => (
        <TouchableOpacity key={dataRumahSakit.id}>
          <ListItem style={{paddingTop:25, paddingBottom:25, backgroundColor:'transparent', borderColor: 'transparent'}} onPress={() => this.props.navigation.navigate('RumahSakitDetil', {data: dataRumahSakit})}> 
            <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com'+dataRumahSakit.photo }} />
            <Body>
              <Text style={styles.hospitalTitle}>{dataRumahSakit.name}</Text>
              <Text style={styles.hospitalLocation}>{dataRumahSakit.address}</Text>
              {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
            </Body>
          </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
        </TouchableOpacity>
      ));
  }

}