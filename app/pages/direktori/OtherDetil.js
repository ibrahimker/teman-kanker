import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail,List,ListItem,Spinner,Toast} from 'native-base';

import Communications from 'react-native-communications';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, WebView } from 'react-native';
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js'
import { Col, Row, Grid } from 'react-native-easy-grid';
import images from '../../config/images.js';
import styles from './style.js';

import HTMLView from 'react-native-htmlview';
const styles2 = StyleSheet.create({
  p: {
    fontFamily: 'Montserrat-Regular',
    fontSize:14,
    color:'#000000',
    marginTop:-10,
    lineHeight: 24,
    marginBottom:-16
  },
  strong: {
    fontFamily: 'Montserrat-Bold'
  },
  b: {
    fontFamily: 'Montserrat-Bold'
  },
  h1: {
    fontFamily: 'Montserrat-Regular'
  },
  i: {
    fontFamily: 'Montserrat-Regular'
  },
  u: {
    fontFamily: 'Montserrat-Regular'
  },
  h2: {
    fontFamily: 'Montserrat-Regular'
  },
  h3: {
    fontFamily: 'Montserrat-Regular'
  },
  li: {
    fontFamily: 'Montserrat-Regular'
  },
  ul: {
    fontFamily: 'Montserrat-Regular'
  },
  ol: {
    fontFamily: 'Montserrat-Regular'
  }
});

export default class OtherDetil extends Component {
  static navigationOptions = {
    title: null,
    header: null
  };

  state = {
    id: '',
    data: {},
    dataLoaded: false
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.data.id;
    fetch('https://temankanker.com/api/Others/' + id)
    .then((response) => response.json())
    .then((responseJson) => {
        console.log(responseJson);
         this.setState({
            data: responseJson,
            dataLoaded: true
         })
      })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    });
  }

  renderLoading() {
    if(!this.state.dataLoaded) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Mengunduh data...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    StatusBar.setBarStyle('light-content', true);
    
    return (
        <Container>
            <Header noShadow style={styles.navBarStyle2}>
                <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='arrow-left'/>
                    </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                
                </Body>
                <Right style={styles.navBarWidth}>
                </Right>
            </Header>
            {this.state.dataLoaded && (
                <Content style={{backgroundColor: '#ffffff', marginTop: -90, zIndex: -10}}>
            
                <Image source={{uri: 'https://temankanker.com'+this.state.data.photo}} style={{height: 300, width: null, flex: 1}}/>
                <Image source={images.backgroundHome} style={styles.backgroundImage}>
                <Text style={styles.hospitalTitle2}>{this.state.data.name}</Text>
                    <View style={{padding:20,paddingTop:10, backgroundColor: 'transparent'}}>
                        <View>
                            <Text style={styles.hospitalSubtitle}>DESKRIPSI</Text>
                            <View style={{paddingTop:10}}>
                              <HTMLView
                                value={this.state.data.description}
                                stylesheet={styles2}
                              />
                            </View>
                        </View>
                        <View style={{ marginTop: 15 }}>
                            <Text style={styles.hospitalSubtitle}>ALAMAT</Text>
                            <Grid>
                                <Col size={50}>
                                    <Text style={styles.hospitalContent}>{this.state.data.address}</Text>
                                </Col>
                                <Col size={50}>
                                    <Right>
                                        <Button onPress={() => this.props.navigation.navigate('Peta', {data: this.state.data.address})} rounded small style={styles.buttonOrange}><Text style={styles.buttonText}>Lihat Peta</Text></Button>
                                    </Right>
                                </Col>
                            </Grid>
                        </View>
                        <View style={{ padding: 20, paddingTop: 25, paddingBottom: 25 }}>
                            <Button onPress={() => Communications.phonecall(this.state.data.phone, true)} rounded block style={styles.buttonOrange}><Text style={styles.buttonText}>Telfon Rumah Sakit</Text></Button>
                        </View>
                    </View>
                </Image>
                </Content>
            )}
            
            {this.renderLoading()}
        </Container>
    );
    }
  }

