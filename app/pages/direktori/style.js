const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	headerIcon: {
		color: '#fff',
		width: 32,
		fontSize:24
	},
	navBarColor:{
        color: '#fff',
        paddingLeft:5,
        fontSize:24
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#fff',
        fontSize: 24,
        paddingLeft: (Platform.OS === 'android') ? 0 : 0
    },
    headerTabStyle:{
    	backgroundColor:'#499FFD',
    	color:'#B2B2B2'
    },
  navBarStyle:{
      // backgroundColor:'rgba(72,157,255,0.5)',
      backgroundColor:'#499FFD',
      height: 70,
	},
  navBarStyle2:{
      backgroundColor:'#499FFD',
      height: 70,
  },
  navBarWidth:{
    maxWidth: 40,
    backgroundColor: 'transparent'
  },
  navBarWidth2:{
    maxWidth: 80,
    backgroundColor: 'transparent'
  },
    navbarIcon:{
    	fontSize:24,
    	color:'#fff',
    	paddingLeft:10
    },
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		marginTop: 5,
  		lineHeight: 30
	},
	hospitalTitle:{
  		fontFamily: 'BalooThambi-Regular',
		color: '#499FFD',
		fontSize: 18,
    lineHeight: 22
	},
  	hospitalTitle2:{
  		fontFamily: 'BalooThambi-Regular',
		color: 'white',
		backgroundColor: '#499FFD',
		fontSize: 18,
    lineHeight: 22,
		padding: 20,
		paddingTop: 15,
    paddingBottom: (Platform.OS === 'android') ? 20 : 10
	},
	hospitalSubtitle: {
		fontFamily: 'Montserrat',
		fontSize: 12,
    fontWeight: '500',
		color: 'grey',
    marginBottom: 5,
    marginTop: 10
	},
	hospitalContent: {
		fontFamily: 'Montserrat-Regular',
		fontSize: 14,
    lineHeight: 24
	},
  	hospitalLocation:{
  		fontFamily: 'Montserrat-Regular',
        color: '#000',
        fontSize: 14,
  	},
    hospitalLocation2:{
      fontFamily: 'Montserrat-Regular',
        color: '#7E7E7E',
        fontSize: 14,
    },
  	hospitalDistance:{
  		fontFamily: 'Montserrat',
        color: '#8c8884',
        fontSize: 16,
	},
	buttonLightBlue:{
    height:50,
    marginBottom:10,
    marginTop:10,
    shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
    backgroundColor:'#499FFD',
  },
  buttonOrange:{
    marginBottom:10,
    shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
    backgroundColor:'#8DC63F',
  },
	buttonText: {
		fontFamily: 'BalooThambi-Regular',
	},
	doctorName: {
		fontFamily: 'BalooThambi-Regular',
		color: '#499FFD',
	},
  	thumbnailIcon :{
  		borderRadius:10
  	},
    input:{
    backgroundColor:'#fffae6',
    marginTop:10,
    marginBottom:5,
    paddingLeft:20,
    borderColor:'#499FFD',
    borderWidth:1,
    borderStyle:'dashed'
  },
  inputIcon: {
    color: '#489FC5',
    width: 32,
    fontSize:20
  },
  placeholderStyles: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18
  },
  balooThambi:{
    fontFamily: 'BalooThambi-Regular',
    fontSize:16,
      marginTop: (Platform.OS === 'android') ? -8 : 4,
  },
    loadingOverlay: {
			opacity: 0.9,
			backgroundColor: 'transparent',
			flex: 1,
			width: null,
			height: null,
			justifyContent: 'center',
			alignItems: 'center'
	},
  borderBottomDir: {
    borderBottomWidth: 1, 
    borderStyle:'dashed',
    paddingLeft:30,
    paddingRight:30,
    borderBottomColor: '#F5AF3E'
  }
}