import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Badge,Left,Body,Right,Title,Card,CardItem,Toast,Thumbnail,Tab, Tabs, List, ListItem, ActionSheet} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage, FlatList} from 'react-native';

import FooterTabNavigation from '../../components/footerTab/FooterTab.js';

import TabRumahSakit from './RumahSakit.js';
import TabDokter from './Dokter.js';

import images from '../../config/images.js';
import styles from './style.js';

var FILTER = [
  "Semua",
  "Jakarta Pusat",
  "Jakarta Barat",
  "Jakarta Selatan",
  "Makassar",
  "Medan",
  "Cancel"
];
var CANCEL_INDEX = 6;

export default class DirektoriScreen extends Component {
  static navigationOptions = {
    header: null,
    tabBarIcon: (
      <Icon style={{ fontSize: 20 }} name='list'/>
    ),
    tabBarLabel: 'Direktori'
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    filterCity: ["Semua"],
    listRumahSakit: [],
    listDokter: [],
    listOther: [],
    dataLoaded: false
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.news[index].reminder = value;
    this.setState(stateCopy);
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      filterCity: ["Semua"],
      listRumahSakit: [],
      listDokter: [],
      listOther: [],
      dataLoaded: false,
      refreshRumahSakit: false,
      refreshDokter: false,
      refreshLainnya: false
    };
  }

  componentDidMount() {
    this.retrieveRumahSakit()
    this.retrieveDokter()
    this.retrieveLainnya()    

    // fetch('https://temankanker.com/api/Hospitals?filter[fields][city]=true')
    // .then((response) => response.json())
    // .then((data) => {
    //   // this.setState({filterCity: data});
    //   for (var i = 0; i < data.length; i++) {
    //     if(!this.state.filterCity.contains(this[i])){
    //       this.state.filterCity.push(this[i]);
    //     }
    //   }
    // })
  }

  retrieveRumahSakit() {
    fetch('https://temankanker.com/api/Hospitals')
    .then((response) => response.json())
    .then((data) => {
      this.setState({ listRumahSakit: data, dataLoaded: true, refreshRumahSakit: false });
      AsyncStorage.setItem("rsData", JSON.stringify(data));
    })
    .catch(error => {
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  retrieveDokter() {
    fetch('https://temankanker.com/api/Doctors?filter[include][relation]=getHospital')
    .then((response) => response.json())
    .then((data) => {
      this.setState({ listDokter: data, dataLoaded: true, refreshDokter: false });
      AsyncStorage.setItem("dokterData", JSON.stringify(data));
    })
    .catch(error => {
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  retrieveLainnya() {
    fetch('https://temankanker.com/api/Others')
    .then((response) => response.json())
    .then((data) => {
      this.setState({ listOther: data, dataLoaded: true, refreshLainnya: false });
      AsyncStorage.setItem("otherData", JSON.stringify(data));
    })
    .catch(error => {
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  refreshRumahSakit = () => {
    this.setState({ refreshRumahSakit: true })
    this.retrieveRumahSakit()
  }

  refreshDokter = () => {
    this.setState({ refreshDokter: true })
    this.retrieveDokter()
  }

  refreshLainnya = () => {
    this.setState({ refreshLainnya: true })
    this.retrieveLainnya()
  }

  renderRumahSakit() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return(
        <FlatList
          keyExtractor={(item, index) => item.id}
          data = {this.state.listRumahSakit}
          refreshing = {this.state.refreshRumahSakit}
          onRefresh = {this.refreshRumahSakit}
          renderItem = {({item, index}) => (
              <TouchableOpacity key={item.id}>
                <ListItem style={{paddingTop:25, paddingBottom:25, borderWidth: 0, borderColor: 'transparent', borderWidth: 0, borderColor: 'transparent'}} onPress={() => this.props.navigation.navigate('RumahSakitDetil', {data: item})}> 
                  <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com'+item.photo }} />
                  <Body>
                    <Text style={styles.hospitalTitle}>{item.name}</Text>
                    <Text style={styles.hospitalLocation}>{item.city}</Text>
                    {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
                  </Body>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
              </TouchableOpacity>
          )}
        />
      )
    }
  }

  renderDokter() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return(
        <FlatList
          keyExtractor={(item, index) => item.id}
          data = {this.state.listDokter}
          refreshing = {this.state.refreshDokter}
          onRefresh = {this.refreshDokter}
          renderItem = {({item, index}) => (
              <TouchableOpacity>
                <ListItem key={item.id} style={{paddingTop:25, paddingBottom:25, borderWidth: 0, borderColor: 'transparent'}} onPress={() =>  this.props.navigation.navigate('ProfileDoctor', {data: item})}>
                  <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com' + item.photo }} />
                  <Body>
                    <Text style={styles.hospitalTitle}>{item.name}</Text>
                    <Text style={styles.hospitalLocation}>{item.speciality}</Text>
                    <Text style={styles.hospitalLocation2}>{item.getHospital.name}</Text>
                    {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
                  </Body>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
              </TouchableOpacity>
          )}
        />
      )
    }
  }

  renderLainnya() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return(
        <FlatList
          keyExtractor={(item, index) => item.id}
          data = {this.state.listOther}
          refreshing = {this.state.refreshLainnya}
          onRefresh = {this.refreshLainnya}
          renderItem = {({item, index}) => (
              <TouchableOpacity key={item.id}>
                <ListItem style={{paddingTop:25, paddingBottom:25, borderWidth: 0, borderColor: 'transparent'}} onPress={() => this.props.navigation.navigate('OtherDetil', {data: item})}> 
                  <Thumbnail style={styles.thumbnailIcon} size={80} source={{ uri: 'https://temankanker.com'+item.photo }} />
                  <Body>
                    <Text style={styles.hospitalTitle}>{item.name}</Text>
                    <Text style={styles.hospitalLocation}>{item.city}</Text>
                    {/*<Text style={styles.hospitalDistance}>8.0 KM</Text>*/}
                  </Body>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
              </TouchableOpacity>
          )}
        />
      )
    }
  }

goToSearch(){
    const { navigate } = this.props.navigation;
    navigate('SearchDir');
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
      return (
      <Container>
        <Header noShadow hasTabs style={styles.navBarStyle}>
          <Left style={styles.navBarWidth}>
          </Left>
          <Left style={styles.navBarWidth}>
          </Left>
          <Body style={styles.navBarCentered}>
            <Title style={styles.navBarTitle}>Direktori</Title>
          </Body>
          <Right style={styles.navBarWidth}>
            <Button transparent onPress={() => this.goToSearch()}>
                <Icon style={styles.headerIcon} name='search'/>
            </Button>
          </Right>
          <Right style={styles.navBarWidth}>
              <Button transparent 
                onPress={() =>
                ActionSheet.show(
                  {
                    options: FILTER,
                    cancelButtonIndex: CANCEL_INDEX,
                    title: "Filter berdasarkan"
                  },
                  buttonIndex => {
                    if(FILTER[buttonIndex] == "Semua" || FILTER[buttonIndex] == "Cancel"){
                      AsyncStorage.getItem("rsData", (err, result) => {
                      this.setState({
                        listRumahSakit: JSON.parse(result),
                      })
                    })
                    AsyncStorage.getItem("dokterData", (err, result) => {
                      this.setState({
                        listDokter: JSON.parse(result),
                      })
                    })
                    AsyncStorage.getItem("otherData", (err, result) => {
                      this.setState({
                        listOther: JSON.parse(result),
                      })
                    })
                    }
                    else{
                      AsyncStorage.getItem("rsData", (err, result) => {
                        this.setState({
                          listRumahSakit: JSON.parse(result)
                        })
                        let filteredRS = this.state.listRumahSakit.filter((rs) => {
                          return rs.city == FILTER[buttonIndex]
                        })
                        this.setState({
                          listRumahSakit: filteredRS
                        })
                      })
                      AsyncStorage.getItem("dokterData", (err, result) => {
                        this.setState({
                          listDokter: JSON.parse(result)
                        })
                        let filteredDokter = this.state.listDokter.filter((dokter) => {
                          return dokter.getHospital.city == FILTER[buttonIndex]
                        })
                        this.setState({
                          listDokter: filteredDokter
                        })
                      })
                      AsyncStorage.getItem("otherData", (err, result) => {
                        this.setState({
                          listOther: JSON.parse(result)
                        })
                        let filteredOther = this.state.listOther.filter((other) => {
                          return other.city == FILTER[buttonIndex]
                        })
                        this.setState({
                          listOther: filteredOther
                        })
                      })
                    }
                  }
                )}
              >
                <Icon style={styles.headerIcon} name='filter'/>
              </Button>
            </Right>
        </Header>
        <Image source={images.backgroundHome} style={styles.backgroundImage}>
          <Tabs tabBarUnderlineStyle={{borderBottomWidth:0}}>
              <Tab heading="Rumah Sakit" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
                  <Image source={images.backgroundHome} style={styles.backgroundImage}>
                    <Container>
                          {this.renderRumahSakit()} 
                    </Container>
                  </Image>
              </Tab>
              <Tab heading="Dokter" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
                  <Image source={images.backgroundHome} style={styles.backgroundImage}>
                    <Container>
                          {this.renderDokter()} 
                    </Container>
                  </Image>
              </Tab>
              <Tab heading="Lainnya" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
                  <Image source={images.backgroundHome} style={styles.backgroundImage}>
                    <Container>
                          {this.renderLainnya()} 
                    </Container>
                  </Image>
              </Tab>
          </Tabs>
        </Image>
      </Container>
    );
  }
}