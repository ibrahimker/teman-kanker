import React, { Component } from 'react';
import {Text,Form,Item,Input,View,Drawer,Spinner,Button,Container,Content,Header,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs,Toast, ActionSheet} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage,Share} from 'react-native';
import moment from 'moment';
import FooterTabNavigation from '../../components/footerTab/FooterTab.js'
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

var FILTER = [
  "Semua",
  "Artikel",
  "Testimoni",
  "Berita",
  "Cancel"
];
var CANCEL_INDEX = 4;

export default class SearchInfoScreen extends Component {

  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    news: [],
    likes: [],
    dataLoaded: false
  }
  
  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'userId':'',
      'id':'',
      news: [],
      likes: [],
      dataLoaded: false
    };

    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });

      fetch('https://temankanker.com/api/InformationLikes?filter[where][userId]=' + result)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        return this.setState({ likes: data });
      })
      .done();
    });
  }

  componentDidMount() {
    
  }

  renderLoading() {
    if(this.state.sendingNote) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 12, margin:12, textAlign:'center'}}>Sedang Mencari, Mohon Tunggu..</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  validateInput() {
    let query = this.state.query;
    const { navigate } = this.props.navigation;

    this.setState({ loging: true });
    this.setState({sendingNote: true});

    if(query != "") {
      fetch('https://temankanker.com/api/Information?filter[where][name][regexp]=/' + encodeURIComponent(query) + '/ig')
      .then((response) => response.json())
      .then((data) => {
        this.setState({ news: data, dataLoaded: true, sendingNote: false });
        AsyncStorage.setItem("newsData", JSON.stringify(data));
      })
      .done();
    }
    else {
      Toast.show({
        supportedOrientations: ['portrait','landscape'],
        text: 'Field must not be empty!',
        position: 'bottom',
        buttonText: 'Okay',
        type:'danger',
        duration: 1000
      })
    }
  }

  share(title,content){
    Share.share({
      message: title+' --- '+content.substring(0,200)+'...',
      url: 'Download Teman Kanker at Play Store',
      title: 'Share to Social Media'
    }, {
      // Android only:
      dialogTitle: title,
      // iOS only:
      excludedActivityTypes: [
        'com.apple.UIKit.activity.PostToTwitter'
      ]
    })
  }


  back(){

    fetch('https://temankanker.com/api/Information?filter[order]=created_at%20desc')
      .then((response) => response.json())
      .then((data) => {
        AsyncStorage.setItem("newsData", JSON.stringify(data));
      })
      .done();
    this.props.navigation.goBack()
  }

  like(user, info, newsIdx){
    if(user!=''){
      if(user!=null){
        this.state.likes.push({userId: user, informationId: info});
        this.state.news[newsIdx].like_count = this.state.news[newsIdx].like_count + 1;
        this.forceUpdate();
        fetch('https://temankanker.com/api/InformationLikes/like', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "userId": user,
            "informationId":info
          })
        })
        .then((response) => { 

        })
      }
    }
  }

  unlike(user, info, idx, newsIdx) {
    if(user!=''){
      if(user!=null){
        this.state.likes.splice(idx, 1);
        this.state.news[newsIdx].like_count = this.state.news[newsIdx].like_count - 1;
        this.forceUpdate();

        fetch('https://temankanker.com/api/InformationLikes/unlike', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "userId": user,
            "informationId":info
          })
        })
        .then((response) => { 
        })
      }
    }
  }

  likeButton(id, count, newsIdx) {
    button = (
          <Button transparent onPress={() =>  this.like(this.state.userId, id, newsIdx)}>
            <Icon name="heart" style={{color: '#B2B2B2', fontSize: 18}}/>
            <Text style={{marginLeft: 5, color: '#B2B2B2'}}>{count}</Text>
          </Button>
        );

    this.state.likes.map((value, idx, arr) => {
      if(value.informationId == id) {
         button = (
          <Button transparent onPress={() =>  this.unlike(this.state.userId, id, idx, newsIdx)}>
            <Icon name="heart" style={{color: '#d9534f', fontSize: 18}}/>
            <Text style={{marginLeft: 5, color: '#B2B2B2'}}>{count}</Text>
          </Button>
        );
      }
    });

    return button;

  }

  renderArtikel() {
      return this.state.news.map((newz,index) => (
        <Card key={newz.id}>
          <CardItem style={styles.thumbnailMargin}>
            <Left>
              <Thumbnail style={styles.thumbnailSize} source={{uri: 'https://temankanker.com'+newz.photo_author}} />
              <Body style={{marginLeft: 15}}>
                <Text style={styles.textUsername}>{newz.author}</Text>
                <Text style={styles.textUserTitle}>Penulis</Text>
              </Body>
            </Left>
          </CardItem>
          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Artikel', {id : newz.id,articleTitle:newz.name })}>
            <CardItem cardBody>
              <Image source={{uri: 'https://temankanker.com'+newz.photo}} style={{resizeMode: 'cover', height: 200, width: null, flex: 1 }}>
                <Badge style={{ backgroundColor: '#8c5ca0',position:'absolute',bottom:10,left:20 }}>
                  <Text style={{ color: '#fff',fontFamily:'Montserrat-Regular', fontSize:14 }}>{newz.type}</Text>
                </Badge>
              </Image>
            </CardItem>
            <CardItem>
              <Body style={{marginTop: 0}}>
                <Text style={styles.textTitle}>{newz.name}</Text>
              </Body>
            </CardItem>
            <CardItem style={{marginTop: -25, backgroundColor: 'transparent'}}>
              <Grid>
                <Row>
                  <Col>
                    <Text style={styles.textArticleTime}>{moment(newz.created_at).format('DD MMMM YYYY')}</Text>
                  </Col>
                  <Col style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                    <Button transparent onPress={() =>  this.share(newz.name,newz.content)} style={{paddingLeft: 0, paddingRight:0, marginLeft:15}}>
                      <Icon name="share-alt" style={{color: '#B2B2B2', fontSize: 16}}/>
                      <Text style={{marginLeft: 5, color: '#B2B2B2', font: 'Montserrat', fontSize: 14}}>{newz.share_count}</Text>
                    </Button>
                    {this.state.userId ? this.likeButton(newz.id, newz.like_count, index) : (
                      <Button transparent style={{paddingLeft: 0, paddingRight:0, marginLeft:15}}>
                        <Icon name="heart" style={{color: '#B2B2B2', fontSize: 16}}/>
                        <Text style={{marginLeft: 5, color: '#B2B2B2', font: 'Montserrat', fontSize: 14}}>{newz.like_count}</Text>
                      </Button>
                    )}

                    {/* {this.likeButton(newz.id, newz.like_count, index)} */}
                  </Col>
                </Row>
              </Grid>
            </CardItem>
          </TouchableOpacity>
        </Card>
      ));
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');
    return (
      <Container>
          <Header noShadow style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
              <Button transparent onPress={() =>  this.back()}>
                <Icon style={styles.navbarIcon} name='close'/>
               </Button>
             </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Informasi</Title>
            </Body>
            <Right style={styles.navBarWidth}/>
          </Header>
            <Image source={images.backgroundHome} style={styles.backgroundImage}>
          <Content>
               <Form style={{padding: 15}}>
                    <Item rounded style={styles.input}>
                          <Icon style={styles.inputIcon} active name='search' />
                          <Input style={styles.placeholderStyles} type="text" placeholder='Kata Pencarian' placeholderTextColor='#93938E' onChangeText={(query) => this.setState({query})}/>
                    </Item>
                    <Button rounded onPress={() => this.validateInput()} style={styles.buttonLightBlue} block>
                        <Text style={styles.balooThambi}>Cari</Text>
                    </Button>
                  </Form>
                  {this.renderLoading()}
              <View style={{margin:16, marginTop: 5}}>
              {this.renderArtikel()}  
              </View>
          </Content>
            </Image>
      </Container>
    );
  }
}