import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs,Toast, ActionSheet} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage,Share, FlatList, ImageBackground} from 'react-native';
import moment from 'moment';
import FooterTabNavigation from '../../components/footerTab/FooterTab.js'
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

var FILTER = [
  "Semua",
  "Artikel",
  "Testimoni",
  "Berita",
  "Cancel"
];
var CANCEL_INDEX = 4;

export default class InformasiScreen extends Component {

  static navigationOptions = {
    header: null,
    tabBarIcon: (
      <Icon style={{ fontSize: 20 }} name='file'/>
    ),
    tabBarLabel: 'Informasi'
  }

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    news: [],
    likes: [],
    dataLoaded: false,
    loadMore: false,
    loadCounter: 0,
    endOfData: false
  }
  
  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'userId':'',
      'id':'',
      news: [],
      likes: [],
      dataLoaded: false,
      disable_like: false,
      refreshing: false,
      retry: false,
      loadCounter: 0,
      endOfData: false
    };
  }

  componentDidMount() {
    this.retreiveInformation()
  }

  retreiveInformation() {
    console.log(this.state.loadCounter)
    fetch('https://temankanker.com/api/Information?filter[order]=created_at%20desc&filter[limit]=' + 10 + '&filter[skip]=' + this.state.loadCounter * 10)
    .then((response) => response.json())
    .then((data) => {
      console.log(data)
      if(data.length > 0) {
        this.setState({ news: [ ...this.state.news, ...data], loadMore: false });
        AsyncStorage.getItem('userId', (err, result) => {
          this.setState({ 'userId': result });

          fetch('https://temankanker.com/api/InformationLikes?filter[where][userId]=' + result)
          .then((response) => response.json())
          .then((data) => {
            return this.setState({ likes: data, dataLoaded: true, refreshing: false, retry: false });
          })
          .catch(error => {
            Toast.show({
              text: 'Terjadi kesalahan pada sistem kami',
              position: 'top',
              type:'danger',
              buttonText: 'OK',
              duration: 3000
            });
            this.setState({ dataLoaded: true, retry: true })
          })
          .done();
        });

        AsyncStorage.setItem("newsData", JSON.stringify(data));
      }
      else {
        Toast.show({
          text: 'Tidak ada informasi lagi',
          position: 'top',
          type:'info',
          duration: 3000
        });

        this.setState({ loadMore: false })
      }
    })
    .catch(error => {
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
      this.setState({ dataLoaded: true, retry: true })
    })
    .done();
  }

  share(title,content){
    Share.share({
      message: title+' --- '+content.substring(0,140)+'... Baca artikel selengkapnya di aplikasi TemanKanker! https://play.google.com/store/apps/details?id=com.yoai.temankanker"',
      url: 'Download Teman Kanker at Play Store',
      title: 'Share to Social Media'
    }, {
      // Android only:
      dialogTitle: title,
      // iOS only:
      excludedActivityTypes: [
        'com.apple.UIKit.activity.PostToTwitter'
      ]
    })
  }

  like(user, info, newsIdx){
    if(user!=''){
      if(user!=null){

        this.setState({ disable_like: true })

        let likes = this.state.likes
        likes.push({userId: user, informationId: info})

        let news = this.state.news
        news[newsIdx].like_count = news[newsIdx].like_count + 1

        fetch('https://temankanker.com/api/InformationLikes/like', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "userId": user,
            "informationId":info
          })
        })
        .then((response) => { 
            this.setState({ likes: likes, news: news, disable_like: false })
        })
      }
    }
  }

  unlike(user, info, idx, newsIdx) {
    if(user!=''){
      if(user!=null){

        this.setState({ disable_like: true })

        let likes = this.state.likes
        likes.splice(idx, 1)

        let news = this.state.news
        news[newsIdx].like_count = news[newsIdx].like_count - 1

        fetch('https://temankanker.com/api/InformationLikes/unlike', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "userId": user,
            "informationId":info
          })
        })
        .then((response) => { 
          this.setState({ likes: likes, news: news, disable_like: false })
        })
      }
    }
  }

  likeButton(id, count, newsIdx) {
    button = (
          <Button disabled={this.state.disable_like} transparent onPress={() =>  this.like(this.state.userId, id, newsIdx)}>
            <Icon name="heart" style={{color: '#B2B2B2', fontSize: 18}}/>
            <Text style={{marginLeft: 5, color: '#B2B2B2'}}>{count}</Text>
          </Button>
        );

    this.state.likes.map((value, idx, arr) => {
      if(value.informationId == id) {
         button = (
          <Button disabled={this.state.disable_like} transparent onPress={() =>  this.unlike(this.state.userId, id, idx, newsIdx)}>
            <Icon name="heart" style={{color: '#d9534f', fontSize: 18}}/>
            <Text style={{marginLeft: 5, color: '#B2B2B2'}}>{count}</Text>
          </Button>
        );
      }
    });

    return button;
  }

  onRefresh = () => {
    this.setState({ refreshing: true })
    this.retreiveInformation()
  }

  renderFooter() {
    return (
      <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', width: null, paddingBottom: 10}}>
        <Button rounded disabled={this.state.loadMore} onPress={() => this.setState({ loadMore: true, loadCounter: ++this.state.loadCounter }, this.retreiveInformation())} style={[styles.buttonPurple, {alignSelf: 'center', width: 150}]}>
          {this.state.loadMore ? (
            <Spinner style={{ flex: 1, width: null, alignItems: 'center' }} color="white" size={'small'}/>
          ) : (
            <Text style={[styles.balooThambi, {textAlign: 'center', flex: 1, width: null}]}>Load More</Text> 
          )}
        </Button>
      </View>
    )
  }

  renderArtikel() {
    
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else if(this.state.retry && this.state.dataLoaded) {
      return (
        <TouchableOpacity onPress={() => {this.setState({ dataLoaded: false, retry: false }); this.onRefresh()}} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 24, fontFamily: 'Montserrat', color: 'grey' }}>Tap untuk refresh</Text>
        </TouchableOpacity>
      )
    }
    else {
      return (
         <FlatList
          keyExtractor={(item, index) => item.id}
          data = {this.state.news}
          ListFooterComponent = {this.renderFooter()}
          refreshing = {this.state.refreshing}
          onRefresh = {this.onRefresh}
          renderItem = {({item, index}) => (
            <View style={{padding: 16, paddingTop: 5, paddingBottom: 5}}>
              <Card style={{ elevation: 0, shadowOpacity: 0, borderColor: 'white' }} key={item.id}>
                <CardItem style={styles.thumbnailMargin}>
                  <Left>
                    <Thumbnail style={styles.thumbnailSize} source={{uri: 'https://temankanker.com'+item.photo_author+'?filter=[order]=type%20ASC'}} />
                    <Body style={{marginLeft: 15}}>
                      <Text style={styles.textUsername}>{item.author}</Text>
                      <Text style={styles.textUserTitle}>Penulis</Text>
                    </Body>
                  </Left>
                </CardItem>
                  <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Artikel', {id : item.id,articleTitle:item.name })}>
                    <CardItem cardBody>
                      <ImageBackground source={{uri: 'https://temankanker.com'+item.photo}} style={{height: 170, width: null, flex: 1 }}>
                        <Badge style={{ backgroundColor: '#8c5ca0',position:'absolute',bottom:10,left:20 }}>
                          <Text style={{ color: '#fff',fontFamily:'Montserrat-Regular', fontSize:10, marginTop:-2}} >{item.type.toUpperCase()}</Text>
                        </Badge>
                      </ImageBackground>
                    </CardItem>
                    <CardItem>
                      <Body style={{marginTop: 0}}>
                        <Text style={styles.textTitle}>{item.name}</Text>
                      </Body>
                    </CardItem>
                  </TouchableOpacity>
                  <CardItem style={{marginTop: -25, backgroundColor: 'transparent'}}>
                    <Grid>
                      <Row>
                        <Col>
                          <Text style={styles.textArticleTime}>{moment(item.created_at).format('DD MMMM YYYY')}</Text>
                        </Col>
                        <Col style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                          <Button transparent onPress={() =>  this.share(item.name,item.content)} style={{paddingLeft: 0, paddingRight:0, marginLeft:15}}>
                            <Icon name="share-alt" style={{color: '#B2B2B2', fontSize: 16}}/>
                            <Text style={{marginLeft: 5, color: '#B2B2B2', fontFamily: 'Montserrat', fontSize: 14}}>{item.share_count}</Text>
                          </Button>
                          {this.state.userId ? this.likeButton(item.id, item.like_count, index) : (
                            <Button transparent style={{paddingLeft: 0, paddingRight:0, marginLeft:15}}>
                              <Icon name="heart" style={{color: '#B2B2B2', fontSize: 16}}/>
                              <Text style={{marginLeft: 5, color: '#B2B2B2', fontFamily: 'Montserrat', fontSize: 14}}>{item.like_count}</Text>
                            </Button>
                          )}
                        </Col>
                      </Row>
                    </Grid>
                  </CardItem>
              </Card>
            </View>
          )}
        />
      )
    }
  }
  goToSearch(){
    const { navigate } = this.props.navigation;
    navigate('SearchInfo');
  }

  render() {
    return (
      <Container>
          <Header noShadow hasTabs style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
            </Left>
            <Left style={styles.navBarWidth}>
            </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Informasi</Title>
            </Body>
            <Right style={styles.navBarWidth}>
              <Button transparent onPress={() => this.goToSearch()}>
                <Icon style={styles.headerIcon} name='search'/>
              </Button>
            </Right>
            <Right style={styles.navBarWidth}>
              <Button transparent 
                onPress={() =>
                ActionSheet.show(
                  {
                    options: FILTER,
                    cancelButtonIndex: CANCEL_INDEX,
                    title: "Filter berdasarkan"
                  },
                  buttonIndex => {
                    if(FILTER[buttonIndex] == "Semua" || FILTER[buttonIndex] == "Cancel"){
                      AsyncStorage.getItem("newsData", (err, result) => {
                      this.setState({
                        news: JSON.parse(result)
                      })
                    })
                    }
                    else{
                      AsyncStorage.getItem("newsData", (err, result) => {
                        this.setState({
                          news: JSON.parse(result)
                        })
                        let filteredNews = this.state.news.filter((newz) => {
                          return newz.type == FILTER[buttonIndex]
                        })
                        this.setState({
                          news: filteredNews
                        })
                      })
                    }
                  }
                )}
              >
                <Icon style={styles.headerIcon} name='filter'/>
              </Button>
            </Right>
          </Header>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
              {this.renderArtikel()}  
          </Image>
      </Container>
    );
  }
}