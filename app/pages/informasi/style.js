const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
  navBarWidth:{
    maxWidth: 40,
    backgroundColor: 'transparent'
  },
	headerIcon: {
		color: '#fff',
		width: 32,
		fontSize:24
	},
	navBarColor:{
        color: '#fff'
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#fff',
        fontSize: 24,
        paddingLeft: (Platform.OS === 'android') ? 0 : 0
    },
    headerTabStyle:{
    	backgroundColor:'#499FFD',
    	color:'#B2B2B2'
    },
    navBarStyle:{
        backgroundColor:'#499FFD',
        height: 70,
    },
    navbarIcon:{
    	fontSize:24,
    	color:'#fff',
    	paddingLeft:10
    },
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'BalooThambi-Regular',
  		color: '#489DFF',
  		fontSize: 14,
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 14
  	},
  	textArticleTime: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 14,
  		paddingTop:12
  	},
  	textTitle: {
  		fontFamily: 'BalooThambi-Regular',
  		color: '#489DFF',
  		fontSize: 22,
  		marginTop: 5,
  		lineHeight: 30
  	},
  	// Footer Style
  	navBarFooterStyle:{
		backgroundColor:'#ffffff',
		borderBottomWidth: 0.5,
		borderBottomColor:'#B2B2B2',
		height: 60
	},
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonNavbarText:{
		flexDirection: 'column',
	},
	footerIconInactive:{
		color: '#B2B2B2',
    	fontSize: 22,
	},
	footerIconActive:{
		color: '#3B5998',
    	fontSize: 22,
	},
	footerTextInactive:{
		color: '#B2B2B2',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	footerTextActive:{
		color: '#3B5998',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	thumbnailSize:{
		height:40,
		width:40,
		borderRadius:20,
	},
	thumbnailMargin:{
		marginTop: 5,
		marginBottom: 5,
	},
	cardTitle:{
		color: '#499FFD',
		fontSize: 22,
		fontFamily: 'BalooThambi-Regular',
		textAlign:'center',
		flex:1
	},
  buttonLightBlue:{
    height:50,
    marginBottom:10,
    marginTop:10,
    shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
    backgroundColor:'#499FFD',
	},
	buttonPurple: {
		backgroundColor: '#8c5ca0',
		height:50,
    marginBottom:10,
    marginTop:10,
    shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
	},
  balooThambi:{
    fontFamily: 'BalooThambi-Regular',
    fontSize:16,
      marginTop: (Platform.OS === 'android') ? -8 : 4,
  },
  input:{
    backgroundColor:'#fffae6',
    marginTop:10,
    marginBottom:5,
    paddingLeft:20,
    borderColor:'#499FFD',
    borderWidth:1,
    borderStyle:'dashed'
  },
  inputIcon: {
    color: '#489FC5',
    width: 32,
    fontSize:20
  },
  placeholderStyles: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18
  },
}