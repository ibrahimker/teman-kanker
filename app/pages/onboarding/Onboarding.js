import React, { Component } from 'react';
import { StyleSheet,StatusBar,AsyncStorage } from 'react-native';
import {Text,View,Button} from 'native-base'
import AppIntro from 'react-native-app-intro';

import images from '../../config/images.js'

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
});

export default class OnboardingScreen extends Component {
  static navigationOptions = {
    title: 'Onboarding',
    header:null
  };

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    'username':''
  }

  constructor() {
    super();
    this.checkAuthentication();
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });
    });
  }

  checkAuthentication = () => {
    AsyncStorage.getItem('isLoggedIn', (err, result) => {
      this.setState({ 'isLoggedIn': result });
    });
  }


  render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');
  	const pageArray = [{
      title: 'Belajar',
      description: 'Pelajari tentang kanker. Penyebab, gejala, cara mengatasi, dan bagaimana cara mencegahnya.',
      img: images.onboardingBelajar,
      imgStyle: {
        height: 220 * 1,
        width: 243 * 1,
      },
      backgroundColor: '#489FFD',
      fontColor: '#fff',
      level: 10,
    }, {
      title: 'Bertindak',
      description: 'Aktif terlibat dalam berbagi pengetahuan dan memecahkan masalah yang berkaitan dengan kanker.',
      img: images.onboardingBeraksi,
      imgStyle: {
        height: 220 * 1,
        width: 239 * 1,
      },
      backgroundColor: '#489FFD',
      fontColor: '#fff',
      level: 10,
    }
    ];
    return (
    	<AppIntro
        onNextBtnClick={this.nextBtnHandle}
        onDoneBtnClick={this.doneBtnHandle}
        onSkipBtnClick={this.onSkipBtnHandle}
        onSlideChange={this.onSlideChangeHandle}
        pageArray={pageArray}
      />
    );
  }

  onSkipBtnHandle = (index) => {
    if(this.state.isLoggedIn == "false"){
      this.props.navigation.navigate('LoginNoClose');
    }
    else{
      this.props.navigation.navigate('Skeleton');
    }
    
  }
  doneBtnHandle = () => {
    if(this.state.isLoggedIn == "false"){
      this.props.navigation.navigate('LoginNoClose');
    }
    else{
      this.props.navigation.navigate('Skeleton');
    }
  }
  nextBtnHandle = (index) => {
    console.log(index);
  }
  onSlideChangeHandle = (index, total) => {
    console.log(index, total);
  }
}