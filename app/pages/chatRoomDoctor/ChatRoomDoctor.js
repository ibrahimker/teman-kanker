import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Spinner,Item, Input} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { GiftedChat, Bubble } from 'react-native-gifted-chat';
import io from 'socket.io-client';

import { StyleSheet, StatusBar,AsyncStorage, TouchableOpacity, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class ChatRoomDoctorScreen extends Component {
  static navigationOptions = {
    title: 'Chat Room',
    header:null
  };

  state = {
    messages: [],
    sender:"",
    roomId:'',
    receiver:"",
    userId:'',
    dataLoaded:false,
    idLoaded:false
  };

  constructor(props) {
    super(props);
    this.socket = io('https://temankanker.com', { transports: ['websocket'] });
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ userId: result, idLoaded: true  });
      this.getRoomId(this.props.navigation.state.params.userId);
    });
  }

  componentDidMount() {
    
  }

  componentWillMount() {
  }

  getRoomId(userId){
    fetch('https://temankanker.com/api/UserKankers/getDoctorRoom?userId='+userId)
    .then((response) => response.json())
    .then((data) => {
      this.onReceivedMessage = this.onReceivedMessage.bind(this);
      this.socket.emit('join', data);
      this.socket.on('message', this.onReceivedMessage);
      this.setState({ roomId: data});
      return this.retrieveChat(data);
    })
    .done();
  }

  retrieveChat(roomId) {
    fetch('https://temankanker.com/api/Messages?filter=%7B%22where%22%3A%7B%22roomId%22%3A%22'+roomId+'%22%7D%2C%22order%22%3A%22created_at%20ASC%22%7D')
    .then((response) => response.json())
    .then((data) => {
      for (var i = 0; i < data.length; i++) {
        this.onReceive(data[i].content,data[i].createdBy,data[i].created_at)
      }
      return this.setState({ chatist: data, dataLoaded: true });
    })
    .done();
  }

  onReceive(text,senderId,createdAt) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(createdAt),
          user: {
            _id: senderId
          },
        }),
      };
    });
  }

  onReceivedMessage(messages){
    this.onReceive(messages.content,messages.createdBy,messages.created_at)
  }

  storeMessages(messages = []){
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  }

  sendToDB(message){
    fetch('https://temankanker.com/api/Messages/sendmessage', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "content":message,
        "roomId":this.state.roomId,
        "createdBy":this.state.userId
      })
    })
    .then((response) => { 

    })
  }

  onSend() {
    /*this.storeMessages(messages);*/
    this._textInput.setNativeProps({text: ''});
    this.sendToDB(this.state.message);
  }

  renderChatInput() {
    return (
      <View style={{ height: 50, width: null, backgroundColor:'white', padding: 7, overflow: "visible"}}>
        <Item rounded style={{flex: 1, width: "80%", backgroundColor: '#efefef', borderColor: 'white', padding: 5 }}>
          <Input ref={component => this._textInput = component} style={{fontSize: 14}} placeholder={'Tulis pesan kamu disini'} placeholderTextColor={'#7d6a97'} onChangeText={(text) => this.setState({ message: text })}/>
        </Item>
        <TouchableOpacity onPress={() => this.onSend()} style={{position: 'absolute', bottom: 0, right: 7, flex: 1, justifyContent: 'center', alignItems: 'center', height: 70, width: 55, backgroundColor: '#855fa8', borderTopLeftRadius: 30, borderTopRightRadius: 30}}>
          <Text style={[styles.textStyle, {fontSize: 15, marginTop: 10}]}>Kirim</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderChatBubble(props) {
    return ( <Bubble {...props} 
      wrapperStyle={{
          left: {
            backgroundColor: 'white',
            padding: 5,
            marginLeft: -45,
            marginBottom: 5
          },
          right: {
            backgroundColor: '#944ea6',
            padding: 5,
            marginBottom: 5
          }
        }} />
    )
  }

  renderChat(){
    if(this.state.dataLoaded && this.state.idLoaded){
      return(
        <GiftedChat
          messages={this.state.messages}
          loadEarlier={true}
          minInputToolbarHeight={50}
          renderBubble={this.renderChatBubble}
          renderInputToolbar={this.renderChatInput.bind(this)}
          user={{
            _id: parseInt(this.state.userId)
          }}
        />
      );
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }
  render() {
    StatusBar.setBarStyle('dark-content', true);
    return (
        <Container>
          <Image source={images.backgroundHome} style={{ flex: 1, width: null, height: null }}>
            <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                  <Icon style={styles.navBarColor} name='arrow-left'/>
                </Button>
              </Left>
              <Body style={styles.navBarCentered}>
                <Title style={styles.navBarTitle}>Teman Kanker</Title>
              </Body>
              <Right style={styles.navBarWidth}/>
            </Header>
            {this.renderChat()}
          </Image>
        </Container>
      );
    }
  }
