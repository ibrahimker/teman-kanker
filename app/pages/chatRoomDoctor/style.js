const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#fff',
		fontSize:24
	},
	navBarTitle:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#fff',
		fontSize: 24
	},
	navBarStyle:{
		backgroundColor:'#489FFD',
		height: 70,
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	textStyle: {
		fontFamily:'BalooThambi-Regular', fontSize:24 , color: 'white', backgroundColor: 'transparent', textAlign: 'center'
	},
	textStyle2: {
		fontFamily:'Montserrat', fontSize:17 , color: 'white', backgroundColor: 'transparent', textAlign: 'center'
	},
	text: {
      fontWeight: (Platform.OS === 'ios') ? '400' : '400',
      fontSize: 14,
      fontFamily: 'Montserrat',
      color: '#9A978F',
    },
    textAnswer: {
      fontWeight: (Platform.OS === 'ios') ? '400' : '400',
      fontSize: 18,
      fontFamily: 'BalooThambi-Regular',
			color: '#489DFF',
			textAlign: 'right'
    },

}