import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Left,Right,Body, Title, Form, Item, Input, Toast} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class ForgotPasswordScreen extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        username: '',
        resetting: false
    }

    constructor(props) {
        super(props)
    }

    renderLoading() {
        if(this.state.resetting) {
            return (
                <View style={styles.loadingOverlay}>
                    <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Memproses reset password...</Text>
                    <Spinner color="#489FFD"/>
                </View>
            );
        }
    }

    resetPassword() {

        let username = this.state.username;

        this.setState({ resetting: true });

        fetch('https://temankanker.com/api/UserKankers/resetPassword', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "email" : username
            })
        })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data)
            this.setState({ resetting: false })
            this.props.navigation.goBack()
            Toast.show({
                text: 'Password baru telah dikirim ke email anda',
                position: 'bottom',
                type:'success',
                duration: 2000
            })
        })
        .catch(error => {
            this.setState({ resetting: false })
            console.log(error)
            Toast.show({
                text: 'Terjadi kesalahan pada sistem kami',
                position: 'bottom',
                type:'danger',
                duration: 2000
            })
        })
    }

    render() {
        StatusBar.setBarStyle('light-content', true);
        return (
            <Container>
                <Header noShadow style={styles.navBarStyle}>
                    <Left style={styles.navBarWidth}>
                        <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                            <Icon style={styles.navBarColor} name='arrow-left'/>
                        </Button>
                    </Left>
                    <Body style={styles.navBarCentered}>
                        <Title style={styles.navBarTitle}>Lupa Password</Title>
                    </Body>
                        <Right style={styles.navBarWidth}>
                    </Right>
                </Header>
                <Image source={images.backgroundHome} style={styles.backgroundImage}>
                    <Content>
                            <View style={styles.form}>
                                <Text style={{textAlign: 'center', fontFamily: 'Montserrat', fontSize: 14, marginBottom: 20, lineHeight: 20}}>Masukkan email anda. {"\n"}Kata sandi baru akan dikirim ke alamat email yang anda masukkan</Text>
                                <Form>
                                    <Item rounded style={styles.input}>
                                        <Icon style={styles.inputIcon} active name='envelope' />
                                        <Input style={styles.placeholderStyles} keyboardType={'email-address'} placeholder='Email' placeholderTextColor='#93938E' onChangeText={(email) => this.setState({username: email})}/>
                                    </Item>
                                </Form>
                                {this.state.username !== '' ? (
                                    <Button rounded onPress={() => this.resetPassword()} style={[styles.buttonLightBlue, {marginTop: 20}]} block>
                                        <Text style={styles.balooThambi}>Ubah</Text>
                                    </Button>
                                ) : (
                                    <Button rounded style={[styles.buttonLightBlue, {marginTop: 20, backgroundColor: 'rgba(73, 159, 253, 0.5)'}]} block>
                                        <Text style={styles.balooThambi}>Ubah</Text>
                                    </Button>
                                )}
                            </View>
                    </Content>
                </Image>
                {this.renderLoading()}
            </Container>
        )
    }
}
