import React, { Component } from 'react';
import {Text,View,Drawer,Fab,List,ListItem,Button,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail,Spinner,Toast} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image,AsyncStorage } from 'react-native';
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js'

import { Col, Row, Grid } from 'react-native-easy-grid';

import MapView from 'react-native-maps';

import images from '../../config/images.js';
import styles from './style.js';

export default class PetaScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    'headerTitle':'Beranda',
    'screen':'0',
    latlng: null,
    dataLoaded:false
  }

  constructor(props) {
    super(props);

    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let address = params.data;

    fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=AIzaSyAd80NDFYvTTlNLXuR3CDZGpWACWKPQTPQ')
    .then((response) => response.json())
    .then((responseJson) => {
        console.log(responseJson.results[0].geometry.location);
        this.setState({ latlng: responseJson.results[0].geometry.location, dataLoaded: true })
    })
    .catch(error => {

      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Alamat tidak dapat ditemukan',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');

    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
        return (
            <Container>
                <Header noShadow style={styles.navBarStyle}>
                    <Left style={styles.navBarWidth}>
                        <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                            <Icon style={styles.navBarColor} name='arrow-left'/>
                        </Button>
                    </Left>
                    <Body style={styles.navBarCentered}>
                        <Title style={styles.navBarTitle}>Peta</Title>
                    </Body>
                    <Right style={styles.navBarWidth}/>
                    </Header>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                        <MapView
                            style={{ left: 0, right: 0, top: 0, bottom: 0, position: 'absolute' }}
                            zoomEnabled={true}
                            loadingEnabled={true}
                            initialRegion={{
                                latitude: this.state.latlng.lat,
                                longitude: this.state.latlng.lng,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                        >
                            <MapView.Marker
                                coordinate={{
                                    latitude: this.state.latlng.lat,
                                    longitude: this.state.latlng.lng,
                                }}
                            />
                        </MapView>
                    </View>
            </Container>
            );
    }
  }
}