const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#fff',
		width: 32,
		fontSize:24
	},
	navBarTitle:{
		fontFamily: 'BalooThambi-Regular',
		fontWeight: '400',
		color: '#fff',
		fontSize: 24
	},
	navBarStyle:{
		backgroundColor:'#489FFD',
		height: 70,
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	}
}