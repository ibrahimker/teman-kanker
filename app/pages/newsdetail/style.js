const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#ffffff'
	},
	navBarTitle:{
		fontFamily: 'Kreon-Regular',
		fontWeight: '400',
		color: '#3B5998',
		fontSize: 24
	},
	navBarStyle:{
		backgroundColor: 'transparent',
		height: 70,
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
	textUserDate: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12,
  		margin: 30,
  		marginBottom: 0,
  		backgroundColor: 'transparent'
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12,
  		backgroundColor: 'transparent'
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		margin: 30,
  		marginTop: 5,
  		marginBottom: -3,
  		backgroundColor: 'transparent',
  		lineHeight: 30
  	},
  	contentStyle2:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 30,
  		marginBottom: 20,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 24,
  		color: '#868686'
  	},
  	contentStyle3:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 20,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 24,
  		color: '#868686'
  	},
  	contentStyle3End:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 50,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 24,
  		color: '#868686',
  	}

}