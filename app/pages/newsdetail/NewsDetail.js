import React, { Component } from 'react';
import {Text,View,Drawer,Button,Icon,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail} from 'native-base';


// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class NewsDetailScreen extends Component {
  static navigationOptions = {
    title: 'News Detail',
    header:null
  };

  state = {
    messages: [],
    id: '',
    title_news: '',
    date_news: '',
    share_news: '',
    like_news: '',
    data: '',
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    let title_news = params.title_news;
    fetch('https://temankanker.com/api/news/' + id)
    .then((response) => response.json())
    .then((responseJson) => {
         console.log(responseJson);
         this.setState({
            data: responseJson
         })
      })
    .done();
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    StatusBar.setBarStyle('light-content', true);
    openDrawer = () => {
      this.drawer._root.open()
    };
    closeDrawer = () =>{
    	this.drawer._root.close()
    };
    pressDrawer = () =>{

    }
    return (

        <Container>
            <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='ios-arrow-back'/>
                </Button>
              </Left>
              <Body/>
              <Right>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='md-heart-outline'/>
                </Button>
              </Right>
            </Header>

            <Content style={{backgroundColor: '#ffffff', marginTop: -90, zIndex: -10}}>
              <Image source={{uri: this.state.data.pict_news}} style={{height: 200, width: null, flex: 1}}/>
              <Image source={images.backgroundHome} style={styles.backgroundImage}>
                <Text style={styles.textUserDate}>{this.state.data.date_news}</Text>
                <Text style={styles.textTitle}>{this.state.data.title_news}</Text>
                <View style={{flexDirection: 'row', marginBottom: 20, marginLeft: 10}}>
                  <Button transparent>
                    <Icon name="md-share" style={{color: '#B2B2B2', fontSize: 22}}/>
                    <Text style={{color: '#B2B2B2', fontFamily: 'Montserrat-Regular', fontSize: 12, marginLeft: 3}}>{this.state.data.share_news}</Text>
                  </Button>
                  <Button transparent>
                    <Icon name="md-heart-outline" style={{color: '#B2B2B2', marginLeft: -15, fontSize: 22}}/>
                    <Text style={{color: '#B2B2B2', fontFamily: 'Montserrat-Regular', fontSize: 12, marginLeft: 3}}>{this.state.data.like_news}</Text>
                  </Button>
                  <Button transparent>
                    <Icon name="md-eye" style={{color: '#B2B2B2', marginLeft: -15, fontSize: 27}}/>
                    <Text style={{color: '#B2B2B2', fontFamily: 'Montserrat-Regular', fontSize: 12, marginLeft: 3}}>{this.state.data.view_news}</Text>
                  </Button>
                </View>

                <View style={{borderBottomWidth: 0.5, borderBottomColor: '#B2B2B2'}}/>
                
                <Text style={styles.contentStyle2}>{this.state.data.description_news}</Text>
              

                <View style={{borderBottomWidth: 0.5, borderBottomColor: '#B2B2B2'}}/>

                <CardItem style={{backgroundColor: 'transparent'}}>
                  <Left>
                    <Thumbnail source={{uri: 'https://maxcdn.icons8.com/Share/icon/Users//user_male_circle_filled1600.png'}} />
                    <Body style={{marginLeft: 15}}>
                      <Text style={styles.textUsername}>{this.state.data.author_news}</Text>
                      <Text style={styles.textUserTitle}>Penulis</Text>
                    </Body>
                  </Left>
                </CardItem>
                
                <View style={{borderBottomWidth: 0.5, borderBottomColor: '#B2B2B2'}}/>

                {/*<Button rounded style={{margin: 20, alignSelf: 'center', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3, shadowRadius: 2, backgroundColor:'#fff', borderColor: '#e4e4e4'}}>
                                  <Icon name="md-share" style={{color: '#979797', fontSize: 18}}/>
                                  <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 14, color: '#4c4c4c', marginLeft: 5}}>Share</Text>
                                </Button>
                                
                                <View style={{borderBottomWidth: 0.5, borderBottomColor: '#B2B2B2'}}/>*/}


              </Image>
            </Content>
        </Container>
      );
    }
  }
