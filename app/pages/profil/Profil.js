import React, { Component } from 'react';
import {Text,View,Drawer,Fab,List,ListItem,Button,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail,Spinner,Toast} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, AsyncStorage, Image , TouchableHighlight, DeviceEventEmitter} from 'react-native';
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js'
import { Col, Row, Grid } from "react-native-easy-grid";

import images from '../../config/images.js';
import styles from './style.js';

// import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';

var ImagePicker = require('react-native-image-picker');
// More info on all the options is below in the README...just some common use cases shown here


export default class ProfilScreen extends Component {
  static navigationOptions = {
    title: 'Profile',
    header:null
  };

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    userProfile:[],
    userProfPict: [],
    dataLoaded:false,
    pictLoaded: false
  }

  constructor() {
    super();
    this.checkAuthentication();
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });
      if(this.state.userId == '') {
        this.props.navigation.navigate('Login');
      }
      else{
        this.retrieveProfile();
      }
    });
  }

  retrieveProfile() {
    fetch('https://temankanker.com/api/UserKankers/'+this.state.userId)
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      return this.setState({ userProfile: data, dataLoaded: true , userProfPict: {uri: "https://temankanker.com" + data.photo + '?rand=' + Math.random()}});
      console.log(data.photo);
    })
    .catch(error => {

      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Gagal mendapatkan data profil',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 2000
      });
    })
    .done();
  }

  checkAuthentication = () => {
    AsyncStorage.getItem('isLoggedIn', (err, result) => {
      this.setState({ 'isLoggedIn': result });
      if(this.state.isLoggedIn == 'false') {
        this.props.navigation.navigate('Login');
      }
    });
  }

  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
    else
      byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  }

  changeProfPict(event){
    console.log("hi");
    ImagePicker.showImagePicker({maxHeight: 300, maxWidth: 300} ,(response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          userProfPict: source,
          dataLoaded: false
        });

        let formdata = new FormData();

        formdata.append("image", {uri: response.uri, name: this.state.userProfile.id + '.jpg', type: 'multipart/form-data'})

        console.log(formdata);

        fetch('https://temankanker.com/api/Storages/profpict/upload',{
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          body: formdata
        })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          fetch('https://temankanker.com/api/UserKankers/' + this.state.userProfile.id,{
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              "photo": '/api/Storages/profpict/download/' + this.state.userProfile.id + '.jpg'
            })
          })
          .then((response) => response.json())
          .then((data) => {
            console.log(data);
            this.setState({
              dataLoaded: true
            })
          })
        })

      }
    });
  }

  renderProfil(){
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      if(this.state.isLoggedIn == 'true'){
        return (
          <Content style={{marginTop: -75, zIndex: 0}}>
                <View>
                  <View style={{backgroundColor: '#489FFD', height: 320, width: null, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableHighlight rounded onPress={() => this.changeProfPict()} style={{height: 130, width: 130, marginTop:60, borderRadius: 65}} >
                      <View>
                        <Image style={{height: 130, width: 130, borderRadius: 65}} onLoad={() => this.setState({pictLoaded: true})} source={this.state.pictLoaded ? this.state.userProfPict : images.profilePictDefault} defaultSource={require('../../images/defaultprofpic-empty.png')} indicator={Progress.Circle}/>
                        <View style={{backgroundColor: 'orange', width: 25, height: 25, position: 'absolute', justifyContent: 'center', alignItems: 'center', borderRadius: 50, bottom: 10, right: 10}}>
                          <Icon color="white" name="camera"/>
                        </View>
                      </View>
                    </TouchableHighlight>
                    <Text style={{fontFamily:'BalooThambi-Regular', fontSize:24 , color: 'white', backgroundColor: 'transparent', textAlign: 'center', marginTop:10}}>{this.state.userProfile.name}</Text>
                    <Text style={{fontFamily:'Montserrat-Regular',fontSize:16, color: '#fff', backgroundColor: 'transparent', textAlign: 'center'}}>{this.state.userProfile.role}</Text>
                  </View>
                </View>                
                <View>
                  <ListItem style={styles.borderSideBar}>
                    <Left>
                      <Text style={styles.text}>Jenis Kelamin</Text>
                    </Left>
                    <Body>
                      <Text style={styles.textAnswer}>{this.state.userProfile.sex?'Pria':'Wanita'}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                </View>
                <View>
                  <ListItem style={styles.borderSideBar}>
                    <Left>
                      <Text style={styles.text}>Kota</Text>
                    </Left>
                    <Body>
                      <Text style={styles.textAnswer}>{this.state.userProfile.city}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                </View>
                <View>
                  <ListItem style={styles.borderSideBar}>
                    <Left>
                      <Text style={styles.text}>Tanggal Lahir</Text>
                    </Left>
                    <Body>
                      <Text style={styles.textAnswer}>{moment(this.state.userProfile.birthdate).format('DD MMMM YYYY')}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                </View>
                <View>
                  <ListItem style={styles.borderSideBar}>
                    <Left>
                      <Text style={styles.text}>Grup</Text>
                    </Left>
                    <Body>
                      <Text style={styles.textAnswer}>{this.state.userProfile.group}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                </View>
                {this.state.userProfile.role !== 'Lainnya' && (
                <View>
                  <View>
                    <ListItem style={styles.borderSideBar}>
                      <Left>
                        <Text style={styles.text}>Tipe</Text>
                      </Left>
                      <Body>
                        <Text style={styles.textAnswer}>{this.state.userProfile.type}</Text>
                      </Body>
                    </ListItem>
                    <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                  </View>
                  <View>
                    <ListItem style={styles.borderSideBar}>
                      <Left>
                        <Text style={styles.text}>Durasi</Text>
                      </Left>
                      <Body>
                        {this.state.userProfile.role === 'Survivor' ? (
                          <Text style={styles.textAnswer}>{this.state.userProfile.duration_start} - {this.state.userProfile.duration_end}</Text>
                        ) : (
                          <Text style={styles.textAnswer}>{this.state.userProfile.duration_start} - Sekarang</Text>
                        )}
                      </Body>
                    </ListItem>
                    <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                  </View>
                  {this.state.userProfile.role === 'Pasien' && (
                      <View>
                          <ListItem style={styles.borderSideBar}>
                            <Left>
                              <Text style={styles.text}>Rumah Sakit</Text>
                            </Left>
                            <Body>
                              <Text style={styles.textAnswer}>{this.state.userProfile.hospital}</Text>
                            </Body>
                          </ListItem>
                          <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                      </View>
                      )}
                      <View>
                      <ListItem style={styles.borderSideBar}>
                        <Left>
                          <Text style={styles.text}>Dokter</Text>
                        </Left>
                        <Body>
                          <Text style={styles.textAnswer}>{this.state.userProfile.doctor}</Text>
                        </Body>
                      </ListItem>
                      <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                      </View>
                    </View>
                  )}
              </Content>                    
        );
      }
      else{
        
      }
    }
  }

  goToEditProfile() {
    this.props.navigation.navigate('EditProfil', {userProfile: this.state.userProfile, refresh: () => this.retrieveProfile()});
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    return (
        <Container>
            <Header noShadow style={styles.navBarStyle}>
              <Left>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                  <Icon style={styles.navBarColor} name='arrow-left'/>
                </Button>
              </Left>
              <Body>
                <Title style={styles.navBarTitle}>Profil</Title>
              </Body>
              <Right>
                <Button transparent onPress={() => this.goToEditProfile()}>
                  <Icon style={styles.navBarColor} name='pencil'/>
                </Button>
              </Right>
            </Header>
            <Image source={images.backgroundHome} style={styles.backgroundImage}>
                {this.renderProfil()}
            </Image>
        </Container>
      );
    }
  }
