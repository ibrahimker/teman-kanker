import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Tabs,Tab,Badge,H1,Form,Item,Input,Picker,ActionSheet,List,ListItem,Spinner} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Modal, KeyboardAvoidingView, TouchableHighlight, Image, AsyncStorage, DeviceEventEmitter } from 'react-native';
import { Col, Row, Grid } from "react-native-easy-grid";
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js'
import DatePicker from 'react-native-datepicker'

import images from '../../config/images.js';
import styles from './style.js';

import * as Progress from 'react-native-progress';

var ImagePicker = require('react-native-image-picker');

export default class EditProfilScreen extends Component {
  static navigationOptions = {
    title: 'Edit Profile',
    header:null
  };

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    userProfile:[],
    userProfPict: [],
    dataLoaded:false,
    pictLoaded: false
  }

  constructor(props) {
    super(props);
    this.state =  {
      fullname : '',
      email: '',
      birthday: '',
      password: '',
      location: '',
      hospital: '',
      sex: '',
      group:'',
      doctor: '',
      cancertype: '',
      kota: [],
      filteredKota: [],
      yearsList: [],
      dataKanker: [],
      kotaField: '',
      showKota: false,
      showGender: false,
      illnessEndDate: '',
      illnessStartDate: '',
      showTypeKanker: false,
      kotaLoaded: false,
      updating: false,
      emailAvailable: null
    }

    console.log(this.props.navigation.state.params.userProfile);

    fetch('https://temankanker.com/api/MasterData?filter={"where":{"param":"Kanker"}}')
    .then((response) => {
      return response.json();
    })
    .then(data => {
      this.setState({ dataKanker: data, dataLoaded: true });
    });

    fetch('https://raw.githubusercontent.com/nolimitid/nama-tempat-indonesia/master/json/kota-kabupaten.json')
    .then((response) => {
      console.log(response);
      return response.json();
    })
    .then((data) => {
      this.setState({ kota: data, filteredKota: data, kotaLoaded: true });
    })
  }

  updateProfile() {
    let name = this.state.fullname ? this.state.fullname : this.state.userProfile.name;
    let sex = this.state.sex === 'Pria' ? true : false;
    let city  = this.state.location;
    let group = this.state.group;
    let birthdate = moment(this.state.date).utc(7).toJSON();
    let type = this.state.cancertype ? this.state.cancertype : '';
    let start = this.state.illnessStartDate ? this.state.illnessStartDate : '';
    let end = this.state.illnessEndDate ? this.state.illnessEndDate : '';
    let hospital = this.state.hospital ? this.state.hospital : (this.state.userProfile.hospital ? this.state.userProfile.hospital : '');
    let doctor = this.state.doctor ? this.state.doctor : (this.state.userProfile.doctor ? this.state.userProfile.doctor : '');

    this.setState({updating: true});

        fetch('https://temankanker.com/api/UserKankers/' + this.state.userProfile.id, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "name" : name,
                "sex" : sex,
                "city" : city,
                "group":group,
                "birthdate" : birthdate,
                "type": type,
                "duration_start" : start,
                "duration_end" : end,
                "hospital" : hospital,
                "doctor" : doctor
            })
        })
        .then(response => {
            console.log(response); 
            const { navigation } = this.props;
            this.props.navigation.state.params.refresh();
            navigation.goBack();
        });
  }

  componentWillMount() {
    this.setState({ userProfile: this.props.navigation.state.params.userProfile, 
                    date: this.props.navigation.state.params.userProfile.birthdate,
                    sex: this.props.navigation.state.params.userProfile.sex ? 'Pria' : 'Wanita',
                    location: this.props.navigation.state.params.userProfile.city,
                    group: this.props.navigation.state.params.userProfile.group,
                    cancertype: this.props.navigation.state.params.userProfile.type,
                    illnessStartDate: this.props.navigation.state.params.userProfile.duration_start,
                    illnessEndDate: this.props.navigation.state.params.userProfile.duration_end,
                    hospital: this.props.navigation.state.params.userProfile.hospital,
                    doctor: this.props.navigation.state.params.userProfile.doctor
                  });

    let years = [];
    let currYear = new Date().getFullYear();
    let startYear = 1900;

    while( currYear >= startYear ) {
      years.push(currYear--);
    }

    this.setState({yearsList: years});
  }

  renderCancerType() {
      return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showTypeKanker}
            onRequestClose={() => {alert("Modal has been closed.")}}
            >
            <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, maxHeight: 300, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
            <View style={{padding: 10, paddingBottom: 0}}>
              <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih tipe kanker</Text>

              <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
                this.setState({showTypeKanker: false})
              }}>
                <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
              </Button>
              
              {this.state.dataLoaded ? (
                  
                      <List
                        style={{height: 200}}
                        dataArray={this.state.dataKanker}
                        renderRow={(item) =>
                          <ListItem key={item.id} style={{borderColor: 'transparent'}} onPress={() => this.pickKanker(item.value)}>
                            <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{item.value}</Text>
                          </ListItem>
                        }>
                      </List>
                  
              ) : (<Spinner color='#B2B2B2'/>)}
              

            </View>
            </View>
          </Modal>
      )
  }

  renderPickYear(type) {
      return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={type === 'start' ? this.state.showYearList : this.state.showYearList2}
          onRequestClose={() => {alert("Modal has been closed.")}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, maxHeight: 300, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10, paddingBottom: 0}}>
            
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>{type === 'start' ? 'Pilih tahun mulai sakit' : 'Pilih tahun sembuh sakit'}</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              type === 'start' ? this.setState({showYearList: false}) : this.setState({showYearList2: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View style={{height: 200}}>
                <List
                  dataArray={this.state.yearsList}
                  renderRow={(item) =>
                    <ListItem key={item} style={{borderColor: 'transparent'}} onPress={() => this.pickYear(item, type)}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{item}</Text>
                    </ListItem>
                  }>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  renderPickKota() {
    if(this.state.kotaLoaded){
      return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showKota}
            onRequestClose={() => {alert("Modal has been closed.")}}
            >
            <View style={{marginTop: 50, backgroundColor: '#489DFF', height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
            <View style={{padding: 10}}>
              <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih kota</Text>

              <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
                this.setState({showKota: false})
              }}>
                <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
              </Button>

              <Item rounded style={styles.input}>
                <Icon style={[styles.inputIcon, {color: 'white'}]} active name='search' />
                <Input value={this.state.kotaField} style={styles.placeholderStyles} type="text" placeholder='Cari kota' placeholderTextColor='#B22B2' onChangeText={(location) => {this.filterKota(location); this.setState({location: location})}}/>
              </Item>

              <View style={{marginBottom: 80}}>
                  <List
                    dataArray={this.state.filteredKota}
                    renderRow={(item) =>
                      <ListItem key={item} style={{borderColor: 'transparent'}} onPress={() => this.pickKota(item)}>
                        <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{item}</Text>
                      </ListItem>
                    }>
                  </List>
              </View>

            </View>
            </View>
          </Modal>
      )
    }
    else {
      return (<Spinner color='#B2B2B2'/>);
    }
  }

  renderPickGender() {
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showGender}
          onRequestClose={() => {alert("Modal has been closed.")}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10}}>
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih jenis kelamin</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              this.setState({showGender: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View>
                <List>
                    <ListItem key={'Pria'} style={{borderColor: 'transparent'}} onPress={() => this.setState({sex: 'Pria', showGender: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Pria</Text>
                    </ListItem>
                    <ListItem key={'Wanita'} style={{borderColor: 'transparent'}} onPress={() => this.setState({sex: 'Wanita', showGender: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>Wanita</Text>
                    </ListItem>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  filterKota(kota) {

    this.setState({ kotaField: kota });

    this.setState({ showKota: true });

    let filteredKota = this.state.kota.filter(value => {
      value = value.toLowerCase();
      kota = kota.toLowerCase();
      return value.indexOf(kota) > -1;
    });

    this.setState({ filteredKota: filteredKota });
  }

  pickKota(kota) {
    this.setState({kotaField: kota});
    this.setState({ showKota: false });
    this.setState({ location: kota });
  }

  pickKanker(kanker) {
    this.setState({cancertype: kanker, showTypeKanker: false});
  }

  pickYear(year, type) {
    type === 'start' ? this.setState({illnessStartDate: year, showYearList: false}) : this.setState({illnessEndDate: year, showYearList2: false})
  }

  checkEmail(e) {
    this.setState({checkEmail: true, emailAvailable: null});
    let email = e.nativeEvent.text.toLowerCase();

    fetch('https://temankanker.com/api/UserKankers/count?where={"username": "'+ email +'"}')
    .then(response => {
      return response.json();
    })
    .then(data => {
      if(this.state.userProfile.username === email) {
        this.setState({emailAvailable: true});
      }
      else if(data.count > 0) {
        this.setState({emailAvailable: false});
      }
      else {
        this.setState({emailAvailable: true});
      }

      this.setState({checkEmail: false});
    });
  }

  renderLoading() {
    if(this.state.updating) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25}}>Memperbaharui Profil Anda...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  renderPickGroup(role = null) {
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showGroup}
          onRequestClose={() => {this.setState({showGroup: false})}}
          >
          <View style={{backgroundColor: '#489DFF', position: 'absolute', width: null, right: 0, left: 0, bottom: 0, height: null, flex: 1, elevation: 30, borderTopLeftRadius: 20, borderTopRightRadius: 20}}>
          <View style={{padding: 10}}>
            <Text style={[styles.navBarTitle, {marginLeft: 10}]}>Pilih Group</Text>

            <Button transparent style={{position: 'absolute', top: 5, right: -10, zIndex: 99}} onPress={() => {
              this.setState({showGroup: false})
            }}>
              <Icon style={[styles.inputIcon, {color: 'white'}]} active name='close' />
            </Button>

            <View>
                <List>
                    <ListItem key={'Pria'} style={{borderColor: 'transparent'}} onPress={() => this.setState({group: role === 'Lainnya' ? 'Relawan' : 'Anak', showGroup: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{role === 'Lainnya' ? 'Relawan' : 'Anak'}</Text>
                    </ListItem>
                    <ListItem key={'Wanita'} style={{borderColor: 'transparent'}} onPress={() => this.setState({group: role === 'Lainnya' ? 'Simpatisan' : 'Dewasa', showGroup: false})}>
                      <Text style={{fontFamily: 'Montserrat-Regular', color: 'white'}}>{role === 'Lainnya' ? 'Simpatisan' : 'Dewasa'}</Text>
                    </ListItem>
                </List>
            </View>

          </View>
          </View>
        </Modal>
      )
  }

  renderAdditionalField() {
    return (
      <View>
        <Item rounded style={styles.input} onPress={() => this.setState({ showTypeKanker: true })} >
            <Icon style={styles.inputIcon} active name='heart' />
            <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.cancertype === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.cancertype === '' ? 'Tipe kanker pasien*' : this.state.cancertype}</Text>
            <Input disabled/>
        </Item>
        {this.state.showTypeKanker && (
                this.renderCancerType()
        )}
        <Item rounded style={styles.input} onPress={() => this.setState({ showYearList: true })}>
            <Icon style={styles.inputIcon} active name='calendar' />
            <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.illnessStartDate === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.illnessStartDate === '' ? 'Tahun Mulai Sakit' : this.state.illnessStartDate}</Text>
            <Input disabled/>
        </Item>
        {this.state.showYearList && (
                this.renderPickYear('start')
        )}
        {this.state.userProfile.role == 'Survivor' && (
            <View>
              <Item rounded style={styles.input} onPress={() => this.setState({ showYearList2: true })}>
                  <Icon style={styles.inputIcon} active name='calendar' />
                  <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.illnessEndDate === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.illnessEndDate === '' ? 'Tahun Sembuh Sakit' : this.state.illnessEndDate}</Text>
                  <Input disabled/>
              </Item>
              {this.state.showYearList2 && (
                      this.renderPickYear('end')
              )}
            </View>
        )}

        {this.state.userProfile.role == 'Pasien' && (
            <Item rounded style={styles.input}>
                <Icon style={styles.inputIcon} active name='home' />
                <Input style={styles.placeholderStyles} defaultValue={this.state.userProfile.hospital} placeholder='Rumah Sakit Pasien' placeholderTextColor='#B2B2B2' onChangeText={(hospital) => this.setState({hospital: hospital})}/>
            </Item>
        )}
        
        <Item rounded style={styles.input}>
            <Icon style={styles.inputIcon} active name='stethoscope' />
            <Input style={styles.placeholderStyles} defaultValue={this.state.userProfile.doctor} type="text" placeholder={this.state.userProfile.role === 'Pasien' ? 'Dokter Pasien' : 'Dokter'} placeholderTextColor='#B2B2B2' onChangeText={(doctor) => this.setState({doctor: doctor})}/>
        </Item>
      </View>
    )
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    return (
        <Image source={images.backgroundHome} style={{flex: 1, width: null}}>
            <Container>
                <Header noShadow style={[styles.navBarStyle, {borderColor: 'transparent'}]}>
                    <Left>
                        <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                        <Icon style={styles.navBarColor} name='close'/>
                        </Button>
                    </Left>
                    <Body>
                        <Title style={styles.navBarTitle}>Profil</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.updateProfile()}>
                        <Icon style={styles.navBarColor} name='check'/>
                        </Button>
                    </Right>
                </Header>
                <Content>
                    <View style={{ padding: 15 }}>
                            <Item rounded style={styles.input}>
                                <Icon style={styles.inputIcon} active name='user' />
                                <Input style={styles.placeholderStyles} type="text" defaultValue={this.state.userProfile.name} placeholder='Nama Lengkap*' placeholderTextColor='#B2B2B2' onChangeText={(fullname) => this.setState({fullname: fullname})}/>
                            </Item>
                            <Item rounded style={styles.input} onPress={() => this.setState({ showGender: true })}>
                                <Icon style={styles.inputIcon} active name='intersex' />
                                <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.sex === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.sex === '' ? 'Pilih jenis kelamin*' : this.state.sex}</Text>
                                <Input disabled/>
                            </Item>
                            {this.state.showGender && (
                                    this.renderPickGender()
                            )}
                            <Item rounded style={styles.input} onPress={() => this.setState({ showKota: true })}>
                                <Icon style={styles.inputIcon} active name='map-marker' />
                                <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, color: 'grey', fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.location === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.location === '' ? 'Pilih kota*' : this.state.location}</Text>
                                <Input disabled/>
                            </Item>
                            {this.state.showKota && (
                                    this.renderPickKota()
                            )}
                            <Item rounded style={styles.input} onPress={() => this.setState({ showGroup: true })}>
                                <Icon style={styles.inputIcon} active name='intersex' />
                                <Text style={[{paddingBottom: 12, paddingTop: 12, paddingLeft: 10, fontFamily: 'Montserrat-Regular', fontSize: 18}, this.state.group === '' ? {color: '#B2B2B2'} : {color: 'black'}]}>{this.state.group === '' ? 'Rentang Usia Pasien*' : this.state.group}</Text>
                            </Item>
                            {this.state.showGroup && (
                                    this.renderPickGroup(this.state.userProfile.role)
                            )}
                            <Item rounded style={styles.input}>
                                <Icon style={styles.inputIcon} active name='calendar' />
                                <Input disabled/>
                                <DatePicker
                                style={{width: 200, backgroundColor: 'transparent', position: 'absolute'}}
                                date={this.state.date}
                                mode="date"
                                format="YYYY-MM-DD"
                                minDate="1900-01-01"
                                iconComponent={(<Icon style={styles.inputIcon2} active name='calendar' />)}
                                maxDate={moment().format('YYYY-MM-DD')}
                                placeholder="Tanggal Lahir*"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {
                                    backgroundColor: 'transparent',
                                    borderWidth: 0,
                                    marginLeft: 20
                                    },
                                    placeholderText: {
                                    fontFamily: 'Montserrat-Regular',
                                    fontSize: 18,
                                    color: '#B2B2B2',
                                    marginLeft: 20
                                    },
                                    dateText: {
                                    fontFamily: 'Montserrat-Regular',
                                    fontSize: 18,
                                    color: 'black',
                                    marginLeft: 15
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({date: date, birthday: date})}}
                                />
                            </Item>

                            {this.state.userProfile.role !== 'Lainnya' && this.renderAdditionalField()}
                            
                           
                            <Text onPress={() => this.props.navigation.navigate('ChangePassword')} style={[styles.text, {textAlign: 'center', marginTop: 20, marginLeft: -5}]}>Saya ingin ubah <Text style={[styles.text, {color: '#489FFD'}]}>Kata Sandi</Text></Text>
                            
                    </View>
                </Content>
                {(this.state.showGroup || this.state.showKota || this.state.showGender || this.state.showYearList || this.state.showTypeKanker || this.state.showYearList2) && (
                    <View style={[styles.loadingOverlay, {backgroundColor: 'black', opacity: 0.4}]}></View>
                )}

                {this.renderLoading()}
            </Container>
        </Image>
      );
    }
  }
