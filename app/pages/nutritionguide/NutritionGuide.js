import React, { Component } from 'react';
import {Text,View,Drawer,Button,Icon,Container,Content,Header,Left,Body,Title} from 'native-base';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

export default class NutritionGuideScreen extends Component {
  static navigationOptions = {
    title: 'Nutrition Guide',
    header:null
  };
   render() {
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
      	this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header>
                <Left>
                  <Button transparent onPress={() => this.drawer._root.open()}>
                    <Icon name='menu'/>
                  </Button>
                </Left>
                <Body>
                  <Title>Nutrition Guide</Title>
                </Body>
              </Header>              
              <Content>
                <View>
                </View>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}