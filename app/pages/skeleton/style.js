const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	headerIcon: {
		color: '#fff',
		width: 32,
		fontSize:20
	},
	navBarColor:{
        color: '#fff'
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#fff',
        fontSize: 24,
        paddingLeft:30
    },
    headerTabStyle:{
    	backgroundColor:'#499FFD',
    	color:'#B2B2B2'
    },
    navBarStyle:{
        backgroundColor:'#499FFD',
        borderBottomWidth: 0.5,
        borderBottomColor:'#B2B2B2',
        height: 70,
    },
    navbarIcon:{
    	fontSize:20,
    	color:'#fff',
    	paddingLeft:10
    },
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		marginTop: 5,
  		lineHeight: 30
  	},
  	// Footer Style
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	footerIconInactive:{
		color: '#B2B2B2',
    	fontSize: 22,
	},
	footerIconActive:{
		color: '#3B5998',
    	fontSize: 22,
	},
	footerTextInactive:{
		color: '#B2B2B2',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	footerTextActive:{
		color: '#3B5998',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	thumbnailSize:{
		height:30,
		width:30,
		borderRadius:15,
	},
	thumbnailMargin:{
		marginTop: 5,
		marginBottom: 5,
	},
	cardTitle:{
		color: '#499FFD',
		fontSize: 22,
		fontFamily: 'BalooThambi-Regular',
		textAlign:'center',
		flex:1
	},
	navBarFooterStyle:{
		backgroundColor:'#fff',
		height: 60,
	},
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonNavbarText:{
		flexDirection: 'column',
		marginBottom: (Platform.OS === 'ios') ? -5 : 5,
		paddingBottom: 50,
	},
	buttonNavbarTextActive:{
		flexDirection: 'column',
		backgroundColor: '#499FFD',
		marginBottom: (Platform.OS === 'ios') ? -15 : -5,
		paddingBottom: 50,
		borderBottomLeftRadius: 0,
		borderBottomRightRadius: 0,
		borderTopLeftRadius: 50,
		borderTopRightRadius: 50,
	},
	footerIconInactive:{
		color: '#7e7e7e',
    	fontSize: 20,
    	paddingTop: 6,
	},
	footerIconActive:{
		color: '#fff',
    fontSize: 20,
    fontFamily:'Montserrat'
	},
	footerIconInactive2:{
		color: '#7e7e7e',
    	fontSize: 24,
    	paddingTop: 6,
	},
	footerIconActive2:{
		color: '#fff',
    fontSize: 24,
    fontFamily:'Montserrat'
	},
	footerTextInactive:{
		color: '#7e7e7e',
		fontSize: 8,
		fontFamily: 'Montserrat',
	},
	footerTextActive:{
		color: '#fff',
		fontSize: 8,
		fontFamily: 'Montserrat',
	},
	footerTextInactive2:{
		color: '#7e7e7e',
		fontSize: 8,
		fontFamily: 'Montserrat',
		marginTop: -4,
	},
	footerTextActive2:{
		color: '#fff',
		fontSize: 8,
		fontFamily: 'Montserrat',
		marginTop:-4,
	},
	thumbnailSize:{
		height:30,
		width:30,
		borderRadius:15,
	},
	thumbnailMargin:{
		marginTop: 5,
		marginBottom: 5,
	}

}