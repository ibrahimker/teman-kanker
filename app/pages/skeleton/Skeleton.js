import React, { Component } from 'react';
import {Text,View,Button,Container,Content,Header,Left,Body,Right,Title,Footer,FooterTab,Tabs,Badge} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage, NetInfo, ImageBackground} from 'react-native';

import OneSignal from 'react-native-onesignal';

import TabBeranda from '../beranda/Beranda.js';
import TabInformasi from '../informasi/Informasi.js';
import TabDirektori from '../direktori/Direktori.js';
// import TabPesan from '../pesan/Pesan.js';
import TabLainnya from '../lainnya/Lainnya.js';
import Login from '../login/LoginNoClose.js'

import FooterTabNavigation from '../../components/footerTab/FooterTab.js';

import images from '../../config/images.js';
import styles from './style.js';
export default class SkeletonScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    'headerTitle':'Beranda',
    'screen':'0',
    'userId':'',
    'oneSignalId':'',
    connection: true
  }

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      this.setState({connection: isConnected});
    });

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.setState({connection: true});
    }
    else {
      this.setState({connection: false});
    }
  }

  removeConnectivityListener() {
    console.log('eventremoved');
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  switchScreen(screen,headerTitle) {
    this.setState({screen: screen})
    this.setState({headerTitle: headerTitle})
  }

  componentWillMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('registered', this.onRegistered);    
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(2);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('registered', this.onRegistered);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onRegistered(notifData) {
    console.log("Device had been registered for push notifications!", notifData);
  }

  onIds(device) {
    console.log('Device info: ', device.userId);
    setTimeout(function() {
      AsyncStorage.setItem('onesignalId', device.userId);
    }, 3000);
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'headerTitle':'Beranda',
      'screen':'0',
      'id':'',
      'userId':'',
      dataLoaded: false
    };

    console.disableYellowBox = true;

    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({userId: result})
    });
    AsyncStorage.getItem('onesignalId', (err, result) => {
      if(result!== null){
        if(this.state.userId!=''){
          fetch('https://temankanker.com/api/UserKankers/registerDevice', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              "userId": this.state.userId,
              "onesignalId":result
            })
          })
          .then((response) => {
            fetch('https://temankanker.com/api/UserKankers/loginDevice', {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                "userId": this.state.userId,
                "onesignalId":result
              })
            })
            .then((response) => {
            })
          })
        }
      }
    });
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');

    let AppComponent = null;

    if (this.state.screen == 0) {
       AppComponent = TabBeranda
    }
    else if (this.state.screen == 1) {
       AppComponent = TabInformasi
    }
    else if (this.state.screen == 2) {
      if(this.state.userId == '' || this.state.userId == null || this.state.userId === undefined) {
        AppComponent = Login
      }
      else {
        AppComponent = TabPesan
      }
    }
    else if (this.state.screen == 3) {
       AppComponent = TabDirektori
    }
    else if (this.state.screen == 4) {
       AppComponent = TabLainnya
    }
    else {
       AppComponent = TabBeranda
    }
      return (
      <Container>
          <ImageBackground source={images.backgroundHome} style={styles.backgroundImage}>
            <AppComponent navigation={this.props.navigation} removeListener={this.removeConnectivityListener}/>
          </ImageBackground>
          
          {!this.state.connection && (
              <View style={{backgroundColor: '#d9534f', padding: 15, position: 'absolute', bottom: 60, left: 0, right: 0, opacity: 0.9}}>
                <Text style={{color: 'white'}}>Koneksi internet anda terputus</Text>
              </View>
          )}

          <Footer noShadow style={styles.navBarFooterStyle}>
            <Body style={styles.bodySpaceBetween}>
              <Button transparent onPress={() => this.switchScreen(0,'Beranda') } style={this.state.screen == 0 ? styles.buttonNavbarTextActive : styles.buttonNavbarText}>
                <Icon style={this.state.screen == 0 ? styles.footerIconActive2 : styles.footerIconInactive2} name='home'/>
                  <Text style={this.state.screen == 0 ? styles.footerTextActive2 : styles.footerTextInactive2}>Beranda</Text>
              </Button>
              <Button transparent onPress={() => this.switchScreen(1,'Informasi') } style={this.state.screen == 1 ? styles.buttonNavbarTextActive : styles.buttonNavbarText}>
                <Icon style={this.state.screen == 1 ? styles.footerIconActive : styles.footerIconInactive} name='file'/>
                  <Text style={this.state.screen == 1 ? styles.footerTextActive : styles.footerTextInactive}>Informasi</Text>
              </Button>
              <Button transparent onPress={() => this.switchScreen(2,'Pesan') } style={this.state.screen == 2 ? styles.buttonNavbarTextActive : styles.buttonNavbarText}>
                <Icon style={this.state.screen == 2 ? styles.footerIconActive : styles.footerIconInactive} name='commenting'/>
                  <Text style={this.state.screen == 2 ? styles.footerTextActive : styles.footerTextInactive}>Pesan</Text>
              </Button>
              <Button transparent onPress={() => this.switchScreen(3,'Direktori') } style={this.state.screen == 3 ? styles.buttonNavbarTextActive : styles.buttonNavbarText}>
                <Icon style={this.state.screen == 3 ? styles.footerIconActive : styles.footerIconInactive} name='list'/>
                  <Text style={this.state.screen == 3 ? styles.footerTextActive : styles.footerTextInactive}>Direktori</Text>
              </Button>
              <Button transparent onPress={() => this.switchScreen(4,'Lainnya') } style={this.state.screen == 4 ? styles.buttonNavbarTextActive : styles.buttonNavbarText}>
                <Icon style={this.state.screen == 4 ? styles.footerIconActive : styles.footerIconInactive} name='ellipsis-h'/>
                  <Text style={this.state.screen == 4 ? styles.footerTextActive : styles.footerTextInactive}>Lainnya</Text>
              </Button>
            </Body>
          </Footer>
      </Container>
    );
  }
}