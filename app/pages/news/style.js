const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
		maxWidth: 40
	},
	navBarTitle:{
		fontFamily: 'Kreon-Regular',
		fontWeight: '400',
		color: '#3B5998',
		fontSize: 24,
	},
	navBarStyle:{
		backgroundColor:'#ffffff',
		height: 70
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		marginTop: 5,
  		lineHeight: 30
  	},
  	// Footer Style
  	navBarFooterStyle:{
		backgroundColor:'#ffffff',
		borderBottomWidth: 0.5,
		borderBottomColor:'#B2B2B2',
		height: 60
	},
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonNavbarText:{
		flexDirection: 'column',
	},
	footerIconInactive:{
		color: '#B2B2B2',
    	fontSize: 22,
	},
	footerIconActive:{
		color: '#3B5998',
    	fontSize: 22,
	},
	footerTextInactive:{
		color: '#B2B2B2',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	footerTextActive:{
		color: '#3B5998',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	thumbnailSize:{
		height:30,
		width:30,
		borderRadius:15,
	},
	thumbnailMargin:{
		marginTop: 5,
		marginBottom: 5,
	}

}