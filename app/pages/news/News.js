import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Footer,FooterTab,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class NewsScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    news: [],
    dataLoaded: false
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.news[index].reminder = value;
    this.setState(stateCopy);
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      news: [],
      dataLoaded: false
    };
    AsyncStorage.getItem('isLoggedIn', (err, result) => {
      this.setState({ 'isLoggedIn': result });
      if(this.state.isLoggedIn == 'false') {
        this.props.navigation.navigate('Login');
      }
    });
    AsyncStorage.getItem('username', (err, result) => {
      this.setState({ 'username': result });
      if(this.state.username == '') {
        this.props.navigation.navigate('Login');
      }
    });
  }

  componentDidMount() {
    fetch('https://temankanker.com/api/news')
    .then((response) => response.json())
    .then((data) => {
      for(i=0; i<data.length; i++) {
        //data[i].reminder = false;
      }
      return this.setState({ news: data, dataLoaded: true });
    })
    .done();
  }

 renderNews() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return this.state.news.map((newz,index) => (
        <Card key={newz.id} style={{marginTop: 16}}>
          <CardItem style={styles.thumbnailMargin}>
            <Left>
              <Thumbnail style={styles.thumbnailSize} source={{uri: 'https://api.adorable.io/avatars/285/abott@adorable.pnga'}} />
              <Body style={{marginLeft: 15}}>
                <Text style={styles.textUsername}>{newz.author_news}</Text>
                <Text style={styles.textUserTitle}>Penulis</Text>
              </Body>
            </Left>
          </CardItem>
          
          <TouchableOpacity onPress={() =>  this.props.navigation.navigate('NewsDetail', {id : newz.id })}>
          
          <CardItem cardBody>
            <Image source={{uri: newz.pict_news}} style={{height: 200, width: null, flex: 1, borderRadius: 5}}/>
          </CardItem>
          <CardItem>
            <Body style={{marginTop: 5}}>
              {/*<Text style={styles.textUserTitle}>{newz.date_news}</Text>*/}
              <Text style={styles.textUserTitle}>{newz.date_news}</Text>
              <Text style={styles.textTitle}>{newz.title_news}</Text>
            </Body>
          </CardItem>
          <CardItem style={{marginTop: -25, backgroundColor: 'transparent'}}>
            <Left>
              <Button transparent>
                <Icon name="share-alt" style={{color: '#B2B2B2', fontSize: 18}}/>
                <Text style={{marginLeft: 5, color: '#B2B2B2'}}>{newz.share_news}</Text>
              </Button>
              <Button transparent>
                <Icon name="heart" style={{color: '#B2B2B2', fontSize: 18}}/>
                <Text style={{marginLeft: 5, color: '#B2B2B2'}}>{newz.like_news}</Text>
              </Button>
            </Left>
          </CardItem>
          </TouchableOpacity>
        </Card>
        
      ));
    }
  }
 render() {
    StatusBar.setBarStyle('dark-content', true);
    StatusBar.setBackgroundColor('white');
    openDrawer = () => {
      this.drawer._root.open()
    };
    closeDrawer = () =>{
      this.drawer._root.close()
    };
    pressDrawer = () =>{
    }
      return (
      <Container>
      <Drawer
          ref={(ref) => { this.drawer = ref; }}
          content={<Sidebar navigation={this.props.navigation} />}
          onClose={() => this.drawer._root.close()}
          panOpenMask={.25} >
          <Header noShadow style={styles.navBarStyle}>
            <Left>
              {/*<Button transparent onPress={() => this.drawer._root.open()}>
                              <Icon style={styles.navBarColor} name='menu'/>
                            </Button>*/}
            </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Beranda</Title>
            </Body>
            <Right>
              {/*<Button transparent onPress={() =>  this.props.navigation.navigate('Notes')}>
                              <Icon style={styles.footerIconInactive} name='sticky-note'/>
                            </Button>
                            <Button transparent onPress={() =>  this.props.navigation.navigate('ChatList')}>
                              <Icon style={styles.footerIconInactive} name='comments'/>
                            </Button>*/}

              {/*<Button transparent onPress={() =>  this.props.navigation.navigate('Notes')} style={styles.buttonNavbarText}>
                              <Icon style={styles.footerIconInactive} name='sticky-note'/>
                              <Text style={styles.footerTextInactive}>Memo</Text>
                            </Button>
                            <Button transparent onPress={() =>  this.props.navigation.navigate('ChatList')} style={styles.buttonNavbarText}>
                              <Icon style={styles.footerIconInactive} name='comments'/>
                              <Text style={styles.footerTextInactive}>Pesan</Text>
                            </Button>*/}
            </Right>
          </Header>              
          <Content style={{backgroundColor: '#ffffff'}}>
            <Image source={images.backgroundHome} style={styles.backgroundImage}>
              <View style={{margin:16, marginTop: 5}}>

              {this.renderNews()}  

              </View>
            </Image>
          </Content>

          <Footer noShadow style={styles.navBarFooterStyle}>
            <FooterTab>
              <Button vertical onPress={() =>  this.props.navigation.navigate('News')} style={styles.buttonNavbarText} onPress={() =>  this.props.navigation.navigate('News')} style={styles.buttonNavbarText}>
                <Icon style={styles.footerIconActive} name='home' />
                <Text style={styles.footerTextActive}>Beranda</Text>
              </Button>
              <Button vertical onPress={() =>  this.props.navigation.navigate('Learn')} style={styles.buttonNavbarText}>
                <Icon style={styles.footerIconInactive} name='info-circle' />
                <Text style={styles.footerTextInactive}>Info</Text>
              </Button>
              <Button badge vertical onPress={() =>  this.props.navigation.navigate('ChatList')} style={styles.buttonNavbarText}>
                <Badge style={{marginBottom:-18}}><Text>51</Text></Badge>
                <Icon style={styles.footerIconInactive} name='comments' />
                <Text style={styles.footerTextInactive}>Pesan</Text>
              </Button>
              <Button vertical onPress={() =>  this.props.navigation.navigate('CancerSupport')} style={styles.buttonNavbarText}>
                <Icon style={styles.footerIconInactive} name='location-arrow' />
                <Text style={styles.footerTextInactive}>Direktori</Text>
              </Button>
              <Button vertical onPress={() => this.drawer._root.open()} style={styles.buttonNavbarText}>
                <Icon style={styles.footerIconInactive} name='ellipsis-h' />
                <Text style={styles.footerTextInactive}>Lainnya</Text>
              </Button>
            </FooterTab>
          </Footer>
          
      </Drawer>
      </Container>
    );
  }
}