const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarStyle:{
		backgroundColor:'#ffffff',
		height: 70,
	},
	navBarWidth:{
		maxWidth: 40
	},
	navBarColor:{
		color: '#B2B2B2'
	},
	navBarTitle:{
		fontFamily: 'Kreon-Regular',
		fontWeight: '400',
		color: '#3B5998',
		fontSize: 24
	},
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	colorTabGrey:{
		color: '#B2B2B2',
		fontFamily: 'Montserrat-Regular',
		fontWeight: '400',
		fontSize: 12
	},
	colorTabNavy:{
		color: '#3B5998',
		fontFamily: 'Montserrat-Regular',
		fontWeight: '400',
		fontSize: 12
	},
	backgroundColorWhite:{
		backgroundColor: '#ffffff'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	contentStyle1:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginBottom: 20,
  		fontFamily: 'Kreon-Regular',
  		fontSize: 18,
  		lineHeight: 25,
  		color: '#3EBB64'
  	},
  	contentStyle2:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 20,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 28,
  		color: '#868686'
  	},
  	contentStyle2Point:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 0,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 28,
  		color: '#3b3b3b'
  	},
  	contentStyle1Quote:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginLeft: 60,
  		marginTop: 20,
  		marginBottom: 10,
  		fontFamily: 'Kreon-Regular',
  		fontSize: 18,
  		lineHeight: 25,
		color: '#3B5998',
  	},
  	contentStyle2Quote:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: -10,
  		marginLeft: 60,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 28,
  		color: '#868686'
	  },
	gridPictureR:{
		margin: 15, 
		marginLeft: 7.5, 
		height: 210,
		borderRadius: 5,		
		width: 165,
		zIndex:3,
		shadowColor: '#000',
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 0.3,
	    shadowRadius: 2,
	},
	gridPictureL:{
		margin: 15, 
		marginRight: 7.5, 
		height: 210,
		width: 165,
		borderRadius: 5,
		shadowColor: '#000',
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 0.3,
	    shadowRadius: 2,	
	},
	gridBackground:{
		flex: 1,
		resizeMode: 'stretch',
		borderRadius: 5,
		height: 210,		
		width: 165,
	},
	backgroundGridColor: {
		flex: 1,
		flexDirection: 'column',
		alignItems:'center',
		justifyContent:'center',
		height: null,
		width: null
	},
	textStyle:{
  		backgroundColor: 'transparent',
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
		color: '#ffffff',
	},
	// Footer Style
  	navBarFooterStyle:{
		backgroundColor:'#ffffff',
		borderBottomWidth: 0.5,
		borderBottomColor:'#B2B2B2',
	},
	bodySpaceBetween:{
		marginLeft: 16,
		marginRight: 16,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	buttonNavbarText:{
		flexDirection: 'column',
		marginBottom: 5
	},
	footerIconInactive:{
		color: '#B2B2B2',
    	fontSize: 22,
	},
	footerIconActive:{
		color: '#3B5998',
    	fontSize: 22,
	},
	footerTextInactive:{
		color: '#B2B2B2',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	footerTextActive:{
		color: '#3B5998',
		fontSize: 10,
		fontFamily: 'Montserrat-Regular',
	},
	

}