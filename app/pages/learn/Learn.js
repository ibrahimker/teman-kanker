import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Footer,Left,Body,Right,Title,Tabs,Tab,ScrollableTab} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class LearnScreen extends Component {
  static navigationOptions = {
    title: 'Learn',
    header:null
  };

  state = {
      learn: [],
      dataLoaded: false,
  }

  componentDidMount() {
    fetch('https://temankanker.com/api/understanding_cancer')
    .then((response) => response.json())
    .then((data) => {
      for(i=0; i<data.length; i++) {
        //data[i].reminder = false;
      }
      return this.setState({ learn: data, dataLoaded: true });
    })
    .done();
  }

  renderLearn() {
      return this.state.learn.map((learns,index) => (
        <Col style={styles.gridPictureL} onPress={() =>  this.props.navigation.navigate('LearnDetail', {id : learns.id })}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>{learns.type_under}</Text>
                                </Image>
                              </Image>
                            </Col>
                            ));}

   render() {
      StatusBar.setBarStyle('dark-content', true);
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
      	this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header hasTabs style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  {/*<Button transparent onPress={() => this.drawer._root.open()}>
                                      <Icon style={styles.navBarColor} name='menu'/>
                                    </Button>*/}
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Informasi Kanker</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>              
              <Content style={{backgroundColor:'#ffffff'}}>
                <View>
                  <Tabs renderTabBar={()=> <ScrollableTab />}>
                    <Tab heading="Anak-anak" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                           <Grid>
                            {this.renderLearn()}
                            
                          </Grid>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Dewasa" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                           <Grid>
                            <Col style={styles.gridPictureL} onPress={() =>  this.props.navigation.navigate('LearnDetail')}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>Leukemia</Text>
                                </Image>
                              </Image>
                            </Col>
                            <Col style={styles.gridPictureR} onPress={() =>  this.props.navigation.navigate('LearnDetail')}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>Neuroblastoma</Text>
                                </Image>
                              </Image>
                            </Col>
                          </Grid>
                          <Grid>
                            <Col style={styles.gridPictureL} onPress={() =>  this.props.navigation.navigate('LearnDetail')}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>Leukemia</Text>
                                </Image>
                              </Image>
                            </Col>
                            <Col style={styles.gridPictureR} onPress={() =>  this.props.navigation.navigate('LearnDetail')}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>Leukemia</Text>
                                </Image>
                              </Image>
                            </Col>
                          </Grid>
                          <Grid>
                            <Col style={styles.gridPictureL} onPress={() =>  this.props.navigation.navigate('LearnDetail')}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>Leukemia</Text>
                                </Image>
                              </Image>
                            </Col>
                            <Col style={styles.gridPictureR} onPress={() =>  this.props.navigation.navigate('LearnDetail')}>
                              <Image source={images.leukimia} style={styles.gridBackground}>
                                <Image style={styles.backgroundGridColor} source={images.backgroundGrid}>
                                  <Text style={styles.textStyle}>Leukemia</Text>
                                </Image>
                              </Image>
                            </Col>
                          </Grid>
                        </Image>
                      </View>
                    </Tab>
                    
                  </Tabs>
                </View>
              </Content> 

              <Footer noShadow style={styles.navBarFooterStyle}>
                <Body style={styles.bodySpaceBetween}>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('News')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='home'/>
                    <Text style={styles.footerTextInactive}>Beranda</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('Learn')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconActive} name='info-circle'/>
                    <Text style={styles.footerTextActive}>Informasi</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('ChatList')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='comments'/>
                    <Text style={styles.footerTextInactive}>Pesan</Text>
                  </Button>
                  {/*<Button transparent onPress={() =>  this.props.navigation.navigate('SymptomChecker')} style={styles.buttonNavbarText}>
                                  <Icon style={styles.footerIconInactive} name='stethoscope'/>
                                  <Text style={styles.footerTextInactive}>Gejala</Text>
                                </Button>*/}
                  <Button transparent onPress={() =>  this.props.navigation.navigate('CancerSupport')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='location-arrow'/>
                    <Text style={styles.footerTextInactive}>Direktori</Text>
                  </Button>
                  <Button transparent onPress={() => this.drawer._root.open()} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='ellipsis-h'/>
                    <Text style={styles.footerTextInactive}>Lainnya</Text>
                  </Button>
                  {/*<Button transparent onPress={() =>  this.props.navigation.navigate('Profile')} style={styles.buttonNavbarText}>
                                  <Icon style={styles.footerIconInactive} name='user'/>
                                  <Text style={styles.footerTextInactive}>Profil</Text>
                                </Button>*/}
                </Body>
              </Footer>
          </Drawer>
          </Container>
        );
    }
}


{
  /*<View>
                  <Tabs renderTabBar={()=> <ScrollableTab />}>
                    <Tab heading="Leukimia" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Leukemia atau kanker darah adalah penyakit yang paling banyak di jumpai pada anak - anak. Penyakit ini disebabkan karena adanya pertumbuhan atau transformasi yang terjadi secara tidak wajar pada sel darah putih ( leukosit ) dan menyebabkan pertumbuhan sel darah putih yang tidak terkendali.</Text>
                          <Text style={styles.contentStyle2}>Produksi sel darah putih yang berlebih ini tentunya menyebabkan ketidak normalan dalam tubuh, seperti gangguan kepada fungsi tulang sumsum, yang menakan sel darah merah, sehingga menyebabkan kekurangan darah, pendarahan dan membuat tubuh tidak berdaya menghadapi infeksi.</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Anemia (Pucat)</Text>
                          <Text style={styles.contentStyle2Point}>• Mudah lelah</Text>
                          <Text style={styles.contentStyle2Point}>• Demam berkepanjangan</Text>
                          <Text style={styles.contentStyle2Point}>• Mudah terkena infeksi</Text>
                          <Text style={styles.contentStyle2Point}>• Pendarahan yang tidak jelas sebabnya</Text>
                          <Text style={styles.contentStyle2Point}>• Nyeri tulang</Text>
                          <Text style={styles.contentStyle2Point}>• Pembengkakan perut</Text>
                          <Text style={styles.contentStyle2Point}>• Pembengkakan kelenjar getah bening</Text>
                          <Text style={styles.contentStyle2Point}>• Memar di area muka</Text>
                          <Text style={styles.contentStyle2Point}>• Berdarah tanpa sebab</Text>
                          <Text style={styles.contentStyle2Point}>• Kehilangan nafsu makan</Text>
                          <Text style={styles.contentStyle2Point}>• Penyusutan berat badan</Text>
                          <Text style={styles.contentStyle2Point}>• Keseimbangan badan</Text>
                          <Text style={styles.contentStyle2Point}>• Ketidaknyamanan di bawah tulang rusuk kiri bawah</Text>
                          <Text style={styles.contentStyle2Point}>• Sel darah putih yang sangat tinggi</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Tumor Otak" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Tumor Otak merupakan penyakit yang mengganggu fungsi, merusak struktur susunan saraf pusat karena terletak di dalam rongga yang terbatas (rongga tengkorak) dan pertumbuhan sel yang abnormal pada dalam atau disekitar organ otak.</Text>
                          <Text style={styles.contentStyle2}>Ada bermacam-macam jenis tumor otak yang dibedakan ke dalam dua kelompok berdasarkan perkembangannya, yaitu tumor jinak (tidak bersifat kanker) dan tumor ganas (bersifat kanker). Tumor yang tumbuh di otak dikenal dengan istilah tumor otak primer, sedangkan tumor yang tumbuh di bagian lain dari tubuh dan menyebar hingga ke otak disebut dengan tumor otak sekunder atau metastatik.</Text>
                          <Text style={styles.contentStyle2}>Tingkatan tumor otak terbagi dari tingkat 1 hingga tingkat 4. Pengelompokan ini didasari oleh perilaku tumor itu sendiri, seperti lokasi tumbuhnya tumor, kecepatan pertumbuhan, dan cara penyebarannya. Tumor otak yang tergolong jinak dan tidak berpotensi ganas berada pada tingkat 1 dan 2. Sedangkan pada tingkat 3 dan 4, biasanya sudah berpotensi menjadi kanker dan sering disebut sebagai tumor otak ganas atau kanker otak.</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit kepala</Text>
                          <Text style={styles.contentStyle2Point}>• Mual tanpa penyebab</Text>
                          <Text style={styles.contentStyle2Point}>• Muntah tanpa penyebab</Text>
                          <Text style={styles.contentStyle2Point}>• Daya penglihatan berkurang</Text>
                          <Text style={styles.contentStyle2Point}>• Penurunan kesadaran</Text>
                          <Text style={styles.contentStyle2Point}>• Kejang-kejang</Text>
                          <Text style={styles.contentStyle2Point}>• Gangguan berbicara</Text>
                          <Text style={styles.contentStyle2Point}>• Keseimbangan tubuh</Text>
                          <Text style={styles.contentStyle2Point}>• Mati rasa pada organ tubuh</Text>
                          <Text style={styles.contentStyle2Point}>• Sulit berbicara</Text>
                          <Text style={styles.contentStyle2Point}>• Masalah kepada pendengaran</Text>
                          <Text style={styles.contentStyle2Point}>• Sulit konsentrasi</Text>
                          <Text style={styles.contentStyle2Point}>• Berhalusinasi</Text>
                          <Text style={styles.contentStyle2Point}>• Kehilangan ingatan (demensia)</Text>
                          <Text style={styles.contentStyle2Point}>• Ketidaksadaran</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Retinoblastoma" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Retinoblastoma adalah kanker pada retina (daerah di belakang mata yang peka terhadap cahaya) yang menyerang anak berumur kurang dari 5 tahun. Penyakit ini muncul dari sel embrionik retina multipotent.</Text>
                          <Text style={styles.contentStyle2}>Kanker ini berbentuk padat, dan pada mereka yang sudah parah biasanya ditandai penonjolan bola mata, dan pertumbuhan penonjolan itu bisa sangat cepat. Sebanyak 2% dari kanker pada masa kanak-kanak adalah retinoblastoma.</Text>
                          <Text style={styles.contentStyle2}>Penyebab retinoblastoma adalah tidak terdapatnya gen penekan tumor yang sifatnya cenderung diturunkan. Sekitar 10% penderita retinoblastoma memiliki saudara yang juga menderita retinoblastoma dan mendapatkan gennya dari orangtua. Kanker bisa menyerang salah satu maupun kedua mata. Kanker juga bisa menyebar ke kantung mata dan ke otak (melalu saraf penglihatan/nervus optikus).</Text>
                          <Text style={styles.contentStyle1Quote}>“Retinoblastma paling sering menyerang anak-anak, tapi jarang dapat terjadi pada orang dewasa.”</Text>
                          <Text style={styles.contentStyle2Quote}>Hendrian D.Soebagjo. SPM(K)</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Pupil berwarna putih</Text>
                          <Text style={styles.contentStyle2Point}>• Mata juling (strabismus)</Text>
                          <Text style={styles.contentStyle2Point}>• Mata merah dan nyeri</Text>
                          <Text style={styles.contentStyle2Point}>• Gangguan penglihatan</Text>
                          <Text style={styles.contentStyle2Point}>• Iris pada kedua mata memiliki warna yang berlainan</Text>
                          <Text style={styles.contentStyle2Point}>• Kebutaan</Text>
                          <Text style={styles.contentStyle2Point}>• Pupil terus melebar</Text>
                          <Text style={styles.contentStyle2Point}>• Mata tidak bisa bergerak dan fokus pada arah yang sama</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit kepala</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Limfoma" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Limfoma adalah salah satu jenis kanker darah yang terjadi ketika limfosit B atau T, yaitu sel darah putih yang menjaga daya tahan tubuh, menjadi abnormal dengan membelah lebih cepat dari sel biasa atau hidup lebih lama dari biasanya.</Text>
                          <Text style={styles.contentStyle2}>Kanker ini menyerang sistem lymphatic, organ dan jaringan yang bertanggung jawab atas kekebalan organ tubuh dan membawa cairan yang disebut getah bening--salah satu komponen darah yang mengatur sel nutrisi. Pembesaran dapat terjadi di daerah leher, ketiak, selangkangan, usus. Bila timbulnya di kelenjar getah bening usus, maka dapat menyebabkan sumbatan pada usus dengan gejal sakit perut, muntah, tidak bisa buang air besar, dan demam.</Text>
                          <Text style={styles.contentStyle2}>Bila tumbuh di daerah dada, maka dapat mendorong atau menekan saluran nafas dan muka membiru. Limfoma dapat ditangani dengan melakukan kemoterapi dan kadang-kadang radioterapi atau transplantasi sumsum tulang, dan penyembuhannya tergantung kepada histologi, jenis, dan tahapan penyakit. Sel kanker tersebut biasanya muncul di nodus limfa, yang juga dapat memengaruhi organ lain seperti kulit, otak, dan tulang (limfoma ekstranodal).</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Berkurangnya jumlah sel darah merah</Text>
                          <Text style={styles.contentStyle2Point}>• Anemia (pucat)</Text>
                          <Text style={styles.contentStyle2Point}>• Sel darah putih & trombosit</Text>
                          <Text style={styles.contentStyle2Point}>• Nyeri tulang</Text>
                          <Text style={styles.contentStyle2Point}>• Keringat malam</Text>
                          <Text style={styles.contentStyle2Point}>• Kehilangan berat badan</Text>
                          <Text style={styles.contentStyle2Point}>• Kelelahan</Text>
                          <Text style={styles.contentStyle2Point}>• Anoreksia</Text>
                          <Text style={styles.contentStyle2Point}>• Gatal</Text>
                          <Text style={styles.contentStyle2Point}>• Dyspnea</Text>
                          <Text style={styles.contentStyle2Point}>• Demam</Text>
                          <Text style={styles.contentStyle2Point}>• Menggigil</Text>
                          <Text style={styles.contentStyle2Point}>• Sering mengalami infeksi</Text>
                          <Text style={styles.contentStyle2Point}>• Tidak nafsu makan</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit dada</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit perut</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Neuroblastoma" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Neuroblastoma adalah suatu jenis kanker pada system saraf. Karena berasal dari sistem saraf, kanker neuroblastoma dapat tumbuh pada berbagai bagian tubuh. Neuroblastoma juga termasuk ke dalam jenis golongan kanker ganas dan dibagi menjadi stadium rendah, stadium menengah dan stadium tinggi.</Text>
                          <Text style={styles.contentStyle2}>Kanker ini paling sering mulai tumbuh dari jaringan kelenjar adrenal yang terdapat di perut. Kemudian sel kanker melakukan penyebaran dengan cepat menuju hati, kelenjar getah bening, tulang, bahkan sampai ke sumsum tulang. Kanker neuroblastoma sering terjadi pada anak – anak. Neuroblastoma pada anak paling sering dialami pada anak yang berusia kurang dari lima tahun.</Text>
                          <Text style={styles.contentStyle2}>Neuroblastoma dapat terjadi di leher, rongga dada, dan mata. Bila terdapat di daerah mata dapat menyebabkan bola mata menonjol, kelopak mata turun dan pupil menyebar. Bila terdapat di tulang belakang dapat menekan saraf tulang belakang dan mengakibatkan kelumpuhan. Tumor di daerah perut akan teraba bila sudah besar. Penyebaran pada tulang dapat menyebabkan patah tulang tanpa sebab, tanpa nyeri sehingga penderitanya dapat pincang mendadak.</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Nyeri di bagian dada atau tulang</Text>
                          <Text style={styles.contentStyle2Point}>• Sesak napas</Text>
                          <Text style={styles.contentStyle2Point}>• Perubahan pada mata</Text>
                          <Text style={styles.contentStyle2Point}>• Ukuran pupil menjadi berbeda</Text>
                          <Text style={styles.contentStyle2Point}>• Kelopak mata turun</Text>
                          <Text style={styles.contentStyle2Point}>• Anemia (Pucat)</Text>
                          <Text style={styles.contentStyle2Point}>• Bagian tubuh bawah bisa menjadi lemah</Text>
                          <Text style={styles.contentStyle2Point}>• Mati rasa</Text>
                          <Text style={styles.contentStyle2Point}>• Gangguan pergerakan</Text>
                          <Text style={styles.contentStyle2Point}>• Jumlah trombosit mengurang</Text>
                          <Text style={styles.contentStyle2Point}>• Benjolan-benjolan di kulit</Text>
                          <Text style={styles.contentStyle2Point}>• Gangguan pada sistem pernapasannya</Text>
                          <Text style={styles.contentStyle2Point}>• Jumlah sel darah putih berkurang</Text>
                          <Text style={styles.contentStyle2Point}>• Mudah memar</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Tumor Wilms" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Tumor Wilms atau Nefroblastoma adalah tumor ganas yang merupakan tumor embryonal yang berkembang dari sisa jaringan ginjal yang imatur. Kanker ini dapat ditandai dengan kencing berdarah, rasa tidak enak didaerah perut, bila sudah cukup besar akan teraba keras dan biasanya diketahui ketika anak di mandikan.</Text>
                          <Text style={styles.contentStyle2}>Waktu puncak terjadinya tumor Wilms adalah sekitar usia 3 sampai 4 tahun dan jarang terjadi setelah usia 6 tahun. Tumor Wilms diyakini berkembang dari sel-sel ginjal yang belum matang. Lebih kurang 10 % tumor ini menyerang kedua ginjal secara bersamaan. Biasanya ditemukan secara kebetulan oleh dokter pada pemeriksaan rutin atau oleh orang tua ketika bermain bersama anaknya, yang menemukan massa yang besar di bawah sela - sela iga di kanan atau kiri abdomen.</Text>
                          <Text style={styles.contentStyle2}>Teknik pencitraan dapat membantu dokter untuk menentukan sejauh mana kanker dalam tumor Wilms dan rencana pengobatannya. Kemungkinan kesembuhan pada anak dengan tumor Wilms sangat besar. Terapi untuk Wilms’ tumor telah berkembang dari eksisi surgikal sebagai metode utama dan dikombinasikan dengan terapi multimodal yaitu kemoterapi dan radioterapi.</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Perut membesar</Text>
                          <Text style={styles.contentStyle2Point}>• Nyeri perut</Text>
                          <Text style={styles.contentStyle2Point}>• Demam</Text>
                          <Text style={styles.contentStyle2Point}>• Malaise (tidak enak badan)</Text>
                          <Text style={styles.contentStyle2Point}>• Nafsu makan berkurang</Text>
                          <Text style={styles.contentStyle2Point}>• Mual</Text>
                          <Text style={styles.contentStyle2Point}>• Muntah</Text>
                          <Text style={styles.contentStyle2Point}>• Sembelit</Text>
                          <Text style={styles.contentStyle2Point}>• Pertumbuhan berlebih pada salah satu sisi tubuh</Text>
                          <Text style={styles.contentStyle2Point}>• Demam</Text>
                          <Text style={styles.contentStyle2Point}>• Darah dalam urin</Text>
                          <Text style={styles.contentStyle2Point}>• Terdapat satu gumpalan dalam perut yang dapat dirasakan</Text>
                          <Text style={styles.contentStyle2Point}>• Tekanan darah tinggi</Text>
                          <Text style={styles.contentStyle2Point}>• Sering mengalami infeksi</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit di bagian tubuh</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Rabdomiosarkoma" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Rabdomiosarkoma merupakan suatu tumor ganas yang aslinya berasal dari jaringan lunak (soft tissue) tubuh, termasuk disini adalah jaringan otot, tendon dan connective tissue. Rabdomiosarkoma merupakan keganasan yang sering didapatkan pada anak-anak.</Text>
                          <Text style={styles.contentStyle2}>Ini adalah otot yang kita kendalikan sendiri (contohnya otot lengan atau kaki). Otot-otot ini juga disebut otot skeletal atau lurik. Tempat rhabdomyosarcoma yang paling umum ditemukan adalah kepala, leher, kantong kemih, vagina, lengan, kaki, dan bagian tubuh lainnya. Dalam rhabdomyosarcoma, sel kanker mirip dengan sel otot yang belum matang.</Text>
                          <Text style={styles.contentStyle2}>Walaupun tumor ini dipercaya berasal dari sel otot primitif dari tubuh, tumor ini dapat muncul dimana saja dalam tubuh, terkecuali jaringan tulang. Tumor ini dapat timbul di berbagai bagian tubuh seperti di kepala dan leher (38%), traktus genitourinarius (21%), ekstremitas (18%), tulang belakang (17%) dan retroperitoneum (7%). Ada tiga jenis rhabdomyosarcoma:</Text>
                          <Text style={styles.contentStyle2}>Rhabdomyosarcoma embrional, biasanya mempengaruhi anak di bawah 6 tahun. Rhabdomyosarcoma embrional sering muncul di daerah kepala dan leher, dan terutama di jaringan sekitar mata (rhabdomyosarcoma orbital). Area tumor umum lainnya adalah area kelamin dan kemih, yang dapat muncul di dalam rahim, vagina, kantong kemih atau kelenjar prostat.</Text>
                          <Text style={styles.contentStyle2}>Rhabdomyosarcoma alveolar biasanya mempengaruhi anak yang lebih tua atau remaja. Rhabdomyosarcoma alveolar cenderung lebih agresif daripada Rhabdomyosarcoma embrional, dan lebih sering muncul di lengan dan kaki, atau dada atau perut.</Text>
                          <Text style={styles.contentStyle2}>Jenis ketiga, bernama rhabdomyosarcoma anaplastic, adalah jenis terlangka. Orang dewasa lebih rentan daripada anak-anak.</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Keluar air mata</Text>
                          <Text style={styles.contentStyle2Point}>• Mata terasa sakit</Text>
                          <Text style={styles.contentStyle2Point}>• Mata menyembul</Text>
                          <Text style={styles.contentStyle2Point}>• Hidung tersumbat</Text>
                          <Text style={styles.contentStyle2Point}>• Mimisan</Text>
                          <Text style={styles.contentStyle2Point}>• Suara berubah</Text>
                          <Text style={styles.contentStyle2Point}>• Hidung mengeluarkan ingus dan nanah</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit perut</Text>
                          <Text style={styles.contentStyle2Point}>• Benjolan dalam perut yang bisa diraba</Text>
                          <Text style={styles.contentStyle2Point}>• Sulit buang air kecil</Text>
                          <Text style={styles.contentStyle2Point}>• Darah di dalam urin</Text>
                          <Text style={styles.contentStyle2Point}>• Benjolan keras di lengan</Text>
                          <Text style={styles.contentStyle2Point}>• Benjolan keras di kaki</Text>
                          <Text style={styles.contentStyle2Point}>• Pembengkakan</Text>
                          <Text style={styles.contentStyle2Point}>• Pendarahan</Text>
                          <Text style={styles.contentStyle2Point}>• Malaise</Text>
                          <Text style={styles.contentStyle2Point}>• Penurunan berat badan</Text>
                        </Image>
                      </View>
                    </Tab>
                    <Tab heading="Osteosarkoma" tabStyle={styles.backgroundColorWhite} activeTabStyle={styles.backgroundColorWhite} textStyle={styles.colorTabGrey} activeTextStyle={styles.colorTabNavy}>
                      <View>
                        <Image source={images.backgroundHome} style={styles.backgroundImage}>
                          <Text style={styles.contentStyle1}>Osteosarcoma (osteosarkoma), atau sarcoma osteogenik, adalah sejenis kanker tulang yang berawal dari sel-sel yang membentuk tulang. Kanker ini menyerang tulang, khususnya di area sekitar lutut dan banyak diderita remaja di bawah umur 15. Kanker ini agresif dan biasanya menyebar ke paru-paru.</Text>
                          <Text style={styles.contentStyle2}>Penyakit ini muncul ketika sel malah membentuk tumor kanker dan bukannya menumbuhkan tulang baru seperti biasanya. Kanker tulang dapat menyerang setiap bagian tulang, tetapi yang terbanyak ditemukan pada tungkai, lengan dan pinggul. Kadang-kadang didahului oleh rudapaksa (benturan keras) seperti jatuh dan sebagainya.</Text>
                          <Text style={styles.contentStyle2}>Osteosarcoma biasanya terjadi pada tulang panjang di dalam lengan dan kaki, sering kali di dekat lutut atau bahu, walaupun bisa terjadi pada setiap tulang. Penyakit ini lebih jarang terjadi pada tulang panggul, rahang, atau rusuk, juga jarang berkembang di dalam jari tangan atau jari kaki. Osteosarcoma mempengaruhi anak-anak dan orang dewasa muda tapi juga bisa terjadi pada orang dewasa yang lebih tua. Risiko mengidap osteosarcoma sedikit lebih tinggi pada pria daripada wanita. Penyakit ini bisa diatasi dengan mengurangi faktor risiko.</Text>
                          <Text style={styles.contentStyle2Point}>Apakah gejala-gejalanya?</Text>
                          <Text style={styles.contentStyle2Point}>• Bengkak atau benjol di sekitar tulang</Text>
                          <Text style={styles.contentStyle2Point}>• Nyeri</Text>
                          <Text style={styles.contentStyle2Point}>• Patah tulang tanpa sebab</Text>
                          <Text style={styles.contentStyle2Point}>• Sakit tulang</Text>
                          <Text style={styles.contentStyle2Point}>• Gerakan tubuh semakin terbatas</Text>
                          <Text style={styles.contentStyle2Point}>• Pincang</Text>
                          <Text style={styles.contentStyle2Point}>• Sering terbangun di malam hari ketika sedang tidur</Text>
                          <Text style={styles.contentStyle2Point}>• Tulang semakin lemah</Text>
                          <Text style={styles.contentStyle2Point}>• Kesakitan ketika udara dingin</Text>
                          <Text style={styles.contentStyle2Point}>• Malaise</Text>
                          <Text style={styles.contentStyle2Point}>• Anemia (pucat)</Text>
                        </Image>
                      </View>
                    </Tab>
                  </Tabs>
                </View> */
}