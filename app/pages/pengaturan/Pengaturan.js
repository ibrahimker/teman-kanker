import React, { Component } from 'react';
import {Text,View,Button,Container,Content,Header,Left,Body,Right,Title,Footer,FooterTab,Tabs} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';

import images from '../../config/images.js';
import styles from './style.js';

export default class PengaturanScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    'headerTitle':'Beranda',
    'screen':'0',
  }

  constructor(props) {
    super(props);
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');
      return (
      <Container>
          <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='arrow-left'/>
                  </Button>
              </Left>
              <Body style={styles.navBarCentered}>
                <Title style={styles.navBarTitle}>Pengaturan</Title>
              </Body>
              <Right style={styles.navBarWidth}/>
            </Header>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
            <Content>
              <View>
                
              </View>
            </Content>
          </Image>
      </Container>
    );
  }
}