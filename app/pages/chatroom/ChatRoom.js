import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Spinner, Toast, List, ListItem, Input, Item} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { GiftedChat, Bubble, MessageText, Time } from 'react-native-gifted-chat';
import io from 'socket.io-client';

import moment from 'moment';

import { StyleSheet, StatusBar,AsyncStorage, Modal, TouchableWithoutFeedback, Image, NetInfo, TouchableOpacity, Platform, BackHandler } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as pesanActions from '../../actions/pesanActions.js'

const CHAT_LIMIT = 50

class ChatRoomScreen extends Component {
  static navigationOptions = {
    title: 'Chat Room',
    header:null
  };

  state = {
    receiverData: [],
    receiverProfPict : [],
    messages: [],
    sender:"",
    roomId:'',
    receiver:"",
    userId:'',
    dataLoaded:false,
    idLoaded:false,
    message: '',
    scrollCounter: 0,
    showReceiverProfile: false,
    loadEarlier: false
  };

  constructor(props) {
    super(props);
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ userId: result, idLoaded: true  });
    });
  }

  componentWillUnmount() {

    if(Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.goToPesan)
    }

    this.socket.close();
    this.removeConnectivityListener();
  }

  componentWillMount() {
    
  }

  componentDidMount() {
    this.socket = io('https://temankanker.com', { transports: ['websocket'] });
    this.onReceivedMessage = this.onReceivedMessage.bind(this);
    this.socket.emit('join', this.props.navigation.state.params.roomId);
    this.socket.on('message', this.onReceivedMessage);
    this.getUserData(this.props.navigation.state.params.receiverId);
    this.enterRoom();
    this.retrieveChat();

    if(Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.goToPesan)
    }

    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      this.setState({connection: isConnected});
    });

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  // Network listener
  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.setState({connection: true});
    }
    else {
      this.setState({connection: false});
    }
  }

  removeConnectivityListener() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }
  //

  enterRoom(){
    fetch('https://temankanker.com/api/Rooms/enterRoom', {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "roomId":this.props.navigation.state.params.roomId,
        "userId":parseInt(this.props.navigation.state.params.senderId),
      })
    })
    .then((response) => { 

    })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    });
  }

  retrieveChat() {
    fetch('https://temankanker.com/api/Messages?filter[where][roomId]='+ this.props.navigation.state.params.roomId +'&filter[order]=created_at%20desc&filter[limit]='+ CHAT_LIMIT +'&filter[skip]=' + this.state.scrollCounter * 50)
    .then((response) => response.json())
    .then((data) => {

      for (var i = 0; i < data.length; i++) {
        this.onReceive(data[i].content,data[i].createdBy,data[i].created_at)
      }

      return this.setState({ dataLoaded: true, loadEarlier: false, scrollCounter: this.state.scrollCounter + 1});
    })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  onReceive(text,senderId,createdAt) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.prepend(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(createdAt),
          user: {
            _id: senderId
          }
        }),
      };
    });
  }

   onReceive2(text,senderId,createdAt) {
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, {
          _id: Math.round(Math.random() * 1000000),
          text: text,
          createdAt: new Date(createdAt),
          user: {
            _id: senderId
          }
        }),
      };
    });
  }

  onReceivedMessage(messages){
    this.onReceive2(messages.content,messages.createdBy,messages.created_at)
  }

  storeMessages(messages = []){
    this.setState((previousState) => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
  }

  sendToDB(message){
    fetch('https://temankanker.com/api/Messages/sendmessage', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "content":message,
        "roomId":this.props.navigation.state.params.roomId,
        "createdBy":this.state.userId
      })
    })
    .then((response) => { 
      
    })
  }

  onSend() {
    /*this.storeMessages(messages);*/
    this._textInput.setNativeProps({text: ''});
    if(this.state.connection) {
      this.sendToDB(this.state.message);
    }
    else {
      Toast.show({
        text: 'Koneksi internet anda terputus',
        position: 'top',
        type:'danger',
        duration: 2000
      });
    }
  }


  renderChatInput() {
    return (
      <View style={{ height: 50, width: null, backgroundColor:'white', padding: 7, overflow: "visible"}}>
        <Item rounded style={{flex: 1, width: "80%", backgroundColor: '#efefef', borderColor: 'white', padding: 5 }}>
          <Input ref={component => this._textInput = component} style={{fontSize: 14}} placeholder={'Tulis pesan kamu disini'} placeholderTextColor={'#7d6a97'} onChangeText={(text) => this.setState({ message: text })}/>
        </Item>
        <TouchableOpacity onPress={() => this.onSend()} style={{position: 'absolute', bottom: 0, right: 7, flex: 1, justifyContent: 'center', alignItems: 'center', height: 70, width: 55, backgroundColor: '#855fa8', borderTopLeftRadius: 30, borderTopRightRadius: 30}}>
          <Text style={[styles.textStyle, {fontSize: 15, marginTop: 10}]}>Kirim</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderMessageText(props) {
    const textStyle = {
      fontSize: 14, fontFamily: 'Montserrat',
    };
    return (
      <MessageText textStyle={{left: textStyle, right: textStyle}} {...props} />
    )
  }

  renderChatBubble(props) {
    return ( <Bubble {...props} 
      wrapperStyle={{
          left: {
            backgroundColor: 'white',
            padding: 5,
            marginLeft: -45,
            marginBottom: 5
          },
          right: {
            backgroundColor: '#944ea6',
            padding: 5,
            marginBottom: 5
          }
        }} />
    )
  }

  renderChat(){
    if(this.state.dataLoaded && this.state.idLoaded){
      return(
        <GiftedChat
          messages={this.state.messages}
          loadEarlier={true}
          onLoadEarlier={() => {this.setState({ loadEarlier: true }); this.retrieveChat()}}
          isLoadingEarlier={this.state.loadEarlier}
          minInputToolbarHeight={50}
          renderBubble={this.renderChatBubble}
          renderMessageText={this.renderMessageText}
          renderInputToolbar={this.renderChatInput.bind(this)}
          user={{
            _id: parseInt(this.state.userId)
          }}
        />
      );
    }
    else{
      return(
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  // recevier profile modal

  getUserData(id) {
    fetch('https://temankanker.com/api/UserKankers/' + id)
    .then(response => {
      return response.json();
    })
    .then(data => {
      let source = { uri: 'https://temankanker.com' + data.photo } 
      this.setState({receiverData: data, receiverProfPict: source});
      console.log(data);
    })
  }

  renderReceiverProfile() {
      return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showReceiverProfile}
            onRequestClose={() => {this.setState({showReceiverProfile: false})}}
            >
            <TouchableWithoutFeedback onPress={() => this.setState({showReceiverProfile: false})}>
              <View style={{ padding: 20, flex: 1 }}>
                <View style={{marginTop: 100, backgroundColor: 'white', height: 450, elevation: 30, borderRadius: 8}}>
                  <View style={{padding: 10, backgroundColor: '#489DFF', height: 220, borderTopLeftRadius: 8, borderTopRightRadius: 8, alignItems: 'center'}}>
                    <Image style={{height: 130, width: 130, borderRadius: 65}} onLoad={() => this.setState({pictLoaded: true})} source={this.state.pictLoaded ?  this.state.receiverProfPict : images.profilePictDefault} defaultSource={require('../../images/defaultprofpic-empty.png')}/>
                    <Text style={[styles.textStyle, {marginTop: 10}]}>{this.state.receiverData.name}</Text>
                    <Text style={styles.textStyle2}>{this.state.receiverData.role}</Text>
                  </View>
                  <Content>
                    <View style={{flex: 1, borderBottomLeftRadius: 8, borderBottomRightRadius: 8}}>
                      <List>
                        <ListItem>
                          <Left>
                            <Text style={styles.text}>Jenis Kelamin</Text>
                          </Left>
                          <Body>
                            <Text style={styles.textAnswer}>{this.state.receiverData.sex ? 'Male' : 'Female'}</Text>
                          </Body>
                        </ListItem>
                        <ListItem>
                          <Left>
                            <Text style={styles.text}>Kota</Text>
                          </Left>
                          <Body>
                            <Text style={styles.textAnswer}>{this.state.receiverData.city}</Text>
                          </Body>
                        </ListItem>
                        <ListItem>
                          <Left>
                            <Text style={styles.text}>Tanggal Lahir</Text>
                          </Left>
                          <Body>
                            <Text style={styles.textAnswer}>{moment(this.state.receiverData.birthdate).format('ll')}</Text>
                          </Body>
                        </ListItem>
                        {this.state.receiverData.role !== 'Lainnya' && (
                          <View>
                              <ListItem>
                                <Left>
                                  <Text style={styles.text}>Tipe Kanker</Text>
                                </Left>
                                <Body>
                                  <Text style={styles.textAnswer}>{this.state.receiverData.type}</Text>
                                </Body>
                              </ListItem>
                              <ListItem>
                                <Left>
                                  <Text style={styles.text}>Durasi</Text>
                                </Left>
                                <Body>
                                  {this.state.receiverData.role === 'Survivor' ? (
                                    <Text style={styles.textAnswer}>{this.state.receiverData.duration_start} - {this.state.receiverData.duration_end}</Text>
                                  ) : (
                                    <Text style={styles.textAnswer}>{this.state.receiverData.duration_start} - Sekarang</Text>
                                  )}
                                </Body>
                              </ListItem>
                              {this.state.receiverData.role === 'Pasien' && (
                                  <ListItem>
                                    <Left>
                                      <Text style={styles.text}>Rumah Sakit</Text>
                                    </Left>
                                    <Body>
                                      <Text style={styles.textAnswer}>{this.state.receiverData.hospital}</Text>
                                    </Body>
                                  </ListItem>
                              )}
                              <ListItem>
                                <Left>
                                  <Text style={styles.text}>Dokter</Text>
                                </Left>
                                <Body>
                                  <Text style={styles.textAnswer}>{this.state.receiverData.doctor}</Text>
                                </Body>
                              </ListItem>
                          </View>
                        )}
                        <ListItem>
                          <Left>
                            <Text style={styles.text}>Group</Text>
                          </Left>
                          <Body>
                            <Text style={styles.textAnswer}>{this.state.receiverData.group}</Text>
                          </Body>
                        </ListItem>
                      </List>
                    </View>
                  </Content>
                  
                </View>
              </View>
            </TouchableWithoutFeedback>
            
          </Modal>
      )
  }

  goToPesan = () => {
    const { goBack } = this.props.navigation;
    this.props.pesanActions.setChatRoomStatus(null, false)
    
    goBack(this.props.navigation.state.params.key)
    return true
  }

  render() {
    return (
        <Container>
          <Image source={images.backgroundHome} style={{ flex: 1, width: null, height: null }}>
            <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent  onPress={() => this.goToPesan()}>
                  <Icon style={styles.navBarColor} name='arrow-left'/>
                </Button>
              </Left>
              <Body style={styles.navBarCentered}>
                <TouchableWithoutFeedback onPress={() => this.setState({ showReceiverProfile: true })}>
                  <Title style={styles.navBarTitle}>{this.props.navigation.state.params.receiverName}</Title>
                </TouchableWithoutFeedback>
              </Body>
              <Right style={styles.navBarWidth}>
                <Image style={{height: 30, width: 30, borderRadius: 15}} onLoad={() => this.setState({pictLoaded: true})} source={this.state.pictLoaded ?  this.state.receiverProfPict : images.profilePictDefault} defaultSource={require('../../images/defaultprofpic-empty.png')}/>
              </Right>
            </Header>
            
            {this.renderReceiverProfile()}

            {this.renderChat()}

            {this.state.showReceiverProfile && (
                <View style={[styles.loadingOverlay, {backgroundColor: 'black', opacity: 0.3}]}></View>
            )}
            </Image>
        </Container>
      );
    }
  }

const mapStateToProps = state => ({
  room: state.pesanReducer
})

const mapDispatchToProps = (dispatch) => ({
  pesanActions: bindActionCreators(pesanActions, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoomScreen)
