const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
  navBarWidth:{
    maxWidth: 40
  },
  navBarColor:{
    color: '#fff',
    paddingLeft:5,
    fontSize:24
  },
  navBarTitle:{
    fontFamily: 'BalooThambi-Regular',
    fontWeight: '400',
    color: '#fff',
    fontSize: 24
  },
  navBarStyle:{
    backgroundColor:'#489FFD',
    height: 70,
  },
  navBarCentered:{
    flex: 1,
    alignItems: 'center'
  },
  navBarColorIcon1:{
		width: 17.1,
    height: 23.4,
    backgroundColor: '#3ebb64',
    border: 'solid 0.5px #3ebb64'
  },
  navBarColorIcon2:{
		width: 17.1,
    height: 23.4,
    backgroundColor: '#1b93c0',
    border: 'solid 0.5px #1b93c0'
  },
  navBarColorIcon3:{
		width: 17.1,
    height: 23.4,
    backgroundColor: '#f44e4b',
    border: 'solid 0.5px #f44e4b'
  },
  navBarColorIcon4:{
		width: 17.1,
    height: 23.4,
    backgroundColor: '#944ea5',
    border: 'solid 0.5px #944ea5'
	},
	navBarTitle:{
    fontFamily: 'BalooThambi-Regular',
    fontWeight: '400',
    color: '#fff',
    fontSize: 24
  },
	navBarStyle:{
    backgroundColor:'#489FFD',
    height: 70,
    borderBottomColor: 'transparent',
  },
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
	textUserDate: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12,
  		margin: 30,
  		marginBottom: 0,
  		backgroundColor: 'transparent'
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12,
  		backgroundColor: 'transparent'
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		margin: 30,
  		marginTop: 5,
  		marginBottom: -3,
  		backgroundColor: 'transparent',
  		lineHeight: 30
      },
    text: {
      fontWeight: (Platform.OS === 'ios') ? '400' : '400',
      fontSize: 14,
      fontFamily: 'Montserrat-Regular',
      marginLeft: 20,
      color: '#9A978F',
    },
    textAnswer: {
      fontWeight: (Platform.OS === 'ios') ? '400' : '400',
      fontSize: 18,
      fontFamily: 'BalooThambi-Regular',
      marginLeft: 20,
      color: '#489DFF',
      textAlign: 'right',
    },
    textTitled: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 14,
  		margin: 30,
  		marginTop: 10,
  		marginBottom: 10,
  		backgroundColor: 'transparent',
        lineHeight: 20,
        textAlign: 'center'
  	},
  	contentStyle2:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 30,
  		marginBottom: 20,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 24,
  		color: '#868686'
  	},
  	contentStyle3:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 20,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 24,
  		color: '#868686'
  	},
  	contentStyle3End:{
  		backgroundColor: 'transparent',
  		margin: 30,
  		marginTop: 0,
  		marginBottom: 50,
  		fontFamily: 'Montserrat-Regular',
  		fontSize: 14,
  		lineHeight: 24,
  		color: '#868686',
  	},
    backgroundImage: {
        flex: 1,
        width:null,
        height: null
		},
	buttonLightBlue:{
		height:40,
		marginBottom:10,
		shadowColor: '#000',
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 0.3,
	    shadowRadius: 2,
		backgroundColor:'#499FFD',
	},
  buttonOrange:{
    marginBottom:10,
    shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
    backgroundColor:'#8DC63F',
  },
	buttonText: {
		fontFamily: 'BalooThambi-Regular'
	},
	loadingOverlay: {
			opacity: 0.9,
			backgroundColor: 'white',
			flex: 1,
			width: null,
			height: null,
			justifyContent: 'center',
			alignItems: 'center'
	},

}