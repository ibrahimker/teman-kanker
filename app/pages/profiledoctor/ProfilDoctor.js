import React, { Component } from 'react';
import {Text,View,Drawer,Fab,List,ListItem,Button,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail,Spinner, Toast} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image,AsyncStorage } from 'react-native';
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js';
import Communications from 'react-native-communications';

import { Col, Row, Grid } from 'react-native-easy-grid';

import images from '../../config/images.js';
import styles from './style.js';

export default class ProfilDoctorScreen extends Component {
  static navigationOptions = {
    title: 'Profile',
    header:null
  };

  state =  {
    'isLoggedIn' : 'false',
    'userId':'',
    'role':'',
    data: null,
    dataJadwal: null,
    dataLoaded: false
  }

  constructor() {
    super();
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.data.id;
    fetch('https://temankanker.com/api/Doctors/' + id + '?filter={"include": { "relation" : "getHospital" }}')
    .then((response) => response.json())
    .then((responseJson) => {
        console.log(responseJson);

        fetch('https://temankanker.com/api/DoctorSchedules?filter[where][doctorId]=' + responseJson.id)
        .then(response => response.json())
        .then(data => {
            console.log(data)
            this.setState({
                data: responseJson,
                dataJadwal: data,
                dataLoaded: true
            })
        })
      })
    .catch(error => {
      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    });
  }

  renderLoading() {
    if(!this.state.dataLoaded) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Mengunduh data...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  renderJadwal() {
    return this.state.dataJadwal.map(item => (
      <ListItem key={item.id} style={{ borderWidth: 0, borderColor: 'transparent' }}>
        <Left><Text style={styles.textAnswer}>{item.day}</Text></Left>
        <Body><Text style={styles.textAnswer}>{("0" + item.start_hour).slice(-2)}:{("0" + item.start_minute).slice(-2)} - {("0" + item.end_hour).slice(-2)}:{("0" + item.end_minute).slice(-2)}</Text></Body>
      </ListItem>
    ))
  }

  renderProfil(){
        return (
          <Content style={{marginTop: -75, zIndex: -10}}>
                <View>
                  <View style={{backgroundColor: '#489FFD', height: 300, width: null, flex: 1}}>
                    <Thumbnail style={{height: 150, width: 150, marginLeft:125, marginTop:100, borderRadius: 75}} source={{uri: 'https://temankanker.com' + this.state.data.photo}} />
                  </View>
                </View>                
                <Image source={images.backgroundHome} style={styles.backgroundImage}>
                  <ListItem style={{borderWidth: 0, borderColor: 'transparent'}}>
                    <Left>
                      <Text style={styles.text}>Spesialis</Text>
                    </Left>
                    <Body>
                      <Text style={styles.textAnswer}>{this.state.data.speciality}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                  <ListItem style={{borderWidth: 0, borderColor: 'transparent'}}>
                    <Left>
                      <Text style={styles.text}>Rumah Sakit</Text>
                    </Left>
                    <Body>
                      <Text style={styles.textAnswer}>{this.state.data.getHospital.name}</Text>
                    </Body>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                  <ListItem style={{borderWidth: 0, borderColor: 'transparent'}}>
                      <View style={{flex: 1}}>
                        <Text style={styles.text}>Jadwal Praktek</Text>
                        {this.renderJadwal()}
                      </View>
                  </ListItem>
                  <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
                  <View style={{ padding: 20, paddingTop: 25, paddingBottom: 25 }}>
                      <Button onPress={() => Communications.phonecall(this.state.data.getHospital.phone, true)} rounded large block style={styles.buttonOrange}><Text style={styles.buttonText}>Telfon Rumah Sakit</Text></Button>
                  </View>
                  <View style={{borderBottomWidth: 0.5, borderBottomColor: '#B2B2B2'}}/>
                </Image>
              </Content>                    
        );
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
      return (
        <Container>
            <Header noShadow style={styles.navBarStyle}>
              <Left style={styles.navBarWidth}>
                <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                  <Icon style={styles.navBarColor} name='arrow-left'/>
                </Button>
              </Left>
              {this.state.dataLoaded && (
                  <Body style={styles.navBarCentered}>
                    <Title style={styles.navBarTitle}>{this.state.data.name}</Title>
                  </Body>
              )}
              <Right style={styles.navBarWidth}/>
            </Header>
            {this.state.dataLoaded && this.renderProfil()}

            {this.renderLoading()}
        </Container>
      );
  }
}
