import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs, ScrollableTab} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage, NetInfo} from 'react-native';

import FooterTabNavigation from '../../components/footerTab/FooterTab.js';

import TabKenaliKanker from './KenaliKanker.js';
import TabKankerAnak from './KankerAnak.js';
import TabKankerDewasa from './KankerDewasa.js';

import images from '../../config/images.js';
import styles from './style.js';

export default class BerandaScreen extends Component {
  static navigationOptions = {
    header: null,
    tabBarIcon: (
      <Icon style={{ fontSize: 20 }} name='home'/>
    ),
    tabBarLabel: 'Beranda'
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    news: [],
    dataLoaded: false,
    connection: true
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.news[index].reminder = value;
    this.setState(stateCopy);
  }

  constructor(props) {
    super(props);
    console.log('beranda')
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      news: [],
      dataLoaded: false,
      connection: true
    };
  }

  goToSearch(){
    const { navigate } = this.props.navigation;
    navigate('Search');
  }

 render() {
    StatusBar.setBarStyle('light-content', true);
      return (
      <Container>
          <Header noShadow hasTabs style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
            </Left>
            <Body style={styles.navBarCentered}>   
              <Title style={styles.navBarTitle}>Beranda</Title>
            </Body>
            <Right style={styles.navBarWidth}>
            <Button transparent onPress={() => this.goToSearch()}>
                <Icon style={styles.headerIcon} name='search'/>
            </Button>
            </Right>
          </Header>
          <Tabs tabBarUnderlineStyle={{borderBottomWidth:0}} renderTabBar={()=> <ScrollableTab />}>
              <Tab heading="Kenali Kanker" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
                  <TabKenaliKanker navigation={this.props.navigation}/>
              </Tab>
              <Tab heading="Kanker Anak" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
                  <TabKankerAnak navigation={this.props.navigation}/>
              </Tab>
              <Tab heading="Kanker Dewasa" tabStyle={{backgroundColor: '#499FFD'}} textStyle={{color: '#a0cfff',fontFamily: 'BalooThambi-Regular'}} activeTabStyle={{backgroundColor: '#499FFD'}} activeTextStyle={{color: '#fff', fontWeight: 'normal',fontFamily: 'BalooThambi-Regular'}}>
                  <TabKankerDewasa navigation={this.props.navigation}/>
              </Tab>
          </Tabs>
          
          {/*
          </Image>
          <FooterTabNavigation navigation={this.props.navigation}/>
          */}
      </Container>
    );
  }
}