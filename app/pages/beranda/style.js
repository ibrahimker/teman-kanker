const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
  navBarWidth:{
    maxWidth: 40,
    backgroundColor: 'transparent'
  },
	headerIcon: {
		color: '#fff',
		width: 32,
		fontSize:24
	},
	navBarColor:{
        color: '#fff'
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#fff',
        fontSize: 24,
        paddingLeft: (Platform.OS === 'android') ? 0 : 0
    },
    headerTabStyle:{
    	backgroundColor:'#499FFD',
    	color:'#B2B2B2'
    },
    navBarStyle:{
        backgroundColor:'#499FFD',
        height: 70,
    },
    buttonLightBlue:{
    height:50,
    marginBottom:10,
    marginTop:10,
    shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 2,
    backgroundColor:'#499FFD',
  },
    navbarIcon:{
    	fontSize:24,
    	color:'#fff',
    	paddingLeft:10
    },
    inputIcon: {
    color: '#489FC5',
    width: 32,
    fontSize:20
  },
  balooThambi:{
    fontFamily: 'BalooThambi-Regular',
    fontSize:16,
      marginTop: (Platform.OS === 'android') ? -8 : 4,
  },
  placeholderStyles: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 18
  },
  input:{
    backgroundColor:'#fffae6',
    marginTop:10,
    marginBottom:5,
    paddingLeft:20,
    borderColor:'#499FFD',
    borderWidth:1,
    borderStyle:'dashed'
  },
  cardStyle: {
    backgroundColor: 'white', 
    marginLeft: 8, 
    marginBottom: 8, 
    marginRight: 8,
    marginTop: 8, 
    borderRadius: 8, 
    overflow: 'hidden'
  },
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12
  	},
  	textTitle: {
  		fontFamily: 'BalooThambi-Regular',
  		color: '#489DFF',
      fontSize: 16,
      textAlign: 'center',
  		lineHeight: 22,
      marginTop: (Platform.OS === 'android') ? -8 : 0,
      paddingTop: (Platform.OS === 'ios') ? 4 : 0,

  	},
	thumbnailSize:{
		height:30,
		width:30,
		borderRadius:15,
	},
	thumbnailMargin:{
		marginTop: 5,
		marginBottom: 5,
	},
	cardTitle:{
		color: '#499FFD',
		fontSize: 22,
		fontFamily: 'BalooThambi-Regular',
		textAlign:'center',
		flex:1
	},
  gridList:{
    justifyContent: 'center'
  },
  viewList:{
    justifyContent: 'center',
    padding: 5,
    margin: 10,
    width: 100,
    height: 100,
    backgroundColor: '#F6F6F6',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#CCC'
  }

}