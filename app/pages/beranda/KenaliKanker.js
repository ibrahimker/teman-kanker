import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Footer,FooterTab,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs, Toast} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage,FlatList} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import moment from 'moment';

import images from '../../config/images.js';
import styles from './style.js';

export default class TabKenaliKankerScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    listArticle: [],
    dataLoaded: false
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      listArticle: [],
      dataLoaded: false
    };
  }

  componentDidMount() {

    fetch('https://temankanker.com/api/Cancers?filter=%7B%22where%22%3A%7B%22type%22%3A%22Info%22%7D%7D')
    .then((response) => response.json())
    .then((data) => {
      let arr = [];
      while(data.length) arr.push(data.splice(0,2));
      return this.setState({ listArticle: arr, dataLoaded: true });
    })
    .catch(error => {
      Toast.show({
        text: 'Gagal mendapatkan data kanker',
        position: 'top',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  renderArtikel() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return this.state.listArticle.map((article,index) => (
        <Row key={index}>
          <Col>
            <TouchableOpacity onPress={() =>  this.props.navigation.navigate('BerandaDetil', {articleId : article[0].id})}>
              <View style={styles.cardStyle}>
                  <Image source={{uri: 'https://temankanker.com' + article[0].photo}} style={{height: 182, width: null, flex: 1, borderTopLeftRadius: 8, borderTopRightRadius: 8}}/>
                  <View style={{padding: 10}}>
                      <Text style={styles.textTitle}>{article[0].name}</Text>
                  </View>
              </View>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity onPress={() =>  this.props.navigation.navigate('BerandaDetil', {articleId : article[1].id})}>
              <View style={styles.cardStyle}>
                  <Image source={{uri: 'https://temankanker.com' + article[1].photo}} style={{height: 182, width: null, flex: 1, borderTopLeftRadius: 8, borderTopRightRadius: 8}}/>
                  <View style={{padding: 10}}>
                      <Text style={styles.textTitle}>{article[1].name}</Text>
                  </View>
              </View>
            </TouchableOpacity>
          </Col>
        </Row>
      ));
    }
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    return (
      <Container>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
            <Content>
              <View style={{margin:8, marginTop: 5}}>
                <Grid>
                  {this.renderArtikel()}
                </Grid>
              </View>
            </Content>
          </Image>
      </Container>
    );
  }
}