import React, { Component } from 'react';
import {Toast,Text,Item,Input,Form,View,Drawer,Spinner,Button,Container,Content,Header,Footer,FooterTab,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab, Tabs} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage,FlatList} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import moment from 'moment';
import images from '../../config/images.js';
import styles from './style.js';

export default class SearchScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    listArticle: [],
    dataLoaded: false
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      listArticle: [],
      dataLoaded: false,
      sendingNote: false
    };
  }

  componentDidMount() {
    // fetch('https://temankanker.com/api/Cancers')
    // .then((response) => response.json())
    // .then((data) => {
    //   return this.setState({ listArticle: data, dataLoaded: true });
    // })
    // .done();
  }

  validateInput() {
    let query = this.state.query;
    const { navigate } = this.props.navigation;

    this.setState({ loging: true });
    this.setState({sendingNote: true});

    if(query != "") {
      fetch('https://temankanker.com/api/Cancers?filter[order]=created_at%20desc&filter[where][name][regexp]=/' + encodeURIComponent(query) + '/ig')
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ listArticle: data, dataLoaded: true, sendingNote: false });
      })
      .done();
    }
    else {
      Toast.show({
        supportedOrientations: ['portrait','landscape'],
        text: 'Field must not be empty!',
        position: 'bottom',
        buttonText: 'Okay',
        type:'danger',
        duration: 1000
      })
    }
  }

  renderLoading() {
    if(this.state.sendingNote) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 12, margin:12, textAlign:'center'}}>Sedang Mencari, Mohon Tunggu..</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  /*
  renderArtikel() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return this.state.listArticle.map((article,index) => (
          <Card key={article.id}>
            <TouchableOpacity onPress={() =>  this.props.navigation.navigate('BerandaDetil', {articleId : article.id})}>
              <CardItem cardBody>
                <Image source={{uri: 'https://temankanker.com'+article.photo}} style={{resizeMode: 'cover', height: 200, width: null, flex: 1 }}/>
              </CardItem>
              <CardItem>
                <Body style={{marginTop: 5}}>
                  <Text style={styles.textTitle}>{article.name}</Text>
                </Body>
              </CardItem>
            </TouchableOpacity>
          </Card>
      ));
    }
  }
  */

  render() {
    openDrawer = () => {
      this.drawer._root.open()
    };
    closeDrawer = () =>{
      this.drawer._root.close()
    };
    pressDrawer = () =>{

    }
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#000');
    return (
      <Container>
      <Header noShadow hasTabs style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
              <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                <Icon style={styles.navbarIcon} name='close'/>
              </Button>
            </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Pencarian</Title>
            </Body>
            <Right style={styles.navBarWidth}>
              {
                // <Button transparent onPress={() => this.goToSearch()}>
                //   <Icon style={styles.headerIcon} name='search'/>
                // </Button>
              }
            </Right>
          </Header>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
        <Content>
          <Form style={{padding: 15}}>
            <Item rounded style={styles.input}>
                  <Icon style={styles.inputIcon} active name='search' />
                  <Input style={styles.placeholderStyles} type="text" placeholder='Kata Pencarian' placeholderTextColor='#93938E' onChangeText={(query) => this.setState({query})}/>
            </Item>
            <Button rounded onPress={() => this.validateInput()} style={styles.buttonLightBlue} block>
                <Text style={styles.balooThambi}>Cari</Text>
            </Button>
          </Form>
           {this.renderLoading()}
            <View style={{margin:16, marginTop: 5}}>
            <FlatList
              contentContainerStyle={styles.gridList}
              horizontal={false}
              columnWrapperStyle={{flex:1,width:null,height:null}}
              numColumns={2}
              data={this.state.listArticle}
              renderItem={({item, separators}) => (
                <Card key={item.id}>
                  <TouchableOpacity onPress={() =>  this.props.navigation.navigate('BerandaDetil', {articleId : item.id})}>
                    <CardItem cardBody>
                      <Image source={{uri: 'https://temankanker.com'+item.photo}} style={{height: 150, width: null, flex: 1}}/>
                    </CardItem>
                    <CardItem>
                      <Body style={{marginTop: 0}}>
                        <Text style={styles.textTitle}>{item.name}</Text>
                      </Body>
                    </CardItem>
                  </TouchableOpacity>
                </Card>
              )}
            />  
            </View>
        </Content>
          </Image>
      </Container>
    );
  }
}