import React, { Component } from 'react';
import { AsyncStorage,Image, StyleSheet, Alert, StatusBar, NetInfo, Dimensions, Platform, Keyboard } from 'react-native';
import {Container, Content, View, Button, Text, Item, Input, Form, Toast, StyleProvider,H1,Spinner,Header,Left,Right,Body,Title } from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style.js';

import getTheme from '../../../native-base-theme/components';
import material from '../../../native-base-theme/variables/material';

import HomeScreen from '../../pages/home/Home.js';
import images from '../../config/images.js';

const APIENDPOINT = 'https://temankanker.com/api/';

export default class LoginScreen extends Component {
  constructor (props){
    super(props);
    this.state = { loging: false, username: '', password: '' };
  }
  static navigationOptions = {
    header:null,
  };

  renderLoading() {
    if(this.state.loging) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Masuk...</Text>
          <Spinner color="#489FFD"/>
        </View>
      );
    }
  }

  componentDidMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      this.setState({connection: isConnected});
    });

    // NetInfo.isConnected.addEventListener(
    //   'connectionChange',
    //   this.handleFirstConnectivityChange
    // );
  }

  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.setState({connection: true});
    }
    else {
      this.setState({connection: false});
    }
  }

  render() {
    StatusBar.setBarStyle('light-content', true);
    StatusBar.setBackgroundColor('#489ffc');

    const { height, width } = Dimensions.get('window')

    return (
        <Container style={styles.container}>
          <Header noShadow style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
              <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                <Icon style={styles.navbarIcon} name='close'/>
              </Button>
            </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Login</Title>
            </Body>
            <Right style={styles.navBarWidth}/>
          </Header>
          {!this.state.loging && (
              <Image source={images.backgroundHome} style={styles.backgroundImage}>
                <View style={{height: Platform.OS === 'ios' ? '100%' : height - 80}}>
                  <View style={styles.form}>
                    <Form>
                      <Item rounded style={styles.input}>
                        <Icon style={styles.inputIcon} active name='envelope' />
                        <Input style={styles.placeholderStyles} keyboardType={'email-address'} type="text" placeholder='Email' placeholderTextColor='#93938E' onChangeText={(username) => this.setState({username})}/>
                      </Item>
                      <Item rounded style={styles.input}>
                        <Icon style={styles.inputIcon} active name='lock' />
                        <Input style={styles.placeholderStyles} secureTextEntry={true} placeholder='Kata Sandi' placeholderTextColor='#93938E' onChangeText={(password) => this.setState({password})}/>
                      </Item>
                    </Form>
                    <Text style={styles.forgotPassword} onPress={() => this.props.navigation.navigate('ForgotPassword')}>Lupa Password?</Text>
                    <Button rounded onPress={() => this.validateInput()} style={styles.buttonLightBlue} block>
                      <Text style={styles.balooThambi}>Masuk</Text>
                    </Button>
                    <Button rounded onPress={() => this.loginAsGuest()} style={styles.buttonNavy} block>
                      <Text style={styles.balooThambi}>Masuk sebagai Tamu</Text>
                    </Button>
                    <Text style={styles.registerHere} onPress={() => this.goToRegistration()}>Belum Punya Akun? <Text style={styles.registerHere2}>Daftar Disini</Text></Text>
                  </View>
                  {/*
                  Gambar nya nimpa text daftar disini, jadi ga bisa di klik. sementara di komen dulu
                  <View style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center', position: 'absolute', bottom: 0, left: 0, right: 0}}>
                  */}
                  <View style={{flex: 1, alignItems: 'center'}}>
                    <Image source={images.loginFooter} style={{width: 300, height: 200}} resizeMode={"cover"}/>
                  </View>
                </View>
              </Image>
          )}
          {this.renderLoading()}
        </Container>
    );
  }

  loginAsGuest(){
    const { navigate } = this.props.navigation;
    navigate('Tab');
  }

  goToRegistration(){
    if(!this.state.connection) {
        Toast.show({
          text: 'Anda harus terhubung dengan internet untuk melakukan registrasi',
          position: 'bottom',
          type:'danger',
          duration: 2000
        });
    }
    else {
      const { navigate } = this.props.navigation;
      navigate('Registration');
    }
  }

  validateInput() {
    let username = this.state.username;
    let password = this.state.password;
    const { navigate } = this.props.navigation;

    Keyboard.dismiss()

    if(!this.state.connection) {
      Toast.show({
        text: 'Koneksi internet anda terputus',
        position: 'bottom',
        type:'danger',
        duration: 2000
      });
    }
    else {
      if(username != "" && password != "") {
        this.setState({ loging: true });
        fetch(APIENDPOINT+'/UserKankers/login', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "username" : username,
            "password" : password
          })
        })
        .then((response) => { 
          this.setState({ loging: false });
          return response.json();    
        })
        .then(function(data){ 
          console.log(data);
          if(data.status === 'error') {
            Toast.show({
              text: 'Username atau password salah!',
              position: 'bottom',
              type:'danger',
              duration: 2000
            });
          } 
          else {
            AsyncStorage.setItem('isLoggedIn', 'true');
            AsyncStorage.setItem('username', username);
            AsyncStorage.setItem('role', '');
            AsyncStorage.setItem('userId', data.id.toString());
            AsyncStorage.setItem('role', data.role);
            navigate('Tab');
            Toast.show({
              text: 'Berhasil masuk!',
              position: 'bottom',
              type:'success',
              duration: 2000
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState({ loging: false });
          Toast.show({
            text: 'Terjadi kesalahan pada sistem kami',
            position: 'bottom',
            type:'danger',
            duration: 2000
          });
        })
        
      }
      else {
        Toast.show({
          supportedOrientations: ['portrait','landscape'],
          text: 'Username dan password harus diisi',
          position: 'bottom',
          buttonText: 'Ok',
          type:'danger',
          duration: 1000
        })
      }
    }
  }
}