const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
	    maxWidth: 40,
	    backgroundColor: 'transparent'
	},
	backgroundImage:{
		width:null,
		flex:1,
	},
	loadingOverlay: {
        opacity: 0.9,
        backgroundColor: 'white',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
		right: 0,
		zIndex: 1000,
        justifyContent: 'center',
        alignItems: 'center'
    },
	container: {
		backgroundColor: '#fff',
	},
	view: {
		paddingTop: 30,
		flex: 0,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	logoText: {
		color:'#11095B',
		paddingTop:20,
		fontSize:34,
		fontFamily: 'Kreon-Regular'
	},
	form: {
		paddingTop:30,
		paddingLeft:40,
		paddingRight:40,
	},
	input:{
		backgroundColor:'#fffae6',
		marginTop:10,
		marginBottom:5,
		paddingLeft:20,
		borderColor:'#499FFD',
		borderWidth:1,
		borderStyle:'dashed'
	},
	buttonLightBlue:{
		height:50,
		marginBottom:10,
		marginTop:10,
		shadowColor: '#000',
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 0.3,
	    shadowRadius: 2,
		backgroundColor:'#499FFD',
	},
	buttonNavy:{
		height:50,
		marginBottom:10,
		shadowColor: '#000',
	    shadowOffset: { width: 0, height: 2 },
	    shadowOpacity: 0.3,
	    shadowRadius: 2,
		backgroundColor:'#3C5899'
	},
	montserrat:{
		fontFamily: 'Montserrat',
		fontSize:14
	},
	balooThambi:{
		fontFamily: 'BalooThambi-Regular',
		fontSize:16,
    	marginTop: (Platform.OS === 'android') ? -8 : 4,
	},
	registerHere:{
		color:'#8660A4',
		fontSize:16,
		paddingTop:10,
		backgroundColor: 'transparent',
		fontFamily: 'Montserrat-Regular',
		textAlign:'center',
	},
	registerHere2:{
		color:'#8660A4',
		fontSize:16,
		paddingTop:10,
		fontFamily: 'Montserrat-SemiBold',
		textAlign:'center',
	},
	forgotPassword:{	
		color:'#499FFD',
		textAlign:'center',
		fontFamily: 'Montserrat',
		backgroundColor: 'transparent',
		fontWeight:'500',
		fontSize:15,
		paddingTop:15,
		paddingBottom:15
	},
	separator:{	
		color:'#B2B2B2',
		textAlign:'center',
		fontFamily: 'Montserrat',
		fontSize:14
	},
	placeholderStyles: {
		fontFamily: 'Montserrat-Regular',
		fontSize: 18
	},
	inputIcon: {
		color: '#489FC5',
		width: 32,
		fontSize:20
	},
	navBarColor:{
        color: '#fff'
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#fff',
        fontSize: 24,
    },
    navBarStyle:{
        backgroundColor:'#499FFD',
        height: 70,
    },
    navbarIcon:{
    	fontSize:24,
    	color:'#fff',
    	paddingLeft:5
    },
    loginFooter:{
    	alignItems:'center',
		flex:1,
		width:null,
		resizeMode:'contain',
		marginLeft:50,
		marginRight:50,
    }
}