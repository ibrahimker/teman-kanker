const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
  navBarWidth:{
    maxWidth: 40,
    backgroundColor: 'transparent'
  },
	headerIcon: {
		color: '#fff',
		width: 32,
		fontSize:20
	},
	navBarColor:{
        color: '#fff'
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#fff',
        fontSize: 24,
        paddingLeft: (Platform.OS === 'android') ? 0 : 0
    },
    headerTabStyle:{
    	backgroundColor:'#499FFD',
    	color:'#B2B2B2'
    },
    navBarStyle:{
        backgroundColor:'#499FFD',
        height: 70,
    },
    navbarIcon:{
    	fontSize:20,
    	color:'#fff',
    	paddingLeft:10
    },
	backgroundImage: {
    	flex: 1,
    	width:null,
    	height: null
  	},
  	textUsername: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 15
  	},
  	textUserTitle: {
  		fontFamily: 'Montserrat-Regular',
  		color: '#B2B2B2',
  		fontSize: 12
  	},
  	textTitle: {
  		fontFamily: 'Kreon-Regular',
  		color: '#4C4C4C',
  		fontSize: 18,
  		marginTop: 5,
  		lineHeight: 30
  	},
  	listTitle:{
  		fontFamily: 'BalooThambi-Regular',
        color: '#499FFD',
        fontSize: 20,
        marginTop: 6
  	},
  	listIcon :{
  		fontSize:20,
      color: '#fff',
	  },
	loadingOverlay: {
        opacity: 0.9,
        backgroundColor: 'white',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
}