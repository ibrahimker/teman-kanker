import React, { Component } from 'react';
import {Text,View,Drawer,Spinner,Button,Container,Content,Header,Badge,Left,Body,Right,Title,Card,CardItem,Thumbnail,Tab,List,ListItem,Toast} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity, AsyncStorage} from 'react-native';

import images from '../../config/images.js';
import styles from './style.js';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class LainnyaScreen extends Component {
  static navigationOptions = {
    header: null,
    tabBarIcon: (
      <Icon style={{ fontSize: 20 }} name='ellipsis-h'/>
    ),
    tabBarLabel: 'Lainnya'
  }

  state =  {
    'isLoggedIn' : 'false',
    'username':'',
    'userId':'',
    'onesignalId':'',
    news: [],
    dataLoaded: false,
    logout: false
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.news[index].reminder = value;
    this.setState(stateCopy);
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'username':'',
      'id':'',
      news: [],
      dataLoaded: false
    };
    this.checkAuthentication();
    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });
    });
    AsyncStorage.getItem('onesignalId', (err, result) => {
      this.setState({onesignalId: result})
    });
  }

  checkAuthentication = () => {
    AsyncStorage.getItem('isLoggedIn', (err, result) => {
      this.setState({ 'isLoggedIn': result });
    });
  }

  renderLoading() {
    if(this.state.logout) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25}}>Keluar...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  logout(navigation){
    this.setState({logout: true});
    fetch('https://temankanker.com/api/UserKankers/logout', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "userId": this.state.userId,
        "onesignalId":this.state.onesignalId
      })
    })
    .then((response) => {
      if(response.hasOwnProperty('error')){
        console.log('gagal')
      }
      else{
        console.log('berhasil')
      }
      AsyncStorage.setItem('isLoggedIn', 'false');
      AsyncStorage.setItem('username', '');
      AsyncStorage.setItem('role', '');
      AsyncStorage.setItem('userId', '');
      navigation.navigate('Tab');
      Toast.show({
        text: 'Logout Berhasil!',
        position: 'bottom',
        type:'success',
        duration: 2000
      });
    })
    .catch(error => {
      this.setState({logout: false});
      Toast.show({
        text: 'Terjadi kesalahan pada sistem kami',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
  }

  componentWillMount() {
    this.goToLogin = this.goToLogin.bind(this)
  }

  goToLogin() {
    this.props.navigation.navigate('Login')
  }
  

 render() {
    StatusBar.setBarStyle('light-content', true);

    if(this.state.isLoggedIn == 'true') {
      return (
        <Container>
          <Header noShadow style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
            </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Lainnya</Title>
            </Body>
            <Right style={styles.navBarWidth}>
            </Right>
          </Header>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
          <Content>
              <List>
                <ListItem onPress={() => this.props.navigation.navigate('Profil')} style={{borderWidth: 0, borderColor: 'transparent'}}>
                  <Row>
                    <Col size={1}>
                      <Button style={{backgroundColor:'#79C946',borderRadius:100}}>
                        <Icon name="user" style={styles.listIcon}/>
                      </Button>
                    </Col>
                    <Col size={4}>
                      <Body>
                        <Text style={styles.listTitle}>Profil</Text>
                      </Body>
                    </Col>
                  </Row>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>

                <ListItem onPress={() => this.props.navigation.navigate('Notes')} style={{borderWidth: 0, borderColor: 'transparent'}}>
                  <Row>
                    <Col size={1}>
                <Button style={{backgroundColor:'#F2AC54',borderRadius:100}}>
                    <Icon name="file-text" style={styles.listIcon}/>
                  </Button>
                  </Col>
                  <Col size={4}>
                  <Body>
                    <Text style={styles.listTitle}>Memo</Text>
                  </Body>
                  </Col>
                  </Row>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>

                {/*<ListItem onPress={() => this.props.navigation.navigate('FAQ')}>
                  <Icon name="question" style={styles.listIcon}/>
                  <Body>
                    <Text style={styles.listTitle}>FAQ</Text>
                  </Body>
                </ListItem>*/}                
                <ListItem onPress={() => this.props.navigation.navigate('TentangKita')} style={{borderWidth: 0, borderColor: 'transparent'}}>
                  <Row>
                    <Col size={1}>
                <Button style={{backgroundColor:'#8C5CA0',borderRadius:100}}>
                    <Icon name="users"style={styles.listIcon} />
                  </Button>
                  </Col>
                  <Col size={4}>
                  <Body>
                    <Text style={styles.listTitle}>Tentang Kami</Text>
                  </Body>
                  </Col>
                  </Row>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>

                <ListItem onPress={() => this.logout(this.props.navigation)} style={{borderWidth: 0, borderColor: 'transparent'}}>
                  <Row>
                    <Col size={1}>
                <Button style={{backgroundColor:'#489FFD',borderRadius:100}}>
                    <Icon name="power-off" style={styles.listIcon} />
                  </Button>
                  </Col>
                  <Col size={4}>
                  <Body>
                    <Text style={styles.listTitle}>Logout</Text>
                  </Body>
                  </Col>
                  </Row>
                </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>

              </List>                
          </Content>
          </Image>
          {this.renderLoading()}
        </Container>
      );
    }
    else{
      return (
        <Container>
          <Header noShadow style={styles.navBarStyle}>
            <Left style={styles.navBarWidth}>
            </Left>
            <Body style={styles.navBarCentered}>
              <Title style={styles.navBarTitle}>Lainnya</Title>
            </Body>
            <Right style={styles.navBarWidth}>
            </Right>
          </Header>
          <Image source={images.backgroundHome} style={styles.backgroundImage}>
          <Content>
            <List>
              {/*<ListItem onPress={() => this.props.navigation.navigate('FAQ')}>
                <Icon name="question" style={styles.listIcon}/>
                <Body>
                  <Text style={styles.listTitle}>FAQ</Text>
                </Body>
              </ListItem>*/}
              <ListItem onPress={() => this.props.navigation.navigate('TentangKita')} style={{borderWidth: 0, borderColor: 'transparent'}}>
                <Row>
                    <Col size={1}>
                <Button style={{backgroundColor:'#8C5CA0',borderRadius:100}}>
                  <Icon name="users"style={styles.listIcon} />
                </Button>
                </Col>
                <Col size={4}>
                <Body>
                  <Text style={styles.listTitle}>Tentang Kami</Text>
                </Body>
                </Col>
                </Row>
              </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
              
              <ListItem onPress={this.goToLogin} style={{borderWidth: 0, borderColor: 'transparent'}}>
                <Row>
                    <Col size={1}>
                <Button style={{backgroundColor:'#489FFD',borderRadius:100}}>
                  <Icon name="user" style={styles.listIcon} />
                </Button>
                </Col>
                <Col size={4}>
                <Body>
                  <Text style={styles.listTitle}>Login</Text>
                </Body>
                </Col>
                </Row>
              </ListItem>
                <Image source={images.dotLine} style={{height: 4, marginLeft:10, width: null}}/>
              
            </List>                
          </Content>
          </Image>
        </Container>
      );
    }
  }
}