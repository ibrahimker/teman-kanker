
import React, { Component } from 'react';

import {Toast,Form,Item,Thumbnail,Input,Right,H3,Card,CardItem,Spinner,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class DoctorScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state = {
    doctor: [],
    dataLoaded: false
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.doctor[index].reminder = value;
    this.setState(stateCopy);
  }

  componentDidMount() {
    fetch('https://temankanker.com/api/doctor')
    .then((response) => response.json())
    .then((data) => {
      for(i=0; i<data.length; i++) {
        //data[i].reminder = false;
      }

      return this.setState({ doctor: data, dataLoaded: true });
    })
    .done();
  }

  renderDoctor() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return this.state.doctor.map((doctors,index) => (
        <TouchableOpacity onPress={() =>  this.props.navigation.navigate('DoctorDetail', {id : doctors.id })}>
          <CardItem key={doctors.id} style={styles.listView}>
            <Left>
              <Thumbnail square source={{uri: doctors.pict_doctor}} />
              <Body style={{marginLeft: 15}}>
                <Text style={styles.textUserOrange}>{doctors.specialist_doctor}</Text>
                <Text style={styles.textTitle}>{doctors.name_doctor}</Text>
                <Text style={styles.textUserTitle}>{doctors.hospital_doctor}</Text>
              </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      ));
    }
  }

   render() {

      StatusBar.setBarStyle('dark-content', true);
      StatusBar.setBackgroundColor('white');
  
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
      	this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.footerIconInactive} name='chevron-left'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Dokter</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header> 
              <Content style={{backgroundColor: '#ffffff'}} >
                <Image source={images.backgroundHome} style={styles.backgroundImage} >
                
                <View style={styles.dirContainer}>
                  <Form>
                    <Item rounded style={styles.inputText}>
                      <Icon style={styles.footerIconInactive} name='search'/>
                      <Input onChangeText={(titles) => this.setState({titles})} placeholderTextColor="#b2b2b2" placeholder="Temukan Dokter" secureTextEntry={false} style={styles.inputCustomFont}/>
                    </Item>
                  </Form>
                </View>

                <View style={{marginTop: 15}}>
                  {this.renderDoctor()}
                </View>

                </Image>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}