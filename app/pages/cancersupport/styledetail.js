export default {
    dirBackground: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    dirContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    textWhite: {
        color: 'white'
    },
    textOrange: {
        color: '#f2c400'
    },
    textSm: {
        fontSize: 10,
        color: 'white'
    },
    inputText: {
        marginTop: 10, 
        backgroundColor: 'white', 
        paddingLeft: 15, 
        paddingRight: 15,
        paddingTop: 5,
        paddingBottom: 5
    },
    inputCustomFont: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14
    },
    navBarWidth:{
        maxWidth: 40
    },
    navBarColor:{
        color: '#fff'
    },
    navBarTitle:{
        fontFamily: 'Kreon-Regular',
        fontWeight: '400',
        color: '#3B5998',
        fontSize: 24,
    },
    navBarStyle:{
      backgroundColor: 'transparent',
      borderBottomWidth: 0,
      height: 90
    },
    navBarStyleDet:{
        backgroundColor:'transparent',
        borderBottomWidth: 0.5,
        borderBottomColor:'#B2B2B2',
        height: 90
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    backgroundImage: {
        flex: 1,
        width:null,
        height: null
    },
    textUsername: {
        fontFamily: 'Kreon-Regular',
        color: '#4C4C4C',
        fontSize: 15
    },
    textUserTitle: {
        fontFamily: 'Montserrat-Regular',
        color: '#B2B2B2',
        fontSize: 12
    },
    textUserOrange: {
        fontFamily: 'Montserrat-Regular',
        color: '#f2c400',
        fontSize: 12,
        margin: 30,
        marginBottom: 0,
        backgroundColor: 'transparent'
    },
    textTitle: {
      fontFamily: 'Kreon-Regular',
      color: '#4C4C4C',
      fontSize: 18,
      margin: 30,
      marginTop: 0,
      marginBottom: 15,
      backgroundColor: 'transparent',
      lineHeight: 30
    },
    textSuggest: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        color: '4C4C4C',
        marginTop: 20
    },
    listView: {
        backgroundColor: 'transparent',
        borderTopWidth: 0.5,
        borderTopColor: '#B2B2B2'
    },
    backgroundImage: {
        flex: 1,
        width:null,
    },


}