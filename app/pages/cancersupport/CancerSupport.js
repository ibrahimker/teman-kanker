import React, { Component } from 'react';
import {Toast,Form,Item,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Footer,Left,Body,Title,Fab,List,ListItem} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StyleSheet, Switch, Image, Alert } from 'react-native';
import styles from './style.js';
import images from '../../config/images.js';

import Sidebar from '../../components/sidebar/Sidebar.js'

export default class CancerSupportScreen extends Component {

  static navigationOptions = {
    title: 'Direktori',
    header:null
  };

   render() {
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
        this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container style={{backgroundColor: 'white'}}>
            <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
            <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  {/*<Button transparent onPress={() => this.drawer._root.open()}>
                                      <Icon style={styles.navBarColor} name='menu'/>
                                    </Button>*/}
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Direktori</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
            </Header>
          <Image source={images.backgroundHome} style={styles.dirBackground}>
            <Content>
                  <View style={styles.dirContainer}>
                    <Form>
                      <Item rounded style={styles.inputText}>
                        <Icon style={styles.footerIconInactive} name='search' />
                        <Input onChangeText={(titles) => this.setState({titles})} placeholderTextColor="#b2b2b2" placeholder="Temukan Rumah Sakit, Dokter, dll" secureTextEntry={false} style={styles.inputCustomFont}/>
                      </Item>
                    </Form>

                    <View style={{margin:20, marginTop:0}}>
                      <Text style={styles.textSuggest} onPress={() =>  this.props.navigation.navigate('Hospital')}>Rumah Sakit</Text>
                      <Text style={styles.textSuggest} onPress={() =>  this.props.navigation.navigate('Doctor')}>Dokter</Text>
                      <Text style={styles.textSuggest} onPress={() =>  this.props.navigation.navigate('Pharmacy')}>Apotek</Text>
                    </View>
                    
                  </View>
                
              </Content>

              <Footer noShadow style={styles.navBarFooterStyle}>
                <Body style={styles.bodySpaceBetween}>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('News')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='home'/>
                    <Text style={styles.footerTextInactive}>Beranda</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('Learn')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='info-circle'/>
                    <Text style={styles.footerTextInactive}>Informasi</Text>
                  </Button>
                  <Button transparent onPress={() =>  this.props.navigation.navigate('ChatList')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='comments'/>
                    <Text style={styles.footerTextInactive}>Pesan</Text>
                  </Button>
                  {/*<Button transparent onPress={() =>  this.props.navigation.navigate('SymptomChecker')} style={styles.buttonNavbarText}>
                                  <Icon style={styles.footerIconInactive} name='stethoscope'/>
                                  <Text style={styles.footerTextInactive}>Gejala</Text>
                                </Button>*/}
                  <Button transparent onPress={() =>  this.props.navigation.navigate('CancerSupport')} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconActive} name='location-arrow'/>
                    <Text style={styles.footerTextActive}>Direktori</Text>
                  </Button>
                  <Button transparent onPress={() => this.drawer._root.open()} style={styles.buttonNavbarText}>
                    <Icon style={styles.footerIconInactive} name='ellipsis-h'/>
                    <Text style={styles.footerTextInactive}>Lainnya</Text>
                  </Button>
                  {/*<Button transparent onPress={() =>  this.props.navigation.navigate('Profile')} style={styles.buttonNavbarText}>
                                  <Icon style={styles.footerIconInactive} name='user'/>
                                  <Text style={styles.footerTextInactive}>Profil</Text>
                                </Button>*/}
                </Body>
              </Footer>
              </Image>              
              </Drawer>
          </Container>
        );
    }
}