export default {
    dirBackground: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    dirContainer: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    textWhite: {
        color: 'white'
    },
    textOrange: {
        color: '#f2c400'
    },
    textSm: {
        fontSize: 10,
        color: 'white'
    },
    inputText: {
        backgroundColor:'#ffffff',
        marginTop:10,
        marginBottom:5,
        paddingLeft:20,
    },
    inputCustomFont: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14
    },
    navBarWidth:{
        maxWidth: 40
    },
    navBarColor:{
        color: '#B2B2B2'
    },
    navBarTitle:{
        fontFamily: 'Kreon-Regular',
        fontWeight: '400',
        color: '#3B5998',
        fontSize: 24,
    },
    navBarStyle:{
        backgroundColor:'#ffffff',
        height: 70,
    },
    navBarStyleDet:{
        backgroundColor:'transparent',
        height: 70,
    },
    navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    backgroundImage: {
        flex: 1,
        width:null,
        height: null
    },
    textUsername: {
        fontFamily: 'Kreon-Regular',
        color: '#4C4C4C',
        fontSize: 15
    },
    textUserTitle: {
        fontFamily: 'Montserrat-Regular',
        color: '#B2B2B2',
        fontSize: 12
    },
    textUserOrange: {
        fontFamily: 'Montserrat-Regular',
        color: '#f2c400',
        fontSize: 12,
        marginBottom: 3
    },
    textTitle: {
        fontFamily: 'Kreon-Regular',
        color: '#4C4C4C',
        fontSize: 18,
        marginTop: 0,
        marginBottom: 3
    },
    textSuggest: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        color: '4C4C4C',
        marginTop: 20
    },
    listView: {
        backgroundColor: 'transparent',
        borderTopWidth: 0.5,
        borderTopColor: '#B2B2B2',
        paddingTop: 20,
        paddingBottom: 20
    },
    backgroundImage: {
        flex: 1,
        width:null,
        height: null
    },

    // Footer Style
    navBarFooterStyle:{
        backgroundColor:'#ffffff',
        borderBottomWidth: 0.5,
        borderBottomColor:'#B2B2B2',
    },
    bodySpaceBetween:{
        marginLeft: 16,
        marginRight: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    buttonNavbarText:{
        flexDirection: 'column',
        marginBottom: 5
    },
    footerIconInactive:{
        color: '#B2B2B2',
        fontSize: 22,
    },
    footerIconActive:{
        color: '#3B5998',
        fontSize: 22,
    },
    footerTextInactive:{
        color: '#B2B2B2',
        fontSize: 10,
        fontFamily: 'Montserrat-Regular',
    },
    footerTextActive:{
        color: '#3B5998',
        fontSize: 10,
        fontFamily: 'Montserrat-Regular',
    },
    thumbnailSize:{
        height:30,
        width:30,
        borderRadius:15,
    },
    thumbnailMargin:{
        marginTop: 5,
        marginBottom: 5,
    }

}