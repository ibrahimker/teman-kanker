
import React, { Component } from 'react';

import {Toast,Form,Grid,Item,Icon,Thumbnail,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab} from 'native-base';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class DoctorDetailScreen extends Component {
  static navigationOptions = {
    title: 'Dokter',
    header:null
  };
   state = {
    messages: [],
    id: '',
    hospital_doctor: '',
    name_doctor: '',
    specialist_doctor: '',
    pict_doctor: '',
    data: '',
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    let title_news = params.title_news;
    fetch('https://temankanker.com/api/doctor/' + id)
    .then((response) => response.json())
    .then((responseJson) => {
         console.log(responseJson);
         this.setState({
            data: responseJson
         })
      })
    .done();
  }

  render() {
      StatusBar.setBarStyle('light-content', true);
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
       this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer 
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header shadow style={styles.navBarStyleDet}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() => this.drawer._root.open()}>
                    <Icon style={styles.navBarColor} name='menu'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>{this.state.data.name_doctor}</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>              
              <Content style={{backgroundColor: '#ffffff'}} >
                <Image source={images.backgroundHome} style={styles.backgroundImage} >
                  <View>
                    <CardItem cardBody>
                          <Image source={{uri: this.state.data.pict_doctor }} style={{height: 200, width: null, flex: 1, borderRadius: 5}}/>
                    </CardItem>
                    <CardItem cardBody>
                          <Body style={{marginLeft: 15, marginTop: 15}}>
                            <Text style={styles.textUserOrange}>Open 24 Hours</Text>
                            <Text style={styles.textTitle}>{this.state.data.specialist_doctor}</Text>
                          </Body>
                    </CardItem>
                      <CardItem style={{marginTop: -25, backgroundColor: 'transparent'}}>
                        <Left>
                          <Button transparent>
                            <Icon name="share" style={{color: '#B2B2B2'}}/>
                            <Text style={{marginLeft: 5, color: '#B2B2B2'}}>12</Text>
                          </Button>
                          <Button transparent>
                            <Icon name="heart" style={{color: '#B2B2B2'}}/>
                            <Text style={{marginLeft: 5, color: '#B2B2B2'}}>12</Text>
                          </Button>
                        </Left>
                      </CardItem>
                      <CardItem>
                          <Image source={{uri: 'https://3.bp.blogspot.com/-0r40o7ZMRuc/U39bemu_9_I/AAAAAAAASl0/KDGApxzFe7A/s1600/alamat+map+peta+rumah+sakit+dharmais+jakarta+slipi.png'}} style={{height: 200, paddingLeft: 15, flex: 1, borderRadius: 5}}/>
                    </CardItem>
                    <CardItem>
                        <Text style={styles.textUserTitle}>{this.state.data.hospital_doctor}</Text>                          
                        <Button transparent>
                          <Icon name="map" style={{color: '#B2B2B2'}}/>
                        </Button>
                    </CardItem>
                  </View>
                </Image>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}