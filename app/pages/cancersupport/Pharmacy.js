
import React, { Component } from 'react';

import {Toast,Form,Item,Thumbnail,Spinner,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, TouchableOpacity } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class TreatmentScreen extends Component {
  static navigationOptions = {
    header: null
  }

  state = {
    pharmacy: [],
    dataLoaded: false
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.pharmacy[index].reminder = value;
    this.setState(stateCopy);
  }

  componentDidMount() {
    fetch('https://temankanker.com/api/pharmacies')
    .then((response) => response.json())
    .then((data) => {
      for(i=0; i<data.length; i++) {
        //data[i].reminder = false;
      }

      return this.setState({ pharmacy: data, dataLoaded: true });
    })
    .done();
  }

  renderPharmacy() {
    if(!this.state.dataLoaded) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else {
      return this.state.pharmacy.map((pharmacies,index) => (
        <TouchableOpacity onPress={() =>  this.props.navigation.navigate('PharmacyDetail', {id : pharmacies.id })}>
          <CardItem key={pharmacies.id} style={styles.listView}>
            <Left>
              <Thumbnail square source={{uri: pharmacies.pict_pharmacies}} />
                <Body style={{marginLeft: 15}} >
                   <Text style={styles.textUserOrange}>Open 24 Hours</Text>
                   <Text style={styles.textTitle}>{pharmacies.name_pharmacies}</Text>
                   <Text style={styles.textUserTitle}>{pharmacies.address_pharmacies}</Text>
                </Body>
            </Left>
          </CardItem>
        </TouchableOpacity>
      ));
    }
  }

   render() {

      StatusBar.setBarStyle('dark-content', true);
      StatusBar.setBackgroundColor('white');
  
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
      	this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.footerIconInactive} name='chevron-left'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Apotek</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>               
              <Content style={{backgroundColor: '#ffffff'}} >
                <Image source={images.backgroundHome} style={styles.backgroundImage} >
                <View style={styles.dirContainer}>
                  <Form>
                    <Item rounded style={styles.inputText}>
                      <Icon style={styles.footerIconInactive} name='search'/>
                      <Input onChangeText={(titles) => this.setState({titles})} placeholderTextColor="#b2b2b2" placeholder="Temukan Apotek" secureTextEntry={false} style={styles.inputCustomFont}/>
                    </Item>
                  </Form>

                </View>

                <View style={{marginTop: 15}}>
                  {this.renderPharmacy()}
                </View>

                </Image>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}