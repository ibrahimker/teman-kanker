
import React, { Component } from 'react';

import {Toast,Form,Grid,Item,Icon,Thumbnail,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab} from 'native-base';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './styledetail.js';

export default class HospitalDetScreen extends Component {
  static navigationOptions = {
    title: 'Rumah Sakit',
    header:null
  };

  state = {
    messages: [],
    id: '',
    address_hospital: '',
    name_hospital: '',
    number_hospital: '',
    latitude_hospital: '',
    longitude_hospital: '',
    data: '',
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    let title_news = params.title_news;
    fetch('https://temankanker.com/api/hospital/' + id)
    .then((response) => response.json())
    .then((responseJson) => {
         console.log(responseJson);
         this.setState({
            data: responseJson
         })
      })
    .done();
  }

  render() {
      StatusBar.setBarStyle('light-content', true);
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
       this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                      <Icon style={styles.navBarColor} name='ios-arrow-back'/>
                  </Button>
                </Left>
                <Body/>
                <Right>
                </Right>
              </Header>

              <Content style={{backgroundColor: '#ffffff', marginTop: -90, zIndex: -10}}>
                <Image source={images.backgroundHome} style={styles.backgroundImage} >
                  <View>
                    <CardItem cardBody>
                          <Image source={{uri: this.state.data.pict_hospital }} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                    
                    <Text style={styles.textUserOrange}>Open 24 Hours</Text>
                    <Text style={styles.textTitle}>{this.state.data.name_hospital}</Text>
                    
                    <CardItem style={{backgroundColor: 'transparent'}}>
                      <Image source={{uri: 'https://3.bp.blogspot.com/-0r40o7ZMRuc/U39bemu_9_I/AAAAAAAASl0/KDGApxzFe7A/s1600/alamat+map+peta+rumah+sakit+dharmais+jakarta+slipi.png'}} style={{height: 200, paddingLeft: 15, flex: 1, borderRadius: 5}}/>
                    </CardItem>
                    <CardItem style={{marginTop: -20, backgroundColor: 'transparent', alignItems: 'center'}}>
                      <Text style={styles.textUserTitle}>{this.state.data.address_hospital}</Text>                          
                      <Button transparent>
                        <Icon name="map" style={{color: '#4c4c4c'}}/>
                      </Button>
                    </CardItem>

                    <Button rounded style={{margin: 20, alignSelf: 'center', shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.3, shadowRadius: 2, backgroundColor:'#3B5998'}}>
                      <Text style={{fontFamily: 'Montserrat-Regular', fontSize: 14, color: '#fff', marginLeft: 5}}>Telepon {this.state.data.number_hospital}</Text>
                    </Button>

                  </View>
                </Image>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}