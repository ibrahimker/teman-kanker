
import React, { Component } from 'react';

import {Toast,Form,Item,Icon,Thumbnail,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab} from 'native-base';

// import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image } from 'react-native';

import Sidebar from '../../components/sidebar/Sidebar.js'

import images from '../../config/images.js';
import styles from './style.js';

export default class TreatmentScreen extends Component {
  static navigationOptions = {
    title: 'Pusat Pengobatan',
    header:null
  };
   render() {
      StatusBar.setBarStyle('dark-content', true);
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
       this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container>
          <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<Sidebar navigation={this.props.navigation} />}
              onClose={() => this.drawer._root.close()}
              panOpenMask={.25} >
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() => this.drawer._root.open()}>
                    <Icon style={styles.navBarColor} name='menu'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Pusat Pengobatan</Title>
                </Body>
                <Right style={styles.navBarWidth}/>
              </Header>              
              <Content style={{backgroundColor: '#ffffff'}} >
                <Image source={images.backgroundHome} style={styles.backgroundImage} >
                <Form style={{margin: 15}}>
                      <Item rounded style={styles.inputText}>
                        <Input onChangeText={(titles) => this.setState({titles})} placeholderTextColor="#b2b2b2" placeholder="Temukan Pusat Pengobatan..."/>
                      </Item>
                    </Form>
                  <View style={{margin:16, marginTop: 5}}>
                    <Card>
                      <CardItem>
                        <Left>
                          <Thumbnail source={{uri: 'https://pbs.twimg.com/profile_images/445029802886459393/TsnqRH6i_400x400.png'}} />
                          <Body style={{marginLeft: 15}}>
                            <Text style={styles.textUserOrange}>Jam Buka: 09.00-16.00</Text>
                            <Text style={styles.textTitle}>Yayasan Kanker Indonesia</Text>
                            <Text style={styles.textUserTitle}>Jl. Dr. GSSJ Ratulangi No.35, RT.2/RW.3, Gondangdia, Menteng</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Card>
                    <Card>
                      <CardItem>
                        <Left>
                          <Thumbnail source={{uri: 'https://pbs.twimg.com/profile_images/445029802886459393/TsnqRH6i_400x400.png'}} />
                          <Body style={{marginLeft: 15}}>
                            <Text style={styles.textUserOrange}>Jam Buka: 09.00-16.00</Text>
                            <Text style={styles.textTitle}>Yayasan Kanker Indonesia</Text>
                            <Text style={styles.textUserTitle}>Jl. Dr. GSSJ Ratulangi No.35, RT.2/RW.3, Gondangdia, Menteng</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Card>
                    <Card>
                      <CardItem>
                        <Left>
                          <Thumbnail source={{uri: 'https://pbs.twimg.com/profile_images/445029802886459393/TsnqRH6i_400x400.png'}} />
                          <Body style={{marginLeft: 15}}>
                            <Text style={styles.textUserOrange}>Jam Buka: 09.00-16.00</Text>
                            <Text style={styles.textTitle}>Yayasan Kanker Indonesia</Text>
                            <Text style={styles.textUserTitle}>Jl. Dr. GSSJ Ratulangi No.35, RT.2/RW.3, Gondangdia, Menteng</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Card>
                    <Card>
                      <CardItem>
                        <Left>
                          <Thumbnail source={{uri: 'https://pbs.twimg.com/profile_images/445029802886459393/TsnqRH6i_400x400.png'}} />
                          <Body style={{marginLeft: 15}}>
                            <Text style={styles.textUserOrange}>Jam Buka: 09.00-16.00</Text>
                            <Text style={styles.textTitle}>Yayasan Kanker Indonesia</Text>
                            <Text style={styles.textUserTitle}>Jl. Dr. GSSJ Ratulangi No.35, RT.2/RW.3, Gondangdia, Menteng</Text>
                          </Body>
                        </Left>
                      </CardItem>
                    </Card>
                  </View>
                </Image>
              </Content> 
          </Drawer>
          </Container>
        );
    }
}