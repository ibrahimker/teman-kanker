const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
	navBarWidth:{
    maxWidth: 40,
    backgroundColor: 'transparent'
  },
  navBarColor:{
    color: '#fff',
    width: 32,
    paddingLeft:5,
    fontSize:24
  },
	navBarTitle:{
    fontFamily: 'BalooThambi-Regular',
    fontWeight: '400',
    color: '#fff',
    fontSize: 24,
    paddingLeft:30
  },
  navBarStyle:{
    backgroundColor:'#489DFF',
    height: 70,
  },
	navBarCentered:{
		flex: 1,
		alignItems: 'center'
	},
	backgroundImage: {
  	flex: 1,
  	width:null,
  	height: null
	},
	textUserDate: {
		fontFamily: 'Montserrat-Regular',
		color: '#B2B2B2',
		fontSize: 12,
		margin: 30,
		marginBottom: 0,
		backgroundColor: 'transparent'
	},
	textUsername: {
    fontFamily: 'BalooThambi-Regular',
    color: '#489DFF',
    fontSize: 14,
    paddingTop:30,
    backgroundColor: 'transparent'
  },
	textUserTitle: {
		fontFamily: 'Montserrat-Regular',
		color: '#B2B2B2',
		fontSize: 14,
		backgroundColor: 'transparent'
	},
	textTitle: {
    fontFamily: 'BalooThambi-Regular',
    color: '#489DFF',
    fontSize: 22,
    marginTop: 5,
    lineHeight: 30,
    padding:30,
    paddingTop:20,
    backgroundColor: 'transparent'
  },
  textArticleTime: {
    fontFamily: 'Montserrat-Regular',
    color: '#B2B2B2',
    fontSize: 14,
    paddingTop:12,
    paddingLeft:30,
    backgroundColor: 'transparent'
  },
	contentStyle2:{
		backgroundColor: 'transparent',
		margin: 30,
		marginTop: 30,
		marginBottom: 20,
		fontFamily: 'Montserrat',
		fontSize: 16,
		lineHeight: 24,
		color: '#000'
	},
	contentStyle3:{
		backgroundColor: 'transparent',
		margin: 30,
		marginTop: 0,
		marginBottom: 20,
		fontFamily: 'Montserrat-Regular',
		fontSize: 14,
		lineHeight: 24,
		color: '#868686'
	},
	contentStyle3End:{
		backgroundColor: 'transparent',
		margin: 30,
		marginTop: 0,
		marginBottom: 50,
		fontFamily: 'Montserrat-Regular',
		fontSize: 14,
		lineHeight: 24,
		color: '#868686',
	},
  thumbnailSize:{
    height:40,
    width:40,
    borderRadius:20,
    marginTop:30,
    marginLeft:30
  },
  thumbnailMargin:{
    marginTop: 5,
    marginBottom: 5,
  },
  contentStyleNew:{
    fontFamily: 'Montserrat-Regular',
    fontSize:14,
    color:'#000000',
    marginTop:10,
    lineHeight:24,
    backgroundColor:'transparent',

  },
  loadingOverlay: {
			opacity: 0.9,
			backgroundColor: 'white',
			flex: 1,
			width: null,
			height: null,
			justifyContent: 'center',
			alignItems: 'center'
	},
}