import React, { Component } from 'react';
import {Text,View,Drawer,Button,Container,Content,Header,Left,Body,Right,Title,Card,CardItem,Thumbnail,Spinner, Toast} from 'native-base';


import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, StatusBar, Image, WebView,Share } from 'react-native';
import moment from 'moment';
import Sidebar from '../../components/sidebar/Sidebar.js'
import { Col, Row, Grid } from 'react-native-easy-grid';
import images from '../../config/images.js';
import styles from './style.js';

import HTMLView from 'react-native-htmlview';
const styles2 = StyleSheet.create({
  p: {
    fontFamily: 'Montserrat-Regular',
    fontSize:14,
    color:'#000000',
    marginTop:-10,
    lineHeight: 24,
    marginBottom:-16
  },
  strong: {
    fontFamily: 'Montserrat-Bold'
  },
  b: {
    fontFamily: 'Montserrat-Bold'
  },
  h1: {
    fontFamily: 'Montserrat-Regular'
  },
  i: {
    fontFamily: 'Montserrat-Regular'
  },
  u: {
    fontFamily: 'Montserrat-Regular'
  },
  h2: {
    fontFamily: 'Montserrat-Regular'
  },
  h3: {
    fontFamily: 'Montserrat-Regular'
  },
});

export default class ArtikelScreen extends Component {
  static navigationOptions = {
    title: 'News Detail',
    header:null,
    tabBarVisible: false,
  };
  
  state = {
    messages: [],
    id: '',
    articleTitle: '',
    articleDate: '',
    articleShare: '',
    articleLike: '',
    data: {},
    dataLoaded: false
  };
  
  share(title,content){
    Share.share({
      message: title+' --- '+content.substring(0,140)+'... Baca artikel selengkapnya di aplikasi TemanKanker! https://play.google.com/store/apps/details?id=com.yoai.temankanker',
      url: 'Download Teman Kanker at Play Store',
      title: 'Share to Social Media'
    }, {
      // Android only:
      dialogTitle: title,
      // iOS only:
      excludedActivityTypes: [
        'com.apple.UIKit.activity.PostToTwitter'
      ]
    })
  }
  
  like(userId,informationId){
    if(userId!=''){
      if(userId!=null){
        fetch('https://temankanker.com/api/InformationLikes/like', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "userId": userId,
          "informationId":informationId
        })
      })
      .then((response) => {
        
      })
    }
  }
}

componentWillMount() {
  const { params } = this.props.navigation.state;
  const { navigate } = this.props.navigation;
  let id = params.id;
  fetch('https://temankanker.com/api/Information/' + id)
  .then((response) => response.json())
  .then((responseJson) => {
    this.setState({
      data: responseJson,
      dataLoaded: true
    })
  })
  .catch(error => {
    const { navigation } = this.props;
    navigation.goBack();
    
    Toast.show({
      text: 'Terjadi kesalahan pada sistem kami',
      position: 'top',
      type:'danger',
      buttonText: 'OK',
      duration: 3000
    });
  });
}

renderLoading() {
  if(!this.state.dataLoaded) {
    return (
      <View style={styles.loadingOverlay}>
      <Text style={{fontSize: 25, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Mengunduh data...</Text>
      <Spinner color="#944ea5"/>
      </View>
      );
    }
  }
  
  renderNode(node, index, siblings, parent, defaultRenderer) {
    if (node.name == 'img') {
      const a = node.attribs;
      return ( <Image style={{width: 300, height: 300}} source={{uri: a.src}}/> );
    }
  }
  
  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.id;
    StatusBar.setBarStyle('light-content', true);
    return (
      <Container>
      <Header noShadow style={styles.navBarStyle}>
      <Left style={styles.navBarWidth}>
      <Button transparent onPress={() =>  this.props.navigation.goBack()}>
      <Icon style={styles.navBarColor} name='arrow-left'/>
      </Button>
      </Left>
      <Body style={styles.navBarCentered}>
      
      </Body>
      <Right style={styles.navBarWidth}>
      <Button transparent onPress={() =>  this.share(this.state.data.name,this.state.data.content)}>
      <Icon style={styles.navBarColor} name='share-alt'/>
      </Button>
      </Right>
      </Header>
      {this.state.dataLoaded && (
        <Content style={{marginTop: -90, zIndex: -10}}>
        <Image source={{uri: 'https://temankanker.com'+this.state.data.photo}} style={{height: 300, width: null, flex: 1}}/>
        <Image source={images.backgroundHome} style={styles.backgroundImage}>
        <Grid>
        <Row>
        <Col size={25}>
        <Thumbnail style={styles.thumbnailSize} source={{uri: 'https://temankanker.com'+this.state.data.photo_author}} />
        </Col>
        <Col size={75}>
        <Text style={styles.textUsername}>{this.state.data.author}</Text>
        <Text style={styles.textUserTitle}>Penulis</Text>
        </Col>
        </Row>
        </Grid>
        <Text style={styles.textTitle}>{this.state.data.name}</Text>
        <Grid style={{marginTop:-30}}>
        <Row>
        <Col>
        <Text style={styles.textArticleTime}>{moment(this.state.data.created_at).format('DD MMMM YYYY')}</Text>
        </Col>
        <Col style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
        <Button transparent onPress={() =>  this.share(this.state.data.name,this.state.data.content)} style={{paddingLeft: 0, paddingRight:0, marginLeft:15, marginRight:16}}>
        <Icon name="share-alt" style={{color: '#B2B2B2', fontSize: 16}}/>
        <Text style={{marginLeft: 5, color: '#B2B2B2', font: 'Montserrat', fontSize: 14}}>{this.state.data.share_count}</Text>
        </Button>
        <Button transparent onPress={() =>  this.like(this.state.userId,this.state.data.id)} style={{paddingLeft: 0, marginLeft:15, marginRight:16}}>
        <Icon name="heart" style={{color: '#B2B2B2', fontSize: 16}}/>
        <Text style={{marginLeft: 5, color: '#B2B2B2', font: 'Montserrat', fontSize: 14}}>{this.state.data.like_count}</Text>
        </Button>
        </Col>
        </Row>
        </Grid>
        <View style={{padding:30,paddingTop:10}}>
        <HTMLView
        value={this.state.data.content}
        stylesheet={styles2}
        renderNode={this.renderNode}
        />
        </View>
        </Image>
        </Content>
        )}
        
        {this.renderLoading()}
        </Container>
        );
      }
    }
    