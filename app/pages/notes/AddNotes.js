import React, { Component } from 'react';
import {Toast,Form,Item,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab,Spinner} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, Switch, Image, Alert, AsyncStorage,DeviceEventEmitter, NetInfo } from 'react-native';
import styles from './style.js';
import images from '../../config/images.js';
import Moment from 'moment';
import DatePicker from 'react-native-datepicker'

import Sidebar from '../../components/sidebar/Sidebar.js'

import PushNotification from 'react-native-push-notification';

export default class AddNoteScreen extends Component {

  state = {
    type: 'konsultasi',
    switchVal: false,
    jadwalObatField: [],
    jadwalObat: [],
    index: 0,
    date2: null
  }
  
  constructor(props) {
    super(props);
    this.state = {sendingNote: false};
    this.state = {date: Moment().toJSON()};
    this.state = {status:false};
    this.state = {jadwalObatField: []};
    this.state = {jadwalObat: []};
    this.state = {datas: []};
    this.state = {type: 'konsultasi'};
    this.state = {counter: 0};
    this.state = {role: ''};
    this.state = {data1: []};

    AsyncStorage.getItem('userId', (err, result) => {
      this.setState({ 'userId': result });
      this.setState({ jadwalObatField: [], type: 'konsultasi' });
      this.setState({ jadwalObat: [] });
      this.retrieveUser(result);
    });
  }

  ShowHideTextComponentView = () =>{
    this.setState({status: true})
    this.setState({type: 'obat'})

}

componentDidMount() {

  PushNotification.configure({
      // (required) Called when a remote or local notification is opened or received
      onNotification: function(notification) {
          console.log( 'NOTIFICATION:', notification );
      },
  });

  // PushNotification.cancelAllLocalNotifications();

  NetInfo.isConnected.fetch().then(isConnected => {
    console.log('First, is ' + (isConnected ? 'online' : 'offline'));
    this.setState({connection: isConnected});
  });

  NetInfo.isConnected.addEventListener(
    'connectionChange',
    this.handleFirstConnectivityChange
  );
}

  retrieveUser(userId){
    fetch('https://temankanker.com/api/UserKankers/' + userId)
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ data1: data })
      })
      .catch(error => {
        const { navigation } = this.props;
        navigation.goBack();

        Toast.show({
          text: 'Terjadi kesalahan pada sistem kami' ,
          position: 'bottom',
          type:'danger',
          buttonText: 'OK',
          duration: 3000
        });
      })
      .done();
  }

ShowHideTextComponentView2 = () =>{
 
    this.setState({status: false})
    this.setState({type: 'konsultasi'})

}

ShowHideTextComponentView3 = () =>{
 
    this.setState({status: false})
    this.setState({type: 'terapi'})

}

  static navigationOptions = {
    title: 'AddMemo',
    header: null
  };

  state =  {
    dataLoaded: false,
    switchVal: false
  }

  toggleSwitch(value) {
    this.setState({switchVal: value});

    console.log(value);
    console.log(Moment(this.state.date).format());
    console.log(Moment(this.state.date).utc(7).toJSON());
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.setState({connection: true});
    }
    else {
      this.setState({connection: false});
      Toast.show({
        text: 'Koneksi internet anda terputus',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
  }

  submitForm() {

    console.log(this.state.connection);

    if(this.state.connection) {
        let name = this.state.names;
        let memo = this.state.memo;
        console.log(this.state.date);
        let date = this.state.type === 'obat' ? Moment('2017-01-01 ' + this.state.jadwalObat[0].date).utc(0).toJSON() : Moment(this.state.date).utc(0).toJSON();
    
        let type = this.state.type;
        let user = this.state.userId;
        let notification_status = this.state.switchVal === undefined ? false : this.state.switchVal;

        let jadwalObat = this.state.jadwalObat;

        this.setState({sendingNote: true});

        if(date === undefined) {
          date = new Date(Date.now());
        }

        fetch('https://temankanker.com/api/Memos', {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            "userId": user,
            "name" : name,
            "memo" : memo,
            "type" : type,
            "date_notification" : date,
            "notification_status": notification_status
          })
        })
        .then((response) => { 
          this.setState({sendingNote: false});
          return response.json();    
        })
        .then(function(data){ 
          console.log(data);

          if(data.hasOwnProperty('error')) {
            Toast.show({
              text: 'Error',
              position: 'bottom',
              type:'danger',
              buttonText: 'OK',
              duration: 1000
            });
          }
          else {

            if(notification_status && data.type === 'konsultasi') {
              let date = new Date(data.date_notification);
              console.log(date);
              console.log(Moment(date).format());
              PushNotification.localNotificationSchedule({
                message: "Waktunya konsultasi " + data.name, // (required)
                date: new Date(data.date_notification), // in 60 secs
                userInfo: {id: data.memoId}
              });
            }

            if(notification_status && data.type === 'terapi') {
              let date = new Date(data.date_notification);
              console.log(date);
              console.log(Moment(date).format());
              PushNotification.localNotificationSchedule({
                message: "Waktunya terapi " + data.name, // (required)
                date: new Date(data.date_notification), // in 60 secs
                userInfo: {id: data.memoId}
              });
            }

            if(data.type === 'obat') {
              console.log();
              jadwalObat.forEach(function(element) {
                  fetch('https://temankanker.com/api/MemoMedicines', {
                      method: 'POST',
                      headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        "memoId": data.id,
                        "name" : data.name,
                        "drink_date" : Moment(Moment(Date.now()).format('YYYY-MM-DD') + ' ' + element.date).toJSON(),
                      })
                  })
                  .then((response) => {
                    return response.json();
                  })
                  .then((data) => {
                    console.log('woi');
                    console.log(data);
                    if(notification_status) {
                      console.log(data.drink_date);
                      PushNotification.localNotificationSchedule({
                        message: "Waktunya minum obat " + data.name, // (required)
                        date: new Date(data.drink_date), // in 60 secs
                        userInfo: {id: data.memoId},
                        repeatType: 'day'
                      });
                    }
                  })

              }, this);
            }

            Toast.show({
              text: 'Memo telah disimpan!',
              position: 'bottom',
              type:'success',
              buttonText: 'OK',
              duration: 1000
            });
          }

        })
        .catch((error) => {
          Toast.show({
            text: 'Telah terjadi kesalahan pada sistem kami',
            position: 'bottom',
            type:'danger',
            buttonText: 'OK',
            duration: 1000
          });
        });

        const { navigation } = this.props;
        this.props.navigation.state.params.refresh();
        navigation.goBack();
    }
    else {
      Toast.show({
        text: 'Mohon periksa koneksi internet anda',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
    
  }

  renderLoading() {
    if(this.state.sendingNote) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25}}>Saving note...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

   konsultasi(){
      return (
        <Form>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} name="user-md" />
                          <Input onChangeText={(names) => this.setState({names})} placeholderTextColor="#b2b2b2" placeholder="Judul" secureTextEntry={false} style={styles.inputCustomFont}/>
                        </Item>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} active name='calendar' />
                          <Input disabled/>
                          <DatePicker
                            style={{width: 250, backgroundColor: 'transparent', position: 'absolute'}}
                            date={this.state.date}
                            mode="datetime"
                            placeholder="Jadwal Konsultasi*"
                            format="YYYY-MM-DD H:mm"
                            minDate={Moment().toJSON()}
                            iconComponent={(<Icon style={styles.inputIcon2} active name='calendar' />)}
                            maxDate="2020-06-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateInput: {
                                backgroundColor: 'transparent',
                                borderWidth: 0,
                                color: 'black',
                                marginLeft: 20
                              },
                              placeholderText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#b2b2b2',
                                marginLeft: 20
                              },
                              dateText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#000000',
                                marginLeft: 40
                              }
                              // ... You can check the source to find the other keys. 
                            }}
                            onDateChange={(date) => {this.setState({date: date})}}
                          
                        />
                        </Item>
                        <Item rounded style={styles.inputTextArea}>
                          <Icon style={styles.inputIcon3} active name='file-text' />
                          <Input onChangeText={(memo) => this.setState({memo})} style={{height:100}} multiline={true} placeholderTextColor="#b2b2b2" placeholder="Catatan" secureTextEntry={false} style={styles.inputCustomFontTextArea}/>
                        </Item>
                        <Item rounded style={styles.inputNotif}>
                          <Grid>
                          <Col size={3}>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 18,}}>Notifikasi</Text>
                          </Col>
                          <Col size={1}>
                          <Switch onValueChange={(value) => this.toggleSwitch(value)} value={this.state.switchVal} />
                          </Col>
                          </Grid>
                        </Item>
                      </Form>
       );
   }

   terapi() {
     return (
        <Form>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} name="stethoscope" />
                          <Input onChangeText={(names) => this.setState({names})} placeholderTextColor="#b2b2b2" placeholder="Judul" secureTextEntry={false} style={styles.inputCustomFont}/>
                        </Item>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} active name='calendar' />
                          <Input disabled/>
                          <DatePicker
                            style={{width: 200, backgroundColor: 'transparent', position: 'absolute'}}
                            date={this.state.date}
                            mode="datetime"
                            placeholder="Jadwal Terapi"
                            format="YYYY-MM-DD H:mm"
                            minDate={Moment().toJSON()}
                            maxDate="2020-06-01"
                            iconComponent={(<Icon style={styles.inputIcon2} active name='calendar' />)}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateInput: {
                                backgroundColor: 'transparent',
                                borderWidth: 0,
                                color: '#000000',
                                marginLeft: 20
                              },
                              placeholderText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#b2b2b2',
                                marginLeft: 20
                              },
                              dateText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#000000',
                                marginLeft: 15
                              }
                              // ... You can check the source to find the other keys. 
                            }}
                            onDateChange={(date) => {this.setState({date: date})}}
                            onChangeText={(date) => this.setState({date})} 
                        />
                        </Item>
                        <Item rounded style={styles.inputTextArea}>
                          <Icon style={styles.inputIcon3} active name='file-text' />
                          <Input onChangeText={(memo) => this.setState({memo})} style={{height:100}} multiline={true} placeholderTextColor="#b2b2b2" placeholder="Catatan" secureTextEntry={false} style={styles.inputCustomFontTextArea}/>
                        </Item>
                        <Item rounded style={styles.inputNotif}>
                          <Grid>
                          <Col size={3}>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 18}}>Notifikasi</Text>
                          </Col>
                          <Col size={1}>
                          <Switch onValueChange={(value) => this.toggleSwitch(value)} value={this.state.switchVal} />
                          </Col>
                          </Grid>
                        </Item>
                      </Form>
       );
   }

   addJadwalObatField() {
    this.state.counter++;
      if(this.state.counter<5) {
        this.state.jadwalObatField.push({index: this.state.index++});
        this.setState({ jadwalObatField: this.state.jadwalObatField }); 
      } else {
        Toast.show({
            text: 'Maaf, Anda telah mencapai batas maksimum',
            position: 'bottom',
            type:'success',
            buttonText: 'OK',
            duration: 1000
          });
      }
   }

   addJadwalObat(date, index) {
      this.state.jadwalObat[index] = {date: date};
      this.setState({ jadwalObat: this.state.jadwalObat });
      console.log(index);
      console.log(this.state.jadwalObat);
   }

   obat(){

      let jadwal = this.state.jadwalObatField.map((value, index) => {
        return (
          <Item rounded style={styles.inputText}>
            <Icon style={styles.inputIcon} active name='clock-o' />
            <Input disabled/>
            <DatePicker
              style={{width: 200, backgroundColor: 'transparent', position: 'absolute'}}
              mode="time"
              date={this.state.jadwalObat[index+1] ? this.state.jadwalObat[index+1].date : undefined}
              placeholder="Jadwal Obat"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              iconComponent={(<Icon style={styles.inputIcon2} active name='clock-o' />)}
              customStyles={{
                dateInput: {
                  backgroundColor: 'transparent',
                  borderWidth: 0,
                  color: '#000000',
                  marginLeft: 20
                },
                placeholderText: {
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18,
                  color: '#b2b2b2',
                  marginLeft: 20
                },
                dateText: {
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18,
                  color: '#000000',
                  marginLeft: 15
                }
                // ... You can check the source to find the other keys. 
              }}
              onDateChange={(date) => this.addJadwalObat(date, index+1)}
          />
          </Item>
        )
      });

      return (
        <Form>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} name="medkit" />
                          <Input onChangeText={(names) => this.setState({names})} placeholderTextColor="#b2b2b2" placeholder="Nama Obat" secureTextEntry={false} style={styles.inputCustomFont}/>
                        </Item>

                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} active name='clock-o' />
                          <Input disabled/>
                          <DatePicker
                            style={{width: 200, backgroundColor: 'transparent', position: 'absolute'}}
                            mode="time"
                            date={this.state.jadwalObat[0] ? this.state.jadwalObat[0].date : undefined}
                            placeholder="Jadwal Obat"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            iconComponent={(<Icon style={styles.inputIcon2} active name='clock-o' />)}
                            customStyles={{
                              dateInput: {
                                backgroundColor: 'transparent',
                                borderWidth: 0,
                                color: '#000000',
                                marginLeft: 20
                              },
                              placeholderText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#b2b2b2',
                                marginLeft: 20
                              },
                              dateText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#000000',
                                marginLeft: 15
                              }
                              // ... You can check the source to find the other keys. 
                            }}
                            onDateChange={(date) => this.addJadwalObat(date, 0)}
                        />
                        </Item>
                        
                        { jadwal }
                        
                        <Button bordered info iconLeft block onPress={() => this.addJadwalObatField()} style={{marginTop: 10, borderRadius: 40}}>
                          <Icon name='plus' color="#489DFF" style={{fontSize:16}}/>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 16,}}>Tambah Jadwal Obat</Text>
                        </Button>
                        
                        <Item rounded style={styles.inputNotif}>
                          <Grid>
                          <Col size={3}>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 18,}}>Notifikasi</Text>
                          </Col>
                          <Col size={1}>
                          <Switch onValueChange={(value) => this.toggleSwitch(value)} value={this.state.switchVal} />
                          </Col>
                          </Grid>
                        </Item>
                      </Form>
       );
   }

   renderTypeBtn() {
    if(this.state.data1.role!='Pasien') {
      return (
        <View style={{flex: 1, flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center'}}>
          <Grid style={{marginLeft:20, marginRight:20}}>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView2} style={{borderRadius: 40, backgroundColor: '#489DFF'}}>
            <Text style={styles.tabButton}>Umum</Text>
          </Button>
          </Col>
          </Grid>
        </View>
      )
    }
    else if(this.state.type === 'konsultasi' ||  this.state.type === undefined) {
      return (
        <View style={{flex: 1, flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center'}}>
          <Grid style={{marginLeft:20, marginRight:20}}>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView2} style={{borderRadius: 40, backgroundColor: '#489DFF'}}>
            <Text style={styles.tabButton}>Konsultasi</Text>
          </Button>
          </Col>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView3} style={{borderRadius: 40, marginLeft: 10, backgroundColor: '#b2b2b2'}}>
            <Text style={styles.tabButton}>Terapi</Text>
          </Button>
          </Col>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView} style={{borderRadius: 40, marginLeft: 10, backgroundColor: '#b2b2b2'}}>
            <Text style={styles.tabButton}>Obat</Text>
          </Button>
          </Col>
          </Grid>
        </View>
      )
    }
    else if (this.state.type === 'terapi') {
      return (
        <View style={{flex: 1, flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center'}}>
          <Grid style={{marginLeft:20, marginRight:20}}>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView2} style={{borderRadius: 40, backgroundColor: '#b2b2b2'}}>
            <Text style={styles.tabButton}>Konsultasi</Text>
          </Button>
          </Col>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView3} style={{borderRadius: 40, marginLeft: 10, backgroundColor: '#489DFF'}}>
            <Text style={styles.tabButton}>Terapi</Text>
          </Button>
          </Col>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView} style={{borderRadius: 40, marginLeft: 10, backgroundColor: '#b2b2b2'}}>
            <Text style={styles.tabButton}>Obat</Text>
          </Button>
          </Col>
          </Grid>
        </View>
      )
    }
    else {
      return (
        <View style={{flex: 1, flexDirection: 'row', marginTop: 10, alignItems: 'center', justifyContent: 'center'}}>
          <Grid style={{marginLeft:20, marginRight:20}}>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView2} style={{borderRadius: 40, backgroundColor: '#b2b2b2'}}>
            <Text style={styles.tabButton}>Konsultasi</Text>
          </Button>
          </Col>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView3} style={{borderRadius: 40, marginLeft: 10, backgroundColor: '#b2b2b2'}}>
            <Text style={styles.tabButton}>Terapi</Text>
          </Button>
          </Col>
          <Col>
          <Button block onPress={this.ShowHideTextComponentView} style={{borderRadius: 40, marginLeft: 10, backgroundColor: '#489DFF'}}>
            <Text style={styles.tabButton}>Obat</Text>
          </Button>
          </Col>
          </Grid>
        </View>
      )
    }
   }

   render() {
      openDrawer = () => {
        this.drawer._root.open()
      };
      closeDrawer = () =>{
        this.drawer._root.close()
      };
      pressDrawer = () =>{

      }
        return (
          <Container style={{backgroundColor: 'white'}}>

            <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='close'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Memo Baru</Title>
                </Body>
                <Right style={styles.navBarWidth}>
                  <Button onPress={() => this.submitForm()} transparent>
                    <Icon style={styles.navBarColor} name='check'/>
                  </Button>
                </Right>
              </Header> 

            <Image source={images.backgroundHome} style={styles.notesBackground}>
              <Content>
                <Text style={styles.mainQuestionDesc}>Jenis Memo:</Text>

                      {this.renderTypeBtn()}

                    <View style={styles.notesContainer}>

                        { (this.state.type === 'konsultasi' || this.state.type === undefined) && this.konsultasi() }
                        { this.state.type === 'terapi' && this.terapi() }
                        { this.state.type === 'obat' && this.obat() }

                    </View>
                  
                </Content>
              </Image>

              {this.renderLoading()}
              
          </Container>
        );
    }
}