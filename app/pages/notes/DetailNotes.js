import React, { Component } from 'react';
import {Toast,Form,Item,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab,Spinner} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StyleSheet, Switch, Image, Alert, StatusBar, DeviceEventEmitter, NetInfo} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style.js';
import images from '../../config/images.js';
import Moment from 'moment';
import DatePicker from 'react-native-datepicker'
import Sidebar from '../../components/sidebar/Sidebar.js'

import PushNotification from 'react-native-push-notification';

export default class DetailNotesScreen extends Component {

  static navigationOptions = {
    title: 'ViewMemo',
    header: null
  };

  state = {
    id: '',
    name: '',
    memo: '',
    date: '',
    notification_status: '',
    data: '',
    updating: false,
    deleting: false,
    dataLoaded: false,
    switchVal: false
  };

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    this.setState({data: params.note, switchVal: params.note.notification_status});
    console.log(params.note);

    PushNotification.configure({
        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
            console.log( 'NOTIFICATION:', notification );
        },
    });

    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      this.setState({connection: isConnected});
    });

     NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.setState({connection: true});
    }
    else {
      this.setState({connection: false});
      Toast.show({
        text: 'Koneksi internet anda terputus',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
  }

  renderLoading() {
    if(this.state.updating) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, height: 50, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Memperbaharui data...</Text>
          <Spinner color="#489FFD"/>
        </View>
      );
    }
  }

  deleteMemo(id){
    fetch('https://temankanker.com/api/Memos/' + id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => { 
      // alert(JSON.stringify(response))
        Toast.show({
          text: 'Data Berhasil Dihapus!',
          position: 'bottom',
          type:'success',
          duration: 2000
        });
        const { navigation } = this.props;
        this.props.navigation.state.params.refresh();
        navigation.goBack();
    })

  }

  editMemo(id){
    if(this.state.connection) {
      this.setState({updating: true});
      let name = this.state.names;
      let memo = this.state.memos;
      let date = this.state.date ? Moment(this.state.date).utc(0).toJSON() : Moment(this.state.data.date_notification).utc(0).toJSON();
      let notification_status = this.state.switchVal;

      fetch('https://temankanker.com/api/Memos/' + id, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "name" : name,
          "memo" : memo,
          "date_notification" : date,
          "notification_status": notification_status
        })
      })
      .then((response) => {
        return response.json();
      })
      .then((response) => { 
        console.log(response);
        if(response.hasOwnProperty('error')){
          Toast.show({
            text: 'Update Gagal, Pastikan data terisi sempurna!',
            position: 'bottom',
            type:'alert',
            duration: 2000
          });
        }
        else{

          const { navigation } = this.props;
          this.props.navigation.state.params.refresh();
          navigation.goBack();

          Toast.show({
            text: 'Update Data Berhasil!',
            position: 'bottom',
            type:'success',
            duration: 2000
          });
        }
      })
    }
    else {
      Toast.show({
        text: 'Mohon periksa koneksi internet anda',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
    
  }  

  toggleSwitch(value) {
    this.setState({switchVal: value});

    if(!value) {
      PushNotification.cancelLocalNotifications({id: this.state.data.id});
    }
    else {
      PushNotification.localNotificationSchedule({
        message: "Waktunya konsultasi " + this.state.data.name, // (required)
        date: new Date(this.state.data.date_notification), // in 60 secs
        userInfo: {id: this.state.data.id}
      });
    }
  }

  render() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    let id = params.note.id;
    StatusBar.setBarStyle('light-content', true);
        return (
          <Container style={{backgroundColor: 'white'}}>

            <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='close'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Jadwal Konsultasi</Title>
                </Body>
                <Right style={styles.navBarWidth}>
                  <Button transparent onPress={() => this.deleteMemo(id)}>
                    <Icon style={styles.navBarColor} name='trash'/>
                  </Button>
                </Right>
                <Right style={styles.navBarWidth}>
                  <Button transparent onPress={() => this.editMemo(id)}>
                    <Icon style={styles.navBarColor} name='pencil'/>
                  </Button>
                </Right>
              </Header> 

            <Image source={images.backgroundHome} style={styles.notesBackground}>
              <Content>

                    <View style={styles.notesContainer}>
                    
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} name="user-md" />
                          <Input defaultValue={this.state.data.name} onChangeText={(names) => this.setState({names: names})} placeholderTextColor="#b2b2b2" placeholder="   Judul" style={styles.inputCustomFont}/>
                        </Item>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} active name='calendar' />
                          <Input disabled/>
                          <DatePicker
                            style={{width: 320, backgroundColor: 'transparent', position: 'absolute'}}
                            date={this.state.date}
                            mode="datetime"
                            placeholder={Moment(this.state.data.date_notification).utc(7).format('lll')}
                            iconComponent={(<Icon style={styles.inputIcon2} active name='calendar' />)}
                            format="YYYY-MM-DD H:mm"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                              dateInput: {
                                backgroundColor: 'transparent',
                                borderWidth: 0,
                                color: '#000000',
                                marginLeft: 20
                              },
                              placeholderText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#000000',
                                marginLeft: 20
                              },
                              dateText: {
                                fontFamily: 'Montserrat-Regular',
                                fontSize: 18,
                                color: '#000000',
                                marginLeft: 40
                              }
                              // ... You can check the source to find the other keys. 
                            }}
                            onDateChange={(date) => {this.setState({date: date})}}
                            onChangeText={(date) => this.setState({date})} 
                            
                        />
                        </Item>
                        <Item rounded style={styles.inputTextArea}>
                          <Icon style={styles.inputIcon3} active name='file-text' />
                          <Input defaultValue={this.state.data.memo} onChangeText={(memos) => this.setState({memos: memos})} multiline={true} placeholderTextColor="#b2b2b2" placeholder="Catatan" style={styles.inputCustomFontTextArea}/>
                        </Item>
                        <Item rounded style={styles.inputNotif}>
                          <Grid>
                          <Col size={3}>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 18,}}>Notifikasi</Text>
                          </Col>
                          <Col size={1}>
                          <Switch onValueChange={(value) => this.toggleSwitch(value)} value={this.state.switchVal} />
                          </Col>
                          </Grid>
                        </Item>
                      
                      
                    </View>
                  
                </Content>
              </Image>
              {this.renderLoading()}
          </Container>
        );
    }
}