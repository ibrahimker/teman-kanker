import React, { Component } from 'react';
import {Toast,Form,Item,Input,Right,H3,Card,CardItem,Text,View,Drawer,Button,Container,Content,Header,Left,Body,Title,Fab,Spinner} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StyleSheet, Switch, Image, Alert, StatusBar, DeviceEventEmitter, NetInfo} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './style.js';
import images from '../../config/images.js';
import Moment from 'moment';
import DatePicker from 'react-native-datepicker'
import Sidebar from '../../components/sidebar/Sidebar.js'

import PushNotification from 'react-native-push-notification';

export default class DetailMedicalScreen extends Component {

  static navigationOptions = {
    title: 'ViewMemo',
    header: null
  };

  state = {
    id: '',
    idx: '',
    name: '',
    memo: '',
    date: '',
    notification_status: '',
    data: '',
    datas: '',
    notes: [],
    note:'',
    counter: 0,
    dataLoaded: false,
    switchVal: null,
    updating: false,
    deleting: false,
    jadwalObatField: [],
    jadwalObat: [],
  };

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.notification_status = value;
    this.setState(stateCopy);
  }

  componentDidMount() {
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation;
    this.setState({ data: params.note, notes: params.note.getMedicine });

    PushNotification.configure({
        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
            console.log( 'NOTIFICATION:', notification );
        },
          popInitialNotification: true,
    });
    
    NetInfo.isConnected.fetch().then(isConnected => {
      console.log('First, is ' + (isConnected ? 'online' : 'offline'));
      this.setState({connection: isConnected});
    });

     NetInfo.isConnected.addEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.setState({connection: true});
    }
    else {
      this.setState({connection: false});
      Toast.show({
        text: 'Koneksi internet anda terputus',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
  }

  renderLoading() {
    if(this.state.updating) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25, height: 50, fontFamily: 'BalooThambi-regular', color: '#489FFD'}}>Memperbaharui data...</Text>
          <Spinner color="#489FFD"/>
        </View>
      );
    }
  }

  deleteMemo(id){
    fetch('https://temankanker.com/api/Memos/' + id, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => { 
      // alert(JSON.stringify(response))

        const { navigation } = this.props;
        this.props.navigation.state.params.refresh();
        navigation.goBack();

        Toast.show({
          text: 'Data Berhasil Dihapus!',
          position: 'bottom',
          type:'success',
          duration: 2000
        });
    })
  }
  deleteMemos(ids){
    fetch('https://temankanker.com/api/MemoMedicines/' + ids, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => { 
      // alert(JSON.stringify(response))
      const { navigation } = this.props;
      this.props.navigation.state.params.refresh();
      navigation.goBack();

        Toast.show({
          text: 'Data Berhasil Dihapus!',
          position: 'bottom',
          type:'success',
          duration: 2000
        });
    })
  }

  addMemos(id){
    let memoId =  id;
    let name = this.state.name;
    let drink_date = this.state.date;

    fetch('https://temankanker.com/api/MemoMedicines/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "memoId" : memoId,
        "name" : name,
        "drink_date" : date
      })
    })
    .then((response) => {
      return response.json();
    })
    .then((response) => { 
      console.log(response);
      if(response.hasOwnProperty('error')){
        Toast.show({
          text: 'Update Gagal, Pastikan data terisi sempurna!',
          position: 'bottom',
          type:'alert',
          duration: 2000
        });
      }
      else{
        Toast.show({
          text: 'Update Data Berhasil!',
          position: 'bottom',
          type:'success',
          duration: 2000
        });
      }
    })
  }  

  editMemo(id){

    if(this.state.connection) {
      this.setState({updating: true});
      let name = this.state.names;
      let memo = this.state.memo;
      let date = this.state.notes[0].drink_date;
      let notification_status = this.state.switchVal ? this.state.switchVal : this.state.data.notification_status;

      this.state.notes.forEach(function(element) {
        if(element.hasOwnProperty('id')) {
          console.log(element);
        }
      }, this);

      fetch('https://temankanker.com/api/Memos/' + id, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "name" : name,
          "memo" : memo,
          "date_notification" : date,
          "notification_status": notification_status
        })
      })
      .then((response) => {
        return response.json();
      })
      .then((response) => { 
        console.log(response);
        if(response.hasOwnProperty('error')){
          Toast.show({
            text: 'Update Gagal, Pastikan data terisi sempurna!',
            position: 'bottom',
            type:'alert',
            duration: 2000
          });
        }
        else{

          this.state.notes.forEach(function(element) {
            if(element.hasOwnProperty('id')) {
              fetch('https://temankanker.com/api/MemoMedicines/' + element.id, {
                  method: 'PUT',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    "drink_date" : element.drink_date,
                  })
              })
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                console.log(data);
              })
            }
            else {
              fetch('https://temankanker.com/api/MemoMedicines', {
                  method: 'POST',
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    "memoId": this.state.data.id,
                    "name" : this.state.data.name,
                    "drink_date" : Moment(element.drink_date).utc(0).toJSON(),
                  })
              })
              .then((response) => {
                return response.json();
              })
              .then((data) => {
                console.log(data);
                if(notification_status) {
                  PushNotification.localNotificationSchedule({
                    message: "Waktunya minum obat " + this.state.data.name, // (required)
                    date: new Date(data.drink_date), // in 60 secs
                    userInfo: {id: data.memoId},
                    repeatType: "hour"
                  });
                }
              })
            }
          }, this);

          const { navigation } = this.props;
          this.props.navigation.state.params.refresh();
          navigation.goBack();

          Toast.show({
            text: 'Update Data Berhasil!',
            position: 'bottom',
            type:'success',
            duration: 2000
          });
        }
      })
    }
    else {
      Toast.show({
        text: 'Mohon periksa koneksi internet anda',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
    
  }  

  changeDate(index, date) {
    let drink_date = [...this.state.notes];
    let dates = Moment(Moment(Date.now()).format('YYYY-MM-DD') + ' ' + date).utc(7).toJSON();
    drink_date[index].drink_date = dates;
    this.setState({notes: drink_date});
    console.log(date);
    console.log(this.state.notes);
  }

  renderDates() {
    console.log(this.state.notes);
      return this.state.notes.map((note,index) => (
        (this.state.notes.length == 1) ?
        <Item rounded style={styles.inputText}>
          <Icon style={styles.navBarColorIn} name='clock-o'/>
          <DatePicker
            style={{width: 250}}
            mode="time"
            showIcon={false}
            placeholder={note.drink_date === '' ? 'Pilih jadwal' : Moment(note.drink_date).utc(0).format('HH:mm A')}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
            dateInput: {
                  backgroundColor: 'transparent',
                  borderWidth: 0,
                  color: '#000000',
                  marginLeft: 20
                },
                placeholderText: {
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18,
                  color: '#000000',
                  marginLeft: 20
                },
                dateText: {
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18,
                  color: '#000000',
                  marginLeft: 15
                }
          }}
          onDateChange={(date) => this.changeDate(index, date)}
                              
         >
        </DatePicker>

        <Right style={{paddingRight:10}}>
         <Button transparent onPress={() => this.addAlert2()}>
          <Icon style={styles.navBarColorIn}  name='trash'/>
        </Button>
        </Right>
        </Item>
        :
        ((index<5) ?
          <Item rounded style={styles.inputText}>
          <Icon style={styles.navBarColorIn} name='clock-o'/>
          <DatePicker
            style={{width: 250}}
            mode="time"
            showIcon={false}
            placeholder={note.drink_date === '' ? 'Pilih jadwal' : Moment(note.drink_date).utc(0).format('HH:mm A')}
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
            dateInput: {
                  backgroundColor: 'transparent',
                  borderWidth: 0,
                  color: '#000000',
                  marginLeft: 20
                },
                placeholderText: {
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18,
                  color: '#000000',
                  marginLeft: 20
                },
                dateText: {
                  fontFamily: 'Montserrat-Regular',
                  fontSize: 18,
                  color: '#000000',
                  marginLeft: 15
                }
          }}
          onDateChange={(date) => this.changeDate(index, date)}
                              
         >
        </DatePicker>

        <Right style={{paddingRight:10}}>
         <Button transparent onPress={() => this.deleteMemos(note.id)}>
          <Icon style={styles.navBarColorIn}  name='trash'/>
        </Button>
        </Right>
        </Item>
          :
        <View>
        {this.addAlert()}
        </View>
        )
      ));
  }

  toggleSwitch(value) {
    this.setState({switchVal: value});

    if(!value) {
      PushNotification.cancelLocalNotifications({id: this.state.data.id});
    }
    else {
      this.state.notes.forEach(function(element) {
          PushNotification.localNotificationSchedule({
            message: "Waktunya minum obat " + element.name, // (required)
            date: new Date(element.drink_date), // in 60 secs
            userInfo: {id: element.memoId},
            repeatType: "hour"
          });
      }, this);
    }
  }

  addJadwalObatField() {
      this.state.notes.push({drink_date: ''});
      console.log(this.state.notes);
      this.setState({ jadwalObatField: this.state.jadwalObatField });
  }

  addAlert() {
    Toast.show({
            text: 'Maaf, Anda telah mencapai batas maksimum',
            position: 'bottom',
            type:'success',
            buttonText: 'OK',
            duration: 1000
      });
  }

  addAlert2() {
    Toast.show({
            text: 'Maaf, Anda tidak dapat menghapus jadwal lagi',
            position: 'bottom',
            type:'success',
            buttonText: 'OK',
            duration: 1000
      });
  }

  // addJadwalObat(date, index) {
  //   this.state.jadwalObat[index] = {date: date};
  //   this.setState({ jadwalObat: this.state.jadwalObat });
  //   console.log(index);
  //   console.log(this.state.jadwalObat);
  // }

  render() {
    const { params } = this.props.navigation.state;
    const { navigation } = this.props;
    let id = params.note.id;
    StatusBar.setBarStyle('light-content', true);
        return (
          <Container style={{backgroundColor: 'white'}}>

            <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='close'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Jadwal Minum Obat</Title>
                </Body>
                <Right style={styles.navBarWidth}>
                  <Button transparent onPress={() => this.deleteMemo(id)}>
                    <Icon style={styles.navBarColor} name='trash'/>
                  </Button>
                </Right>
                <Right style={styles.navBarWidth}>
                    <Button transparent onPress={() => this.editMemo(id)}>
                      <Icon style={styles.navBarColor} name='pencil'/>
                    </Button>
                </Right>
              </Header> 

            <Image source={images.backgroundHome} style={styles.notesBackground}>
              <Content>
                  
                    <View style={styles.notesContainer}>
                        <Item rounded style={styles.inputText}>
                          <Icon style={styles.inputIcon} name="medkit" />
                          <Input defaultValue={this.state.data.name} onChangeText={(names) => this.setState({names: names})} placeholderTextColor="#b2b2b2" placeholder="Judul" style={styles.inputCustomFont}/>
                        </Item>
                        { this.renderDates() }
                        <Button bordered info iconLeft block onPress={() => this.addJadwalObatField()} style={{marginTop: 10, borderRadius: 40}}>
                          <Icon name='plus' color="#489DFF" style={{fontSize:16}}/>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 16,}}>Tambah Jadwal Obat</Text>
                        </Button>
                        <Item rounded style={styles.inputNotif}>
                          <Grid>
                          <Col size={3}>
                          <Text style={{fontFamily: 'Montserrat-Regular',fontSize: 18,}}>Notifikasi</Text>
                          </Col>
                          <Col size={1}>
                          <Switch onValueChange={(value) => this.toggleSwitch(value)} value={this.state.switchVal} />
                          </Col>
                          </Grid>
                        </Item>
                    
                    </View>

                </Content>
              </Image>
              {this.renderLoading()}
          </Container>
        );
    }
}