const React = require('react-native');

const { StyleSheet, Platform, Dimensions } = React;

export default {
    navBarWidth:{
        maxWidth: 50,
        backgroundColor: 'transparent'
      },
      navBarCentered:{
        flex: 1,
        alignItems: 'center'
    },
    balooThambi:{
        fontFamily: 'BalooThambi-Regular',
        fontSize:16,
        marginTop: (Platform.OS === 'android') ? -8 : 4,
    },
    loadingOverlay: {
        opacity: 0.9,
        backgroundColor: 'white',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex:10
    },
    backgroundImage:{
        flex: 1,
        resizeMode: 'stretch',
        borderRadius: 5,
        height: 310,        
        width: 265,
        justifyContent: 'center',
        alignItems: 'center',
        left: 40,
        top: 40,
        bottom: 40,
        right: 40
    },
    notesBackground: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    notesContainer: {
        paddingTop: 10,
        paddingLeft: 15,
        paddingRight: 15,
        paddingBottom: 15
    },
    card: {
        borderRadius:5,
        marginTop: 10,
        marginBottom: 10,
        elevation: 0,
        shadowOpacity: 0,
        backgroundColor: '#fff'
    },
    cardItem: {
        backgroundColor: 'transparent'
    },
    cardItemBorderTop: {
        backgroundColor: 'transparent',
        borderTopWidth: 0.5,
        borderTopColor: 'rgba(255,255,255,0.5)',
        marginTop: -10,
    },
    textWhite: {
        color: 'blue',
        paddingBottom: 5
    },
    textGrey: {
        color: '#aaaaaa',
        fontSize: 14,
        marginTop:4,
        fontFamily: 'Montserrat-Regular'
    },
    textOrange: {
        color: 'white',
        fontFamily: 'Montserrat-Regular',
        fontSize: 16,
    },
    textSm: {
        fontSize: 12,
        color: 'white',
        fontFamily: 'Montserrat-Regular'
    },
    inputText: {
        backgroundColor:'transparent',
        marginTop:10,
        marginBottom:5,
        paddingLeft:20,
        borderWidth:2,
        borderStyle:'dashed'
    },
    inputNotif: {
        backgroundColor:'transparent',
        marginTop:10,
        marginBottom:5,
        paddingLeft:20,
        borderWidth:0,
        borderStyle:'dashed',
        borderColor: 'transparent'
    },
    inputIcon: {
        color: '#489DFF',
        width: 35,
        fontSize: 20,
    },
    inputTextArea: {
        marginTop: 10, 
        backgroundColor: 'transparent', 
        paddingLeft: 15, 
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 5,
        borderWidth:2,
        borderStyle:'dashed',
    },
    navBarColor:{
        color: '#fff',
        fontSize:24
    },
    navBarColorIn:{
        color: '#489DFF',
        fontSize:24
    },
    navBarTitle:{
        fontFamily: 'BalooThambi-Regular',
        fontWeight: '400',
        color: '#FFF',
        fontSize: 24
    },
    navBarTitles:{
        fontFamily: 'BalooThambi-Regular',
        color: '#489DFF',
        fontSize: 20,
        height: 26,
        marginTop:-6,
    },
    navBarStyle:{
        backgroundColor:'#489DFF',
        height: 70,
    },
    inputCustomFont: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 18,
        color: 'black'
    },
    inputCustomFontTextArea: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 18,
        height: 100
    },
    inputCustomFontCenter: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 14,
        textAlign: 'center'
    },
    mainQuestionDesc: {
        textAlign: 'center',
        color: '#7E7E7E',
        fontFamily: 'Montserrat-Regular',
        marginLeft: 40,
        marginRight: 40,
        marginTop: 20,
    },
    buttonLightBlue:{
        height:50,
        marginBottom:10,
        marginTop:10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        backgroundColor:'#499FFD',
    },
    tabButton: {
        fontFamily: 'Montserrat-Regular',
        fontSize: (Platform.OS === 'android') ? 12 : 15,
    },
    inputIcon3: {
        color: '#489DFF',
        width: 35,
        fontSize: 20,
        top: -33,
        left: 5,
    },
    inputIcon2: {
        color: '#489DFF',
        width: 35,
        position: 'absolute',
        left: 20,
        fontSize: 20,
    },
    listIcon :{
        fontSize:20,
      color: '#fff',
      shadowOpacity: 0
    },
}