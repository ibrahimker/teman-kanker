import React, { Component } from 'react';
import {Fab,Form,Right,H3,Card,CardItem,Text,View,Button,Container,Content,Header,Left,Body,Title,Spinner,Toast} from 'native-base';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Moment from 'moment';
//import Icon from 'react-native-vector-icons/FontAwesome';
import { StyleSheet, Image, Alert, Switch, StatusBar, TouchableOpacity, DeviceEventEmitter, AsyncStorage, NetInfo} from 'react-native';
import styles from './style.js';
import images from '../../config/images.js';

import Sidebar from '../../components/sidebar/Sidebar.js'

import PushNotification from 'react-native-push-notification';

export default class NotesScreen extends Component {
  static navigationOptions = {
    header: null
  }

  constructor(props) {
    super(props);
    this.state = {
      'isLoggedIn' : 'false',
      'userId':'',
      'id':'',
      notes: [],
      dataId: [],
      datas: [],
      email_confirm: '',
      role: '',
      dataLoaded: false,
      connection: true
    };
  }

  componentDidMount() {
      PushNotification.configure({
          // (required) Called when a remote or local notification is opened or received
          onNotification: function(notification) {
              console.log( 'NOTIFICATION:', notification );
          },
      });

       AsyncStorage.getItem('userId', (err, result) => {
        this.setState({ 'userId': result });
        this.retrieveNotes(result);
        this.retrieveUser(result);
      });

      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleFirstConnectivityChange
      );
  }

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange
    );
  }

  handleFirstConnectivityChange = (isConnected) => {
    console.log('Then, is ' + (isConnected ? 'online' : 'offline'));

    if(isConnected) {
      this.retrieveNotes(this.state.userId);
    }
    else {
      Toast.show({
        text: 'Koneksi internet anda terputus',
        position: 'bottom',
        type:'info',
        buttonText: 'OK',
        duration: 3000
      });
    }
  }

  toggleSwitch(value,index) {
    stateCopy = Object.assign({}, this.state);
    stateCopy.notes[index].notification_status = value;
    this.setState(stateCopy);
  }

  submitForm(userId) {
    fetch('https://temankanker.com/api/UserKankers/getRegistrationToken?userId=' + userId)
    .then((response) => response.json())
    .then((data) => {
      return this.setState({ dataId: data, dataLoaded: true });
    })
    const { navigate } = this.props.navigation;
    const { goBack } = this.props.navigation;
    DeviceEventEmitter.emit('refresh',  {})
    goBack();
    this.holla();
  }

  retrieveUser(userId){
    fetch('https://temankanker.com/api/UserKankers/' + userId)
      .then((response) => response.json())
      .then((data) => {
        return this.setState({ datas: data })
      })
      .catch(error => {

        const { navigation } = this.props;
        navigation.goBack();

        Toast.show({
          text: 'Terjadi kesalahan pada sistem kami',
          position: 'bottom',
          type:'danger',
          buttonText: 'OK',
          duration: 3000
        });
      })
      .done();
  }

  retrieveNotes(userId) {
    this.setState({dataLoaded: false});
    fetch('https://temankanker.com/api/Memos?filter={"where":{"userId": '+ userId +'}, "include": { "relation" : "getMedicine" }}')
    .then((response) => response.json())
    .then((data) => {

      console.log(data);

      for(i=0; i<data.length; i++) {
        data[i].created_at = Moment(data[i].created_at).utc(7).format('LLL');
        data[i].date_notification = Moment(data[i].date_notification).utc(7).format('LLL');

      }
      if(data.length>0){
        data.sort((a,b) => {
          return new Date(a.created_at).getTime() - new Date(b.created_at).getTime()
        }).reverse();
      }
      return this.setState({ notes: data, dataLoaded: true });
    })
    .catch(error => {

      const { navigation } = this.props;
      navigation.goBack();

      Toast.show({
        text: 'Gagal mendapatkan data memo',
        position: 'bottom',
        type:'danger',
        buttonText: 'OK',
        duration: 3000
      });
    })
    .done();
  }

  renderNotes() {
    if(!this.state.dataLoaded || !this.state.connection) {
      return (
        <View style={{flex: 1, alignItems: 'center'}}>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
   else if(this.state.datas.role=='Dokter') {
      return this.state.notes.map((note,index) => (
          (note.type=='obat') ? 
          <TouchableOpacity key={note.id} onPress={() =>  this.props.navigation.navigate('DetailMedical', {note : note, refresh: () => this.retrieveNotes(this.state.userId)})}>
          <Card style={styles.card}>
            <CardItem style={styles.cardItem}>
             <Button style={{backgroundColor:'#8560A8',borderRadius:100, marginTop:4, marginRight: 12}}>
                <Icon name="medkit" style={styles.listIcon} />
              </Button>
              <Body style={{marginTop: 5, paddingBottom: 5}}>
                <Text style={styles.navBarTitles} numberOfLines={1}><H3 style={styles.textOrange}></H3> {note.name}</Text>
                {/* <Text style={styles.textGrey}><H3 style={styles.textOrange}></H3> {note.date_notification}</Text> */}
                <View style={{flexDirection: 'row'}}>
                    {note.getMedicine.map((value, index) => (note.getMedicine.length === index + 1) ? (
                      <Text style={styles.textGrey}>{Moment(value.drink_date).utc().format('hh:mm A')}</Text>
                    ) : (
                      <Text style={styles.textGrey}>{Moment(value.drink_date).utc().format('hh:mm A')}, </Text>
                    ))}
                </View>
              </Body>
            </CardItem>
          </Card>
          </TouchableOpacity>
        : (
          (note.type=='konsultasi') ? 
          <TouchableOpacity key={note.id} onPress={() =>  this.props.navigation.navigate('DetailNotes', {note : note, refresh: () => this.retrieveNotes(this.state.userId)})}>
          <Card style={styles.card}>
            <CardItem style={styles.cardItem}>
              <Button style={{backgroundColor:'#F5AF3E',borderRadius:100, marginTop:4, marginRight: 12}}>
                <Icon name="user-md" style={styles.listIcon} />
              </Button>
              <Body style={{marginTop: 5, paddingBottom: 5}}>
                <Text style={styles.navBarTitles} numberOfLines={1}><H3 style={styles.textOrange}></H3> {note.name}</Text>
                <Text style={styles.textGrey}><H3 style={styles.textOrange}></H3> {note.date_notification}</Text>
              </Body>
            </CardItem>
          </Card>
          </TouchableOpacity>
        :
        <TouchableOpacity key={note.id} onPress={() =>  this.props.navigation.navigate('DetailTerapi', {note : note, refresh: () => this.retrieveNotes(this.state.userId)})}>
          <Card style={styles.card}>
            <CardItem style={styles.cardItem}>
              <Button style={{backgroundColor:'#8DC63F',borderRadius:100, marginTop:4, marginRight: 12}}>
                <Icon name="stethoscope" style={styles.listIcon} />
              </Button>
              <Body style={{marginTop: 5, paddingBottom: 5}}>
                <Text style={styles.navBarTitles} numberOfLines={1}><H3 style={styles.textOrange}></H3> {note.name}</Text>
                <Text style={styles.textGrey}><H3 style={styles.textOrange}></H3> {note.date_notification}</Text>
              </Body>
            </CardItem>
          </Card>
          </TouchableOpacity>
          )
        ));
    }
    else {
      if(this.state.datas.email_confirm==false || this.state.datas.email_confirm==null || this.state.datas.email_confirm=='' || this.state.datas.email_confirm==undefined) {
        return(
       <View style={{flex: 10, alignItems: 'center'}}>
          <Form style={{padding: 15}}>
              <Text style={styles.textGrey}>Maaf, email Anda belum dikonfirmasi, klik tombol dibawah untuk memverifikasi.</Text>
             <Button rounded onPress={() => this.submitForm(this.state.userId)} style={styles.buttonLightBlue} block>
              <Text style={styles.balooThambi}>Konfirmasi Email</Text>
             </Button>
          </Form>
        </View>
        );
      }
      else if(this.state.notes == [] || this.state.notes == '' || this.state.notes == null || this.state.notes === undefined){
        return (
          <Image source={images.memoEmpty} style={styles.backgroundImage} />  
        );
      }
      else if(this.state.datas.email_confirm==true) {
        return this.state.notes.map((note,index) => (
          (note.type=='obat') ? 
          <TouchableOpacity key={note.id} onPress={() =>  this.props.navigation.navigate('DetailMedical', {note : note, refresh: () => this.retrieveNotes(this.state.userId)})}>
          <Card style={styles.card}>
            <CardItem style={styles.cardItem}>
             <Button style={{backgroundColor:'#8560A8',borderRadius:100, marginTop:4, marginRight: 12}}>
                <Icon name="medkit" style={styles.listIcon} />
              </Button>
              <Body style={{marginTop: 5, paddingBottom: 5}}>
                <Text style={styles.navBarTitles} numberOfLines={1}><H3 style={styles.textOrange}></H3> {note.name}</Text>
                {/* <Text style={styles.textGrey}><H3 style={styles.textOrange}></H3> {note.date_notification}</Text> */}
                <View style={{flexDirection: 'row'}}>
                    {note.getMedicine.map((value, index) => (note.getMedicine.length === index + 1) ? (
                      <Text style={styles.textGrey}>{Moment(value.drink_date).utc().format('hh:mm A')}</Text>
                    ) : (
                      <Text style={styles.textGrey}>{Moment(value.drink_date).utc().format('hh:mm A')}, </Text>
                    ))}
                </View>
              </Body>
            </CardItem>
          </Card>
          </TouchableOpacity>
        : (
          (note.type=='konsultasi') ? 
          <TouchableOpacity key={note.id} onPress={() =>  this.props.navigation.navigate('DetailNotes', {note : note, refresh: () => this.retrieveNotes(this.state.userId)})}>
          <Card style={styles.card}>
            <CardItem style={styles.cardItem}>
              <Button style={{backgroundColor:'#F5AF3E',borderRadius:100, marginTop:4, marginRight: 12}}>
                <Icon name="user-md" style={styles.listIcon} />
              </Button>
              <Body style={{marginTop: 5, paddingBottom: 5}}>
                <Text style={styles.navBarTitles} numberOfLines={1}><H3 style={styles.textOrange}></H3> {note.name}</Text>
                <Text style={styles.textGrey}><H3 style={styles.textOrange}></H3> {note.date_notification}</Text>
              </Body>
            </CardItem>
          </Card>
          </TouchableOpacity>
        :
        <TouchableOpacity key={note.id} onPress={() =>  this.props.navigation.navigate('DetailTerapi', {note : note, refresh: () => this.retrieveNotes(this.state.userId)})}>
          <Card style={styles.card}>
            <CardItem style={styles.cardItem}>
              <Button style={{backgroundColor:'#8DC63F',borderRadius:100, marginTop:4, marginRight: 12}}>
                <Icon name="stethoscope" style={styles.listIcon} />
              </Button>
              <Body style={{marginTop: 5, paddingBottom: 5}}>
                <Text style={styles.navBarTitles} numberOfLines={1}><H3 style={styles.textOrange}></H3> {note.name}</Text>
                <Text style={styles.textGrey}><H3 style={styles.textOrange}></H3> {note.date_notification}</Text>
              </Body>
            </CardItem>
          </Card>
          </TouchableOpacity>
          )
        ));
      }
    }
  }

  goToNotes(){
      const { navigate } = this.props.navigation;
      navigate('Skeleton');
  }

  renderLoading() {
    if(this.state.sendingNote) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25}}>Saving note...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
    else if(!this.state.dataLoaded) {
      return (
        <View style={styles.loadingOverlay}>
          <Text style={{fontSize: 25}}>Mengunduh data...</Text>
          <Spinner color="#944ea5"/>
        </View>
      );
    }
  }

  holla() {
    Toast.show({
          text: 'Silahkan cek email Anda untuk verifikasi!',
          position: 'bottom',
          type:'success',
          duration: 5000
        });
  }

   render() {

      StatusBar.setBarStyle('light-content', true);
        return (
          <Image source={images.backgroundHome} style={styles.notesBackground}>
            <Container>
              <Header noShadow style={styles.navBarStyle}>
                <Left style={styles.navBarWidth}>
                  <Button transparent onPress={() =>  this.props.navigation.goBack()}>
                    <Icon style={styles.navBarColor} name='arrow-left'/>
                  </Button>
                </Left>
                <Body style={styles.navBarCentered}>
                  <Title style={styles.navBarTitle}>Memo</Title>
                </Body>
                <Right style={styles.navBarWidth}>
                  {
                    this.state.datas.email_confirm &&
                    <Button transparent>
                      <Icon style={styles.navBarColor} name='plus' onPress={() => this.props.navigation.navigate('AddNote', { refresh: () => this.retrieveNotes(this.state.userId) })}/>
                    </Button>
                  } 
                  {
                    this.state.datas.role=='Dokter' &&
                    <Button transparent>
                      <Icon style={styles.navBarColor} name='plus' onPress={() => this.props.navigation.navigate('AddNote', { refresh: () => this.retrieveNotes(this.state.userId) })}/>
                    </Button>
                  }
                </Right>
              </Header>
              <Content>
                <View style={styles.notesContainer}>                
                  {this.renderNotes()}
                </View>
              </Content> 
              
            </Container>
          </Image>
        );
    }
}