import React, { Component } from 'react';
import { StyleSheet,Text,View, StatusBar } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Root } from 'native-base';

import { createStore, applyMiddleware, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import * as reducers from './reducers'

import { App, Tab } from './config/router.js'

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
const reducer = combineReducers(reducers)
const store = createStoreWithMiddleware(reducer)

export default class cancure extends Component {
  static navigationOptions = {
    title: 'Welcome',
  };

  constructor(props) {
    super(props);
    console.disableYellowBox = true;
  }

  render() {
    StatusBar.setBackgroundColor('#489ffc');
    return (
      <Provider store={store}>
        <Root>
          <App />
        </Root>
      </Provider>
    );
  }
}