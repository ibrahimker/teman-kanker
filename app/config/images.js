const images = {
	logoWithText: require('../images/logo-tidak-beta.png'),
	logo: require('../images/logo-beta-2.png'),
	logoyoai: require('../images/logoyoai.png'),
	logoibm: require('../images/logoibm.png'),
	logoykpi: require('../images/logoykpi.png'),
	logodharmais: require('../images/logodharmais.png'),
	onboardingBelajar: require('../images/onboarding-belajar.png'),
	onboardingBeraksi: require('../images/onboarding-beraksi.png'),
	backgroundHome: require('../images/bg-pattern.png'),
	loginFooter:require('../images/login-footer.png'),
	buttonFooterBarActive:require('../images/button-active.png'),
	pesanEmpty:require('../images/pesan-empty.png'),
	memoEmpty:require('../images/memo-empty.png'),
	icontkactive:require('../images/beranda-active.svg'),
	icontkinactive:require('../images/beranda-inactive.svg'),
	profilePictDefault:require('../images/defaultprofpic-empty.png'),
	dotLine:require('../images/line.png')
};

export default images;