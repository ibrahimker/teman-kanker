import React from 'react';
import { StackNavigator, TabNavigator } from 'react-navigation';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';

import TabBar from '../components/tabBar/TabBar.js'

import SkeletonScreen from '../pages/skeleton/Skeleton.js';
import LoginScreen from '../pages/login/Login.js';
import LoginNoCloseScreen from '../pages/login/LoginNoClose.js';
import BerandaScreen from '../pages/beranda/Beranda.js';
import InformasiScreen from '../pages/informasi/Informasi.js';
import DirektoriScreen from '../pages/direktori/Direktori.js';
import LainnyaScreen from '../pages/lainnya/Lainnya.js';
import ArtikelScreen from '../pages/artikel/Artikel.js';
// import PesanScreen from '../pages/pesan/Pesan.js';
import PesanReduxScreen from '../pages/pesan/PesanRedux.js';
import OnboardingScreen from '../pages/onboarding/Onboarding.js';
import OnboardScreen from '../pages/onboard/Onboard.js';
import Onboard2Screen from '../pages/onboard/Onboard2.js';
import PengaturanScreen from '../pages/pengaturan/Pengaturan.js';
import ProfilScreen from '../pages/profil/Profil.js';
import EditProfilScreen from '../pages/profil/EditProfil.js';
import ProfileDoctorScreen from '../pages/profiledoctor/ProfilDoctor.js';
import FAQScreen from '../pages/faq/FAQ.js';
import TentangKitaScreen from '../pages/tentangKita/TentangKita.js';
import BerandaDetilScreen from '../pages/berandaDetil/BerandaDetil.js';
import RumahSakitDetilScreen from '../pages/direktori/RumahSakitDetil.js';
import OtherDetilScreen from '../pages/direktori/OtherDetil.js';
import SearchScreen from '../pages/beranda/Search.js';
import SearchInfoScreen from '../pages/informasi/SearchInfo.js';
import SearchDirScreen from '../pages/direktori/SearchDir.js';
import SearchChatScreen from '../pages/pesan/SearchChat.js';

import PetaScreen from '../pages/peta/Peta.js';

import ChangePasswordScreen from '../pages/changePassword/ChangePassword.js';
import ForgotPasswordScreen from '../pages/forgotPassword/ForgotPassword.js';

import HomeScreen from '../pages/home/Home.js';
import LearnScreen from '../pages/learn/Learn.js';
import LearnDetailScreen from '../pages/learndetail/LearnDetail.js';
import NewsScreen from '../pages/news/News.js';
import NewsDetailScreen from '../pages/newsdetail/NewsDetail.js';
import NutritionGuideScreen from '../pages/nutritionguide/NutritionGuide.js';
import ActScreen from '../pages/act/Act.js';
import NotesScreen from '../pages/notes/Notes.js';
import DetailNotesScreen from '../pages/notes/DetailNotes.js';
import DetailMedicalScreen from '../pages/notes/DetailMedical.js';
import DetailTerapiScreen from '../pages/notes/DetailTerapi.js';
import AddNoteScreen from '../pages/notes/AddNotes.js';
import CancerSupportScreen from '../pages/cancersupport/CancerSupport.js';
import HospitalScreen from '../pages/cancersupport/Hospital.js';
import HospitalDetScreen from '../pages/cancersupport/HospitalDetail.js';
import DoctorScreen from '../pages/cancersupport/Doctor.js';
import DoctorDetailScreen from '../pages/cancersupport/DoctorDetail.js';
import TreatmentScreen from '../pages/cancersupport/Treatment.js';
import PharmacyScreen from '../pages/cancersupport/Pharmacy.js';
import PharmacyDetScreen from '../pages/cancersupport/PharmacyDetail.js';

import ChatRoomScreen from '../pages/chatroom/ChatRoom.js';
import ChatRoomDoctorScreen from '../pages/chatRoomDoctor/ChatRoomDoctor.js';

import NewChatScreen from '../pages/pesan/NewChat.js';

import RegistrationScreen from '../pages/registration/Registration.js';
import RegistrationScreen_2 from '../pages/registration/Registration_2.js';
import RegistrationScreen_3 from '../pages/registration/Registration_3.js';

export const chatStack = StackNavigator({
	Pesan: { screen: PesanReduxScreen },
	SearchChat: { 
		screen: SearchChatScreen ,
		navigationOptions: ({navigation}) => ({
			tabBarVisible: false,
		}),
	},
	ChatRoom: { 
		screen: ChatRoomScreen ,
		navigationOptions: ({navigation}) => ({
			tabBarVisible: false,
		}),
	},
	ChatRoomDoctor: { 
		screen: ChatRoomDoctorScreen ,
		navigationOptions: ({navigation}) => ({
			tabBarVisible: false,
		}),
	},
	NewChat: { 
		screen: NewChatScreen ,
		navigationOptions: ({navigation}) => ({
			tabBarVisible: false,
		}),
	},	
}, {
	transitionConfig: ()=> {
		return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
	}
})

export const Tab = TabNavigator({
	Beranda: { screen: BerandaScreen },
	Informasi: { screen: InformasiScreen },
	Pesan: { screen: chatStack },
	Direktori: { screen: DirektoriScreen },
	Lainnya: { screen: LainnyaScreen },
}, {
	tabBarComponent: TabBar,
	animationEnabled: false,
	swipeEnabled: false,
	tabBarPosition: 'bottom',
	lazy: true,
	tabBarOptions: {
		showLabel: true,
		activeTintColor: 'white',
		inactiveTintColor: '#7e7e7e'
	}
})

export const App = StackNavigator({
	Onboard: { screen: OnboardScreen },
	Onboard2: { screen: Onboard2Screen },
	
	Onboarding: { screen: OnboardingScreen },
	Login: { screen: LoginScreen },
	ForgotPassword: { screen: ForgotPasswordScreen },

	Tab: { screen: Tab },

	// Skeleton: { screen: SkeletonScreen },	
	Direktori: { screen: DirektoriScreen },
	SearchDir: { screen: SearchDirScreen },
	Beranda: { screen: BerandaScreen },
	Informasi: { screen: InformasiScreen },
	SearchInfo: { screen: SearchInfoScreen },
	Lainnya: { screen: LainnyaScreen },
	Artikel: { screen: ArtikelScreen },
	TentangKita: { screen: TentangKitaScreen },
	// Pesan: { screen: PesanScreen },
	Pengaturan: { screen: PengaturanScreen },
	News: { screen: NewsScreen },
	NewsDetail: { screen: NewsDetailScreen },
	Home: { screen: HomeScreen },
	Search: { screen: SearchScreen },
	// SearchChat: { screen: SearchChatScreen },
	ChangePassword: { screen: ChangePasswordScreen },
	BerandaDetil: { screen: BerandaDetilScreen },
	RumahSakitDetil: { screen: RumahSakitDetilScreen },
	OtherDetil: { screen: OtherDetilScreen },
	
	LoginNoClose: { screen: LoginNoCloseScreen },
	FAQ: { screen: FAQScreen },
	Profil: { screen: ProfilScreen },
	EditProfil: { screen: EditProfilScreen },
	ProfileDoctor: { screen: ProfileDoctorScreen },

	Peta: { screen: PetaScreen },
	
	Learn: { screen: LearnScreen },
	LearnDetail: { screen: LearnDetailScreen },
	NutritionGuide: { screen: NutritionGuideScreen },
	Act: { screen: ActScreen },
	Notes: { screen: NotesScreen },	
	AddNote: { screen: AddNoteScreen },
	DetailNotes: { screen: DetailNotesScreen },
	DetailMedical: { screen: DetailMedicalScreen },
	DetailTerapi: { screen: DetailTerapiScreen },
	CancerSupport: { screen: CancerSupportScreen },
	Hospital: { screen: HospitalScreen },
	HospitalDetail : {screen: HospitalDetScreen},
	Doctor: { screen: DoctorScreen },
	DoctorDetail: { screen: DoctorDetailScreen },
	Treatment: { screen: TreatmentScreen },
	Pharmacy: { screen: PharmacyScreen },
	PharmacyDetail: { screen: PharmacyDetScreen },
	
	// ChatRoom: { screen: ChatRoomScreen },
	// ChatRoomDoctor: { screen: ChatRoomDoctorScreen },
	// NewChat: { screen: NewChatScreen },	
	Registration: { screen: RegistrationScreen },	
	Registration_2: { screen: RegistrationScreen_2 },	
	Registration_3: { screen: RegistrationScreen_3 },
}, {
	transitionConfig: ()=> {
		return {screenInterpolator: CardStackStyleInterpolator.forHorizontal}
	}
});